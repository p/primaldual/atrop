;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; package: gauss -*-
;;;; **********************************************************************
;;;;
;;;; GAUSS algorithm for EQUATIONS from the LRESTR class
;;;;
;;;; (c) 2004 Utz-Uwe Haus, <atrop@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; **********************************************************************

(eval-when
    (:compile-toplevel :load-toplevel)
  (declaim (optimize (safety 1) (debug 1) (speed 3))))

#+sbcl
(progn ;; fixup from sbcl-devel for array typing problems in 0.9.9
  ;; probably should be conditionalized out for recent versions
  (in-package :sb-kernel)
  (defun sb-kernel:vector-t-p (x) 
    (or (simple-vector-p x) 
	(and (sb-kernel::complex-vector-p x) 
	     (do ((data (sb-kernel:%array-data-vector x) (sb-kernel:%array-data-vector data))) 
	       ((not (sb-kernel:array-header-p data)) (simple-vector-p data)))))))



(defpackage #:GAUSS
  (:documentation "Rational-arithmetic Gauss algorithm ($Revision$, $Date$).")
  (:use #:COMMON-LISP #:LRESTR #:ATROP-UTILS)
  (:export ;; functions
	    #:gauss-eliminate-column #:gauss-eliminate #:rank)
  )


(in-package #:GAUSS)

(defun gauss-eliminate-column (m pivcol)
  "Perform Gauss elimination on matrix M in column PIVCOL.
Returns index of the pivot row used, between 0 and length of M. 
Returns -1 if the column does not permit elimination, i.e. is all-0 or the matrix has no rows."
  (declare (type (vector equation) m)
	   (type (integer 0 #.ARRAY-DIMENSION-LIMIT) pivcol))
  (let* ((pivrowidx (select-pivrow m pivcol)))
    (declare (type (integer -1 #.ARRAY-DIMENSION-LIMIT) pivrowidx))
    (if (= -1 pivrowidx)
	(progn
	  ;(format t "(gauss-eliminate-column): No nonzero in column ~D.~%" pivcol)
	  (return-from gauss-eliminate-column -1)))
    
    (let* ((pivrow (aref m pivrowidx))
	   (pivelt (svref (linform pivrow) pivcol))
	   (numcols (length (linform (aref m 0))))) ;; this is lazy
      
      (scale pivrow (/ 1 pivelt))

      (assert (= 1 (svref (linform (aref m pivrowidx)) pivcol)))
      (loop for r from 0 below (length m)
	 unless (= (the integer r) pivrowidx)
	 do (let* ((row (aref m r))
		   (x (svref (linform row) pivcol)))
	      (declare (type rational x))
	      (unless (= x 0)
		(loop for c from 0 below numcols
		   initially
		     (setf (rhs row) (- (rhs row)
					(* x (rhs pivrow))))
		   do (setf (svref (linform row) c)
			      (- (svref (linform row) c)
				 (* x (svref (linform pivrow) c))))
		   finally (beautify row)))))
      pivrowidx)))

(defun gauss-eliminate (m &key (colsequence '()))
  "Perform Gauss elimination on matrix M, on all columns listed in COLSEQUENCE, sequentially.
May apply sorting changes to the rows of M.
If COLSEQUENCE is not given, eliminate from the left as far as possible."
  (declare (type (vector equation) m)
	   (type list colsequence))
  (let ((numrows (length m)))
    (when (> numrows 0)
      (if (null colsequence)
	  (setq colsequence (iota (length (linform (aref m 0))))))
      (do ((round 0)
	   (cols colsequence (cdr cols)))
	  ((or (null cols)
	       (= round numrows)))
	(let* ((submatrix (make-array (- (length m) round)
				      :element-type 'equation 
				      :displaced-to m :displaced-index-offset round))
	       (pivrow (gauss-eliminate-column submatrix (car cols))))
	  (when (> pivrow 0)
	    ;; we have to swap the pivot row into the 0th row 
	    (let ((tmp (aref submatrix 0)))
	      (setf (aref submatrix 0) (aref submatrix pivrow)
		    (aref submatrix pivrow) tmp)))
	  ;; advance submatrix-counter if leading 1 has been constructed in this round
	  (when (>= pivrow 0)
	    (setq round (1+ round)))
	  )))))

(defun select-pivrow (m col)
  "Find a row of M that has a nonzero entry in column COL and has fewest nonzeros overall.
If none exists, return -1."
  (declare (type (vector equation) m))
  ;; still simple-minded, should take potential fill-in into account better
  (let ((candidates
	 (loop for row from 0 below (length m)
	       when (not (= 0 (the rational (svref (linform (aref m row)) col))))
	       collect (cons row (nzcnt (aref m row))))))
    (sort candidates #'< :key #'cdr)
    (if (null candidates)
	-1
	(car (first candidates)))))

(defun rank (m &key (non-destructively nil))
  (declare (type (array equation) m))
  (let ((matrix (if non-destructively
		    (error "cannot copy yet")
		    m)))
    (gauss-eliminate matrix)
    (let ((rank 0)
	  (col 0))
      (loop for row from 0 below (length m)
	    do (let ((next-nonzero
		      (position-if-not #'zerop (linform (aref matrix row))
				       :start col)))
		 (when (and next-nonzero
			    (> next-nonzero col)) ;; TODO: could stop if no progress
		   (incf rank)
		   (setq col next-nonzero))))
      rank)))

  
