;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; package: traf -*-
;;;; *********************************************************************
;;;;
;;;; traf: Transformation between inner and outer description
;;;;
;;;; (c) 2005-2006 Utz-Uwe Haus, <atrop@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; *********************************************************************

(eval-when (:compile-toplevel :load-toplevel)
  (declaim (optimize (safety 1) (debug 2) (speed 3))))

(defpackage #:TRAF
  (:documentation "Transformation between inner and outer description ($Revision$, $Date$).")
  (:use #:COMMON-LISP #:poly #:lrestr #:atrop-utils) 
  (:export #:i2o #:o2i
	   )
  )


(in-package #:TRAF)


(defun i2o (p)
  "Compute an outer description for P."
  (i2o-fm p))

(defun o2i (p)
  "Compute an inner description for P."
  (o2i-fm p))

;;;;; Outer-to-inner transformation

(defun o2i-fm (p)
  "Compute an inner description for P using fourier-motzkin elimination on the polar.
If a VALID-POINT is specified in P, or 0 is recognized to be feasible, 
direct construction of the polar can be applied. 
Otherwise a helper problem (A  b) <= b 
                           (0 -1) <= 0
is used, where -e^n is feasible, and its vertices with e^n-component = 0 are vertices of P, 
and whose extremal rays with e^n-component = 0 are the extremal rays of P."
  (declare (type h-poly p))
  (multiple-value-bind (p* is-in-extended-space)
      (compute-polar p)
    (declare (type v-poly p*))
    (let ((q (i2o-fm p*)))
      (declare (type h-poly q))
      (write-poly-ieq-format q :fname-info "outer description of polar")
      (if is-in-extended-space
	  (format *error-output* "(polar is homogenized)~%"))
      (let ((inner (extract-inner-description q is-in-extended-space)))
	(format t "extracted ~A as inner description~%" inner)
        (undo-validpoint-translation inner (valid-point p) is-in-extended-space)
	))))

(declaim (ftype (function (h-poly) v-poly) compute-polar))
(defun compute-polar (p)
  "Compute the polar of P suitable for computation of the inner description.
Returns new h-poly P* and a boolean indicating whether the extended formulation was used."
  (multiple-value-bind (pp is-in-extended-space?)
      (cond
	((and (valid-point p)
	      (point-validp p (valid-point p)))
	 (values p NIL))
	((point-validp p (make-0-point (adim p)))
	 (progn
	   (format *error-output*
		   "No feasible point given but 0 is feasible, not homogenizing~%")
	   (setf (valid-point p) (make-0-point (adim p)))
	   (values p NIL)))
	(T
	 (values (homogenize p) T)))
    (values (compute-polar-helper pp (valid-point pp)) is-in-extended-space?)))

(defun compute-polar-helper (p valid-point)
  "Do the real work, with VALID-POINT guaranteed to be valid for P."
  ;; gaussify equations to top-right staircase form
  (gauss:gauss-eliminate (eq-rows p))
  (assert (= 0 (numeqs p))) ; FIXME
  ;;(delete-empty-equations p)
  ;; 
  ;; often the valid-point will not have big support, hence we try to save computation time by pretending it were sparse
  (let ((vp-indices (loop for i of-type array-index 
		       from 0 below (length valid-point)
		       when (not (zerop (svref valid-point i)))
		       collect i)))
    ;; translation to make 0-vec the feasible point
    (loop for i of-type array-index from 0 below (numieqs p)
       with ieq = (aref (ieq-rows p) i)
       do (let ((rhs-correction (loop for j of-type array-index in vp-indices
				   summing (* (svref (linform ieq) j)
					      (svref valid-point j)))))
	    (setf (rhs ieq) (- (rhs ieq) rhs-correction))))
    (loop for i of-type array-index from 0 below (adim p)
       with vi = (svref valid-point i)
       do (progn 
	    (when (lower-bounds p)
	      (setf (aref (lower-bounds p) i) (- (aref (lower-bounds p) i) vi)))
	    (when (upper-bounds p)
	      (aref (upper-bounds p) i) (- (aref (upper-bounds p) i) vi)))))
  
  ;; After possibly scaling with 1/rhs we now have rows A_i with rhs 1
  ;; and B_j with rhs 0, and 0 is feasible
  ;; The polar is given by conv{0, A_i^t} + cone{B_i}
  (let ((Ai (list (make-0-point (adim p))))
	(Bj '()))
    (loop for i of-type array-index from 0 below (numieqs p)
       do (let ((ieq  (aref (ieq-rows p) i)))
	    (assert (string= (sense ieq) "<="))
	    (format T "Have ieq with linform ~A~%" (linform ieq))
	    (if (zerop (rhs ieq))
		(push (copy-seq (linform ieq)) Bj)
		(let ((a (copy-seq (linform ieq)))
		      (factor (/ 1 (rhs ieq))))
		  (format T "nonzero rhs ~A, scaling by ~A~%" (rhs ieq) factor)
		  (loop for j of-type array-index from 0 below (adim p)
		     do (setf (svref a j) (* factor (svref a j))))
		  (format T "generator ~A~%" a)
		  (push a Ai)))))
    (format T "Found Ai ~A, Bi ~A~%" Ai Bj)
    (let ((result (make-v-poly (adim p) Ai Bj)))
      (write-poly-poi-format result :fname-info "this is the polar")
      result)))

(defun undo-validpoint-translation (p valid-point is-in-extended-space?)
  ""
  (declare (type v-poly p))
  (when (not is-in-extended-space?)
    ;; shift 0 to the valid-point we have in P, i.e. add valid-point to all
    ;; points in P
    (let ((vp-indices (loop for i of-type array-index 
		       from 0 below (length valid-point)
			 when (not (zerop (svref valid-point i)))
			 collect i)))
      ;; translation to shift 0-vec to the feasible point
      (loop for i of-type array-index from 0 below (numpoints p)
	 with pt = (aref (poly-points p) i)
       do (loop for j of-type array-index in vp-indices
	     do (incf (svref pt j) (svref valid-point j))))
      p))
  (when is-in-extended-space?
    (format T "no tranlation necessary after homogenization (!?~%")
    p))
  

(declaim (ftype (function (h-poly) h-poly) homogenize))
(defun homogenize (p)
  "Compute homogenization for P (so that n+1'th unit vector becomes feasible)"
  (format T "Homogenizing...~%")
  (let* ((adim (+ (adim p) 1))
	 (rows-from-p 
	  (cons
	   (make-inequality :sense '<= :rhs 0
			    :linform (let ((v (make-array adim :element-type 'rational :initial-element 0 :adjustable NIL)))
				       (setf (svref v (- adim 1)) -1)
				       v))
	   (loop for i of-type array-index
	      from 0 below (numieqs p)
	      collecting 
		(let ((ieq (aref (ieq-rows p) i))
		      (new-lf (make-array adim :element-type 'rational :adjustable nil)))
		  ;; extend ieq by rhs keeping old rhs and sense
		  (loop for i of-type array-index from 0 below (- adim 1)
		     do (setf (svref new-lf i) (svref (linform ieq) i))
		     finally (setf (svref new-lf (- adim 1))
				   (- (rhs ieq))))
		  (make-inequality :sense (sense ieq)
				   :rhs 0
				   :linform new-lf))))))
    (make-h-poly adim  rows-from-p
		 :valid-point (let ((v (make-array adim :element-type 'rational :initial-element 0 :adjustable NIL)))
				(setf (svref v (- adim 1)) 1)
				v))))

(defun extract-inner-description (p is-in-extended-space)
  (assert (= 0 (numeqs p)))   ;; FIXME: apply BTRAN using equations instead
  (let ((adim (adim p))
	(points '())
	(rays '()))
    (if (not is-in-extended-space)
	;; This means we had 0 as a feasible point (maybe after shifting), which we need to add here before shifting back
	(progn
	  (push (make-0-point adim) points)
	  (loop for i across (ieq-rows p)
	     do (if (zerop (rhs i))
		    (push (ieqlinform->point i adim) rays)
		    (let ((pt (ieqlinform->point i adim)))
		      (loop for j of-type array-index from 0 below adim
			 with factor = (/ 1 (rhs i))
			 do (setf (svref pt j) (* factor (svref pt j))))
		      (push pt points))))
	  ;; shifting back is done by caller
	  )
	;; homogenized: scale all rows to hom-coord==1
	(let ((hom-coord (- adim 1)))
	  (format T "De-Homogenizing (on coordinate ~D)~%" hom-coord)
	  (loop for i across (ieq-rows p)
	       do (progn 
		    (assert (zerop (rhs i))) ;; all things are rays in hog
		    (if (zerop (aref (linform i) hom-coord))
			(push (ieqlinform->point i (- adim 1) :de-homog hom-coord) rays)
			(push (ieqlinform->point i (- adim 1) :de-homog hom-coord) points))))))
    (make-v-poly (if is-in-extended-space (- adim 1) adim)
		 points rays)))


;;;;; Inner-to-outer transformation
(declaim (ftype (function (v-poly) h-poly) i2o-fm))
(defun i2o-fm (p)
  ;; if P is conv{X} + cone{Y} we have to set up
  ;;  X  Y -I   = 0
  ;;  1  0  0   = 1      (1 row)
  ;; -I  0  0  <= 0
  ;;  0 -I  0  <= 0
  (let* ((points (remove-duplicates (poly-points p) :test #'equalp))
	 (rays (remove-duplicates (poly-rays p) :test #'equalp))
	 (s (if (simple-vector-p points) (length points) (fill-pointer points)))
	 (m (if (simple-vector-p rays) (length rays) (fill-pointer rays)))
	 (n (adim p)))
    (when (> (+ (- (numpoints p) s) (- (numrays p) m)) 0)
      (format *error-output* "Dropped ~D duplicate generator~P~%" 
	      (+ (- (numpoints p) s) (- (numrays p) m))
	      (+ (- (numpoints p) s) (- (numrays p) m))))
    (let ((q 
	   (make-h-poly (+ s m n)
			(append
			 ;; X Y -I
			 (loop for row of-type array-index
			    from 0 below n
			    collecting (let ((lhs (make-array (+ s m n) :element-type 'rational
							      :initial-element 0
							      :adjustable nil 
							      :fill-pointer nil))
					     (nzcnt 0))
					 ;; X
					 (loop for j of-type array-index 
					    from 0 below s
					    do (let ((value (svref (aref (poly-points p) j) row)))
						 (setf (svref lhs j) value)
						 (unless (zerop value) (incf nzcnt))))
					 ;; Y
					 (loop for j of-type array-index 
					    from 0 below m
					    do (let ((value (svref (aref (poly-rays p) j) row)))
						 (setf (svref lhs (+ j s)) value)
						 (unless (zerop value) (incf nzcnt))))
					 ;; -I
					 (setf (svref lhs (+ s m row)) -1)
					 (incf nzcnt)
					 (make-equation :linform lhs :nzcnt nzcnt :rhs 0)))
			 ;; convex combination of first s columns
			 (list
			  (make-equation :linform (let ((lhs (make-array (+ s m n) :element-type 'rational 
									 :initial-element 0
									 :adjustable nil 
									 :fill-pointer nil)))
						    (loop for j of-type array-index 
						       from 0 below s
						       do (setf (svref lhs j) 1))
						    lhs)
					 :nzcnt s
					 :rhs 1))
			 ;; two -I ieq blocks
			 (loop for row of-type array-index
			    from 0 below (+ s m)
			    collecting (let ((lhs (make-array (+ s m n) :element-type 'rational
							      :initial-element 0
							      :adjustable nil 
							      :fill-pointer nil)))
					 (loop for j of-type array-index 
					    from 0 below s
					    do (setf (svref lhs row) -1))
					 (make-inequality :sense '<=
							  :linform lhs
							  :rhs 0
							  :nzcnt 1)))))))
      (format *error-output* "~&Constructed auxilliary problem of ~D rows, ~D columns; need to eliminate ~D variables~%" 
	      (poly:numieqs q) (poly:adim q) (+ s m))
      (write-poly-ieq-format q :fname-info "auxilliary problem")
      (fm-elim q :elim-vars (iota (+ s m)) :expect-no-duplicates T)
      (write-poly-ieq-format q :fname-info "after fm")
      (format *error-output* "~&Elimination done, shrinking system~%")
      ;;(format *error-output* "~&(new x1 was x~D)~%" (1+ (+ s m)))
      
      (let ((eqs (loop for i of-type array-index
		    from 0 below (numeqs q)
		    collecting (make-equation :linform (make-array n :element-type 'rational
								   :initial-contents (subseq (linform 
											      (aref (eq-rows q) i))
											     (+ s m))
								   :adjustable nil)
					      :nzcnt (nzcnt (aref (eq-rows q) i))
					      :rhs (rhs (aref (eq-rows q) i)))))
	    (ieqs (loop for i of-type array-index
		     from 0 below (numieqs q)
		     collecting (make-inequality :linform (make-array n :element-type 'rational
								      :initial-contents (subseq (linform 
												 (aref (ieq-rows q) i))
												(+ s m))
								      :adjustable nil)
						 :sense (sense (aref (ieq-rows q) i))
						 :rhs (rhs (aref (ieq-rows q) i))))))
	(make-h-poly n (append eqs ieqs))))))
				   


;;;;; Utilities
(declaim (ftype (function (inequality array-index) (simple-array rational))
		ieqlinform->point))
(defun ieqlinform->point (i copy-count &key (de-homog nil))
  "Copy COPY-COUNT entries from (linform I) into a fresh simple-vector of rationals.
If de-homog is non-nil, it should be an array index. The vector will be scaled so that the value indexed
becomes 1 before extracting the other components."
  (let* ((lf (linform i))
	 (p (make-array copy-count
			:element-type 'rational :adjustable NIL :fill-pointer NIL))
	 (scale (if de-homog
		    (/ 1 (aref lf de-homog))
		    1)))
    (loop for idx of-type array-index from 0 below copy-count
       do (setf (aref p idx) (* scale (aref lf idx))))
    p))

(declaim (ftype (function (array-index) (simple-array rational))
		make-0-point))
(defun make-0-point (dimension)
  (make-array dimension
	      :element-type 'rational :adjustable NIL :fill-pointer NIL
	      :initial-element 0))
