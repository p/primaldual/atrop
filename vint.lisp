;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; package: vint -*-
;;;; *********************************************************************
;;;;
;;;; vint: Enumeration of integral points within polytope
;;;;
;;;; (c) 2005 Matthias Köppe, <mkoeppe@mail.math.uni-magdeburg.de>
;;;; and Utz-Uwe Haus, <atrop@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; *********************************************************************

;; NOTE: Timing on Allegro CL shows that using (cons rational rational) as range
;;       data type is on-par with an implementation using simple-arrays of 2 
;;       elements. No need to try that again.

(eval-when (:compile-toplevel :load-toplevel)
  (declaim (optimize (safety 1) (debug 1) (speed 3))))

(defpackage #:vint
  (:documentation "Enumeration of integral points within a polytope ($Revision$, $Date$).")
  (:use #:COMMON-LISP #:poly #:traf #:lrestr #:atrop-utils)
  (:export #:enumerate-points
	   )
  )


(in-package #:vint)

;; for v-polys we first apply i2o and then vint for outer descriptions
(defun enumerate-points (p)
  (cond
    ((h-poly-p p) (vint-outer p))
    ((v-poly-p p) (progn
		    (format t "Poly given by inner description, transforming first.~%")
		    (let ((hp (i2o p)))
		      (vint-outer hp))))))

(defun scalar*interval (coefficient lb ub)
 (declare (type rational coefficient)
	  (type integer lb ub))
 (cond 
    ((plusp coefficient)
     (values (* lb coefficient) (* ub coefficient)))
    ((minusp coefficient)
     (values (* ub coefficient) (* lb coefficient)))
    (t
     (values 0 0))))

(defun update-lhs-range (range coefficient lb ub &optional decrementp)
  (declare (type (cons rational rational) range)
	   (type integer lb ub)
	   (type rational coefficient))
  (multiple-value-bind (lower upper)
      (scalar*interval coefficient lb ub)
    (if decrementp
	(progn
	  (decf (car range) lower)
	  (decf (cdr range) upper))
	(progn
	  (incf (car range) lower)
	  (incf (cdr range) upper)))))

(defun lhs-range (lrestr lb-vector ub-vector)
  "Compute the range of the left-hand side of LRESTR when 
the variables are in the bounds from LB-VECTOR to UB-VECTOR.
Return the range as a cons (LOWER-RANGE . UPPER-RANGE)."
  (declare (type (simple-array rational) lb-vector ub-vector))	   
  (loop 
     for lb across lb-vector
     for ub across ub-vector
     for coefficient across (linform lrestr)
     with range = (cons 0 0)
     do (update-lhs-range range coefficient lb ub nil)
     finally (return range)))

(defun lhs-range-feasible-p (lrestr lhs-range)
  "Compute whether at least one value in [LOWER-RANGE, UPPER-RANGE]
satisfies LRESTR."
  (declare (type (cons rational rational) lhs-range))
  (destructuring-bind (lower-range . upper-range) lhs-range
    (let ((rhs (rhs lrestr)))
      (ecase (sense lrestr)
	((lrestr::==)
	 (<= lower-range rhs upper-range))
	((lrestr::<=) 
	 (<= lower-range rhs))
	((lrestr::<)
	 (< lower-range rhs))
	((lrestr::>=)
	 (>= upper-range rhs))
	((lrestr::>)
	 (> upper-range rhs))))))  

(defun vint-outer (p)
  (declare (type h-poly p))
  (let ((lb (lower-bounds p))
	(ub (upper-bounds p))
	(adim (the array-index (poly::adim p))))
    (unless (and lb ub)
      ;; FIXME: if we have cplex-glue (maybe with glpk-backend) we could
      ;;        compute some bounds here
      ;; FIXME: we could in fact compute IP-bounds (maybe with time-limit)
      ;; FIXME: We could even have LP/IP row range computations
      (error "Need lower and upper bounds for enumerating integral points"))
    (let* ((constraints (concatenate 'vector (ieq-rows p) (eq-rows p)))
	   (solution (make-array adim :element-type 'integer
				 :initial-element 0))
	   (lhs-ranges (make-array (length constraints)))
	   (solution-list '())
	   (solution-list-length 0) ;; for progress reports
	   (solution-list-length-lastreport 0))
      (declare (type integer solution-list-length solution-list-length-lastreport))
      (loop for constraint across constraints
	 for index of-type array-index from 0
	 do (setf (svref lhs-ranges index)
		  (lhs-range constraint lb ub)))
      (labels ((enumerate-coordinate (col)
		 (declare (type array-index col))
		 (cond 
		   ((notevery #'lhs-range-feasible-p 
			      constraints lhs-ranges))
		   ((>= col adim)
		    (push (copy-seq solution) solution-list)
		    (incf solution-list-length)
		    (when (and (= 0 (mod solution-list-length 100))
			       (>= solution-list-length
				(* 2 solution-list-length-lastreport)))
			(format *error-output* "~D... " solution-list-length)
			(setq solution-list-length-lastreport solution-list-length)))
		   (t
		    (loop 
		       initially
			 (loop for constraint across constraints
			    for lhs-range across lhs-ranges
			    do (update-lhs-range lhs-range
						 (svref (linform constraint) col)
						 (svref lb col) (svref ub col)
						 t))
		       for val of-type integer from (svref lb col) upto (svref ub col)
		       do (progn
			    (setf (svref solution col) val)
			    (loop for constraint across constraints
			       for lhs-range across lhs-ranges
			       do (update-lhs-range lhs-range
						    (svref (linform constraint) col)
						    val val nil))
			    (enumerate-coordinate (1+ col))
			    (loop for constraint across constraints
			       for lhs-range across lhs-ranges
			       do (update-lhs-range lhs-range
						(svref (linform constraint) col)
						val val t)))
		       finally
			 (loop for constraint across constraints
			    for lhs-range across lhs-ranges
			    do (update-lhs-range lhs-range
						 (svref (linform constraint) col)
						 (svref lb col) (svref ub col)
						 nil)))))))
	(enumerate-coordinate 0))
      (format *error-output* "done.~%")
      (make-v-poly adim (nreverse solution-list) '()))))

