;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10               -*-
;;;; ********************************************************************
;;;;
;;;; things to do in the fresh allegro core while dumping image
;;;;
;;;; (c) 2006 Utz-Uwe Haus, <atrop@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; ********************************************************************


(asdf:operate 'asdf:load-op '#:atrop)

(setq EXCL:*global-gc-behavior* :auto ;; does not really make it into the dumped image
      COMMON-LISP:*load-verbose* NIL)

;; silence GC
(setf (sys:gsgc-switch :print) nil)
