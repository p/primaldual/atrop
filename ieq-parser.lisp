;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; package: ieq-parser -*-
;;;; ***************************************************************************
;;;;
;;;; IEQ format parser.
;;;;
;;;; (c) 2004-2006 Utz-Uwe Haus, <atrop@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; ***************************************************************************

(eval-when
    (:compile-toplevel :load-toplevel)
  (declaim (optimize (safety 1) (debug 1) (speed 3))
	   ))

(defpackage #:IEQ-PARSER
  (:documentation "Parser for IEQ format ($Revision$, $Date$).")
  (:use #:COMMON-LISP #:ATROP-UTILS #:PARSER)
  (:export 
   #:parse-ieq-format
   ;; conditions
   #:ieq-parser-error
   #:ieq-parser-warning
   )
  )
;;;; TODO: parsing of VALID, LOWER_BOUNDS, UPPER_BOUNDS sections, and rows without line number

(in-package #:IEQ-PARSER)

;;;;; The ieq format is ugly.

(defclass ieq-parser-source (parser-source)
  ((hdim :initform 0 :type (integer 0) :accessor hdim)
   (ieqlist :initform NIL
	    :type list
	    :accessor ieqlist)
   (eorder :initform NIL
	   :type (or null (array integer *))
	   :accessor eorder)
   (ub     :initform NIL
	   :type (or null (array rational *))
	   :accessor ub)
   (lb     :initform NIL
	   :type (or null (array rational *))
	   :accessor lb)
   (valid-point :initform NIL
		:type (or null (array rational *))
		:accessor valid-point)
   (obj :initform NIL
	:type (or null (array rational *))
	:accessor obj)
   (objsense :initform NIL
	     :type (member 'MAX 'MIN NIL)
	     :accessor objsense)
   ))

;; FIXME: handle lower-level parser conditions by changing them to our conditions
(define-condition ieq-parser-error (parser-error)
  ()
  (:report (lambda (condition s)
	     (declare (type stream s))
	     (format s "IEQ-Parser error at (~D,~D): ~A~%"
		     (parser::line (parser::parser-error-parser-obj condition))
		     (parser::col (parser::parser-error-parser-obj condition))
		     (parser::parser-error-message condition)))))

(define-condition ieq-parser-warning (parser-warning)
  ()
  (:report (lambda (condition s)
	     (declare (type stream s))
	     (format s "IEQ-Parser warning at (~D,~D): ~A~%"
		     (parser::line (parser::parser-warning-parser-obj condition))
		     (parser::col (parser::parser-warning-parser-obj condition))
		     (parser::parser-warning-message condition)))))


(defmethod ieq-parser-error ((src ieq-parser-source) (message string))
  (signal 'ieq-parser-error :message message :parser-obj src))
(defmethod ieq-parser-warning ((src ieq-parser-source) (message string))
  (warn 'ieq-parser-warning :message message :parser-obj src))

;;; externally visible function:
(defun parse-ieq-format (&optional (file t))
  "Parse IEQ format from FILE, which may be a pathname, a string or a
stream, otherwise use stdin by default."
  (restart-case
   (handler-bind ((ieq-parser-error #'(lambda (condition)
					(format t "Parsing ~A failed.~%~A~%" file condition)
					(invoke-restart 'give-up NIL)))
		  (ieq-parser-warning #'(lambda (condition)
					  (format t "~A" condition)
					  (invoke-restart 'muffle-warning))))
     (with-open-file-or-stream (istream file :direction :input)
       (let ((*break-on-signals* nil
	                         ;t
	       ))
	 (let ((i (make-instance 'ieq-parser-source :stream istream)))
	   (parse-ieq i)))))
    (give-up (&optional v)  (declare (ignore v))
	     (error "Parsing IEQ file failed."))))


;; internally parsing is handled by this method:
(defmethod parse-ieq ((src ieq-parser-source))
  "Parse IEQ file from SRC. Returns a list of 7 components:
DIM IEQ-LIST LB UB ELIM-ORDER VALID-POINT OBJECTIVE.
Except for DIM and IEQLIST all these may be NIL if they were not specified in the input file."
  ;; first: dimension line
  (skip-empty-lines src)
  (setf (hdim src) (parse-ieq-dim src))
  (if (<= (the integer (hdim src)) 0)
      (ieq-parser-error src "Cannot handle ieq files with DIM <= 0."))
  (do ((next-section (identify-section src) (identify-section src)))
      ((not next-section)
       (ieq-parser-error src "Cannot find next section or END"))
    (skip-empty-lines src)
    (case next-section
      ('(CONV_SECTION CONE_SECTION)
       (ieq-parser-error src "CONV_SECTION and CONE_SECTION invalid in IEQ files."))
      ('VALID
       ;(format t "Found VALID section.~%")
       (setf (valid-point src) (parse-validsec src)))
      ('COMMENT
       ;(format t "Found COMMENT section.~%")
       (skip-comment-section src))
      ('LOWER_BOUNDS
       ;(format t "Found LB section.~%")
       (setf (lb src) (parse-boundsec src)))
      ('UPPER_BOUNDS
       ;(format t "Found UB section.~%")
       (setf (ub src) (parse-boundsec src)))
      ('INEQUALITIES_SECTION
       ;(format t "Found IEQ section.~%")
       (setf (ieqlist src) (parse-inequalities src)))
      ('ELIMINATION_ORDER
       ;(format t "Found EORDER section.~%")
        (setf (eorder src) (parse-elimorder src)))
      ('OBJECTIVE
       (format t "FIXME: should now parse OBJECTIVE~%")
       ;(format t "No objective section, but that's okay.~%")
       )
      ('END
       (if (not (ieqlist src))
	   (ieq-parser-error src "IEQ file is missing INEQUALITIES_SECTION."))
       (return-from nil))
      (t
       (ieq-parser-error src (format nil "Unknown section keyword `~A'." next-section)))))
  ;; return results
  (list (hdim src) (ieqlist src) (lb src) (ub src) (eorder src) (valid-point src) (obj src)))





;; helper functions

(defmethod parse-ieq-number ((src ieq-parser-source))
  "Read a line-spec number in SRC or throw up."
  (must-read src #\( nil t)
  (let ((num (parse-uint src)))
    (must-read src #\) t nil)
    num))


  
(defmethod parse-num/var-pair ((src ieq-parser-source) &optional (require-sign t))
  "Parse token of the type [sign] [number[/number]] x{index}. Sign is required if REQUIRE-SIGN is not NIL.
 x{index} must be one of x1..x{(hdim src)} inclusively."
  (declare (type boolean require-sign))
  (skip-space src)
  (let ((num 1)
	(deno 1)
	(idx 0))
    (declare (type integer num deno idx))
    
    (setq num (parse-sign src require-sign))
    
    (skip-space src)
    (let ((v (parse-uint src nil -1)))
      (declare (type (integer -1) v))
      (if (>= v 0)
	  ;; found numerator, try denominator next
	  (let ((c (progn (skip-space src)
			  (read-a-char src))))
	    (if (char-equal c #\/)
		;; found `/', so try denominator next
		(let ((w (progn (skip-space src)
				(parse-uint src nil -1))))
		  (declare (type (integer -1) w))
		  (if (< w 0)
		      (progn
			(ieq-parser-warning src "denominator defaults to 1.")
			(setq w 1)))
		  (setq deno w))
	      (unread-a-char src c))
	    (setq num (* num v)))
	;; no numerator -> immediately try finding x{index}
	))
    (must-read src #\x t nil nil)
    (let ((i (parse-uint src nil -1)))
      (declare (type (integer -1) i))
      (if (< i 0)
	  (ieq-parser-error src "Cannot parse variable index."))
      (if (or (< i 1)
	      (> i (hdim src)))
	  (ieq-parser-error src (format NIL "Index ~D invalid when DIM is ~D." i (hdim src))))
      (setq idx i))
    (cons  idx (the rational (/ num deno)))))

(defmethod parse-num/var-pairs ((src ieq-parser-source))
  "Parse tokens of the type [sign] [number[/number]] x{index}. Returns a list of
varname/coefficient pairs, which need not be ordered."
  (do* ((term (parse-num/var-pair src nil) (parse-num/var-pair src))
	(res (cons term '()) (cons term res))
	(next-char (progn (skip-space src) (peek-a-char src)) (progn (skip-space src) (peek-a-char src))))
      ((not (member next-char '(#\+ #\-))) ;; term not followed by sign -> assume end of linear form
       (reverse res))))

(defmethod parse-sense ((src ieq-parser-source))
  (declare (ftype (function (ieq-parser-source) symbol) parse-sense))
  "Parse <, <=, ==, >= , or > row sense indicator. Returns it as a symbol.
Also accepts = to mean == (which is `non-standard' but often appears in 
users's ieq files."
  (skip-space src)
  (let* ((c1 (read-a-char src))
	 (c2 (read-a-char src)))
    (if (char= c2 #\=)
	(cond
	  ((char= c1 #\<) '<=)
	  ((char= c1 #\>) '>=)
	  ((char= c1 #\=) '=)
	  (t (ieq-parser-error src "Row sense must be `<', `<=', `>=', `>', or `=='.")))
	(progn
	  (unread-a-char src c2)
	  (cond
	    ((char= c1 #\<) '<)
	    ((char= c1 #\>) '>)
	    ((char= c1 #\=) '=)
	    (t (ieq-parser-error src "Row sense must be `<', `<=', `>=', `>', or `=='.")))))))

(defmethod parse-rhs ((src ieq-parser-source))
  (parse-rational src t))


(defmethod parse-inequalities ((src ieq-parser-source))
  "Parse lines from SRC until the beginning does not look like either a line-number spec or a number. Skip blank lines.
Returns a list of inequalities, in the same order as in the input file."
  (do ((cont t)
       (ieqs '()))
      ((not cont) (reverse ieqs))
    (skip-space src)
    (let ((c (read-a-char src)))
      (cond
       ((char= c #\Newline)
	(progn (unread-a-char src c)
	       (skip-to-next-line src)))
       ((and
	 (not (member (the character c) '(#\- #\+ #\( #\x) :test #'char=))
	 (not (digit-char-p c)))
	(progn (unread-a-char src c) (setq cont NIL)))
       (t
	;; this char is for us
	(unread-a-char src c)
	(if (char= c #\()
	    ;; An ieq number. We ignore it.
	    (let ((ieqnum (parse-ieq-number src)))
	      (declare (ignore ieqnum))
	      ;;(format t "parsing ieq line ~D...~%" ieqnum)
	      )
	  (skip-space src))
	(let* ((c/c-list (parse-num/var-pairs src))
	       (sense (parse-sense src))
	       (rhs (parse-rhs src)))
	  (skip-to-next-line src)
	  ;;(format t "parsed ~A~%" (cons (list c/c-list sense rhs) ieqs))
	  (setq ieqs (cons (list c/c-list sense rhs)
			   ieqs))
	  ))))))

(defmethod parse-elimorder ((src ieq-parser-source))
  (let ((result 
	 (make-array (the integer (hdim src))
		     :element-type 'array-index
		     :initial-element 0
		     :adjustable NIL)))
    (skip-space src)
    (do ((idx 0 (1+ idx))
	 (val (parse-uint src nil -1) (progn
					(skip-space src)
					(parse-uint src nil -1))))
	((< val 0) result)
      (declare (type array-index idx)
	       (type (or (integer -1 -1) array-index) val))
      (setf (aref result idx) val))))

(defmethod parse-validsec ((src ieq-parser-source))
  "Read (hdim src) rationals in a row."
  (parse-N-rationals src (hdim src)))
  
(defmethod parse-boundsec ((src ieq-parser-source))
  "Read (hdim src) rationals in a row."
  (parse-N-rationals src (hdim src)))
  
				  

(defmethod parse-ieq-dim ((src ieq-parser-source))
  "Parse a DIM = <dim> line. Next input line must contain it, or a parse error is signaled. Returns -1 on failure."
  (must-read src "DIM" nil t nil)
  (must-read src #\= nil t)
  (let ((dim (parse-uint src nil nil)))
    (if (not (numberp dim))
	(ieq-parser-error src "`DIM =' must be followed by a number."))
    (skip-to-next-line src)
    dim))

(defmethod parse-ieq-objective ((src ieq-parser-source))
  'FIXME
  )

(defmethod identify-section ((src ieq-parser-source))
  "Move to next non-empty line and identify whether it contains a section keyword
for IEQ or POI files keyword.
Returns NIL if not, otherwise one of the following symbols:
VALID, COMMENT, INEQUALITIES_SECTION, LOWER_BOUNDS, UPPER_BOUNDS, OBJECTIVE,
ELIMINATION_ORDER, CONE_SECTION, CONV_SECTION, END.
Advances the input to the start of the line following the keyword,
or to the start of the non-keyword line of NIL is returned."
  (skip-empty-lines src)
  (let ((c (read-a-char src))
	(answer (lambda (symbol) (skip-to-next-line src) symbol)))
    ;(format t "identify-section: dispatching on |~A|~%" c)
    (case (char-upcase c)
      (#\V (funcall answer 'VALID))
      (#\C (funcall answer 'COMMENT))
      (#\I (funcall answer 'INEQUALITIES_SECTION))
      (#\L (funcall answer 'LOWER_BOUNDS))
      (#\U (funcall answer 'UPPER_BOUNDS))
      (#\O (funcall answer 'OBJECTIVE))
      (#\E (let ((cc (read-a-char src)))
	     ;;(format t "identify-section [EORDER?]: dispatching on |~A|~%" cc)
	     (case (char-upcase cc)
	       (#\L (funcall answer 'ELIMINATION_ORDER))
	       (#\N (funcall answer 'END))
	       (t (progn
		    (unread-a-char src cc)
		    (unread-a-char src c)
		    NIL)))))
      (#\C (let* ((co (read-a-char src))
		  (con (read-a-char src))
		  (conx (read-a-char src)))
	     ;; check common chars
	     (if (not (and (char= co  #\O)
			   (char= con #\N)))
		 ;; bail out
		 (progn
		   (unread-a-char src conx)
		   (unread-a-char src con)
		   (unread-a-char src co)
		   (unread-a-char src c)
		   NIL)
		 ;; check discriminating char
		 (case (char-upcase conx)
		   (#\E (funcall answer 'CONE_SECTION))
		   (#\V (funcall answer 'CONV_SECTION))
		   (t (progn
			(unread-a-char src conx) (unread-a-char src con)
			(unread-a-char src co) (unread-a-char src c)
			NIL))))))
      (otherwise 
       (ieq-parser-error src 
			 (format nil "Syntax error in IEQ file, recently read `~A'" c))))))

(defmethod skip-comment-section ((src ieq-parser-source))
  "Skip over lines until we see a new section keyword."
  (skip-empty-lines src)
  (let ((next-section (identify-section src)))
    (if next-section
	(unread-string src (format nil "~%~A~%" next-section))
	(progn 
	  (skip-to-next-line src)
	  (skip-comment-section src)))))
