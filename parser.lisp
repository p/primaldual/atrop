;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; package: parser -*-
;;;; ***************************************************************************
;;;;
;;;; Parser utilities.
;;;;
;;;; (c) 2005 Utz-Uwe Haus, <atrop@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; ***************************************************************************

(eval-when
    (:compile-toplevel :load-toplevel)
  (declaim (optimize (safety 1) (debug 1) (speed 3))
	   (inline #:read-a-char #:unread-a-char #:advance-filepos #:peek-a-char)))

(defpackage #:PARSER
  (:documentation "Parser class ($Revision$, $Date$).")
  (:use #:COMMON-LISP)
  (:export 
   ;; class
   #:parser-source
   ;; basic methods
   #:read-a-char #:unread-a-char
   #:read-word #:unread-string
   #:peek-a-char
   #:skip-chars-in-list #:skip-space
   #:skip-to-next-line
   #:skip-empty-lines
   
   ;; advanced methods
   #:must-read
   #:parse-uint #:parse-sign #:parse-rational
   #:parse-N-rationals

   ;; conditions and identically-named ctors
   #:parser-error #:parser-warning #:parser-eof   

   ;;
   )
  )

(in-package #:PARSER)

(defclass parser-source ()
  ((istream :initarg :stream
	    :reader istream)
   (unread-stack :initform '()
		 :accessor unread-stack)
   (line :initform 1
	 :type (integer 0)
	 :accessor line)
   (col :initform 1
	:type (integer 0)
	:accessor col)
   ))

(define-condition parser-error (error)
  ((message :initarg :message
	    :reader parser-error-message)
   (parser-obj :initarg :parser-obj
	       :reader parser-error-parser-obj))
  (:report (lambda (condition s)
	     (declare (type stream s))
	     (format s "Parser error at (~D,~D): ~A~%"
		     (line (parser-error-parser-obj condition))
		     (col (parser-error-parser-obj condition))
		     (parser-error-message condition)))))

(define-condition parser-warning (warning)
  ((message :initarg :message
	    :reader parser-warning-message)
   (parser-obj :initarg :parser-obj
	       :reader parser-warning-parser-obj))
  (:report (lambda (condition s)
	     (declare (type stream s))
	     (format s "Parser warning at (~D,~D): ~A~%"
		     (line (parser-warning-parser-obj condition))
		     (col (parser-warning-parser-obj condition))
		     (parser-warning-message condition)))))

(define-condition parser-eof (condition)
  ((parser-obj :initarg :parser-obj
	       :reader parser-eof-parser-obj))
  (:report (lambda (condition s)
	     (declare (type stream s))
	     (format s "Parser reached EOF at (~D,~D)~%"
		     (line (parser-eof-parser-obj condition))
		     (col (parser-eof-parser-obj condition))))))

(defmethod parser-error ((src parser-source) (message string))
  (signal 'parser-error :message message :parser-obj src))

(defmethod parser-warning ((src parser-source) (message string))
  (signal 'parser-warning :message message :parser-obj src))

(defmacro suppressing-eof (&body body)
  (let ((restart-label (gensym "bail-out")))
    `(restart-case 
	 (handler-bind ((parser-eof #'(lambda (c)
					(declare (ignore c))
					(invoke-restart ',restart-label))))
	   (progn
	     ,@body))
       (,restart-label () (values)))))

(defmethod advance-filepos ((src parser-source) &key (lines 0) (chars 0))
  (declare (type integer lines chars))
  (setf (line src) (+ (line src) lines))
  (setf (col src)  (+ (col src) chars)))

(defmethod read-a-char ((src parser-source))
  "Read a single character from SRC. Signals a parser-eof condition on EOF."
  (let ((c (or (pop (unread-stack src))
	       (read-char (istream src) nil nil))))
    (when (not c)
      (signal 'parser-eof :parser-obj src))
    (if (char-equal c #\Newline)
	(progn
	  (setf (col src) 0) ;; for resync purposes after newlines that come from unreading
	  (advance-filepos src :lines 1))
	(advance-filepos src :chars 1))	
    ;;(format t "read a `~A'; now at (~A,~A)~%" c (line src) (col src))
    (the character c)))

(defmethod peek-a-char ((src parser-source))
  "Return the next single character from SRC, but do not remove it from SRC.
Signals a parser-eof condition on EOF."
  (let ((c (or (car (unread-stack src))
	       (peek-char nil (istream src) nil nil))))
    (when (not c)
      (signal 'parser-eof :parser-obj src))
    ;(format t "peeked at `~A'~%" c)
    (the character c)))

(defmethod unread-a-char ((src parser-source) (c character))
  "Push back a single character to SRC."
  (push c (unread-stack src))
  (if (char-equal c #\Newline)
      (advance-filepos src :lines -1 :chars (- (col src)))
      (advance-filepos src :chars -1))
  ;(format t "unread a `~C'; now at (~A,~A)~%" c (line src) (col src))
  (the character c))


(defmethod read-word ((src parser-source) 
		      &optional (delimiters '(#\Space #\, #\Newline #\Tab #\. #\?)))
  "Read a word from SRC, where word delimiters are all characters in the list
DELIMITERS.
DELIMITERS defaults to  #\Space #\, #\Newline #\Tab #\. #\?"
  (let ((l '()))
    (restart-case 
	(handler-bind ((parser-eof #'(lambda (c)
				       (declare (ignore c))
				       (invoke-restart 'return-the-word))))
	  (do ((c (read-a-char src) (read-a-char src)))
	      (nil)
	    (if (member c delimiters :test #'char=)
		(progn (unread-a-char src c)
		       (invoke-restart 'return-the-word))
		(push c l))))
      (return-the-word ()
	(coerce (nreverse l) 'string)))))


(defmethod unread-string ((src parser-source) (s string))
  "Push back the string S to SRC."
  (declare (type simple-string s))
  (loop for i from (- (length s) 1) downto 0
       do (unread-a-char src (schar s i))))

(defmethod skip-chars-in-list ((src parser-source) (character-list list))
  "Skip characters listed CHARACTER-LIST."
  (suppressing-eof 
   (do ((c (read-a-char src) (read-a-char src)))
       ((not (member c character-list :test #'char=)) (unread-a-char src c))
     )))

(defmethod skip-space ((src parser-source))
  "Skip #\Space and #\Tab characters."
  (skip-chars-in-list src '(#\Space #\Tab)))

(defmethod must-read ((src parser-source) (c character)
		      &optional
		      (skip-leading-whitespace nil)
		      (skip-trailing-whitespace nil)
		      (case-sensitive t))
  "Read character C from SRC.
 Optionally skip leading and/or trailing whitespace. Obey case if CASE-SENSITIVE."
  (if skip-leading-whitespace
      (skip-space src))
  (let ((x (read-a-char src)))
    (unless (if case-sensitive
		(char= c x)
		(char-equal c x))
      (progn
	;;(format *error-output* "parser-error~%")
	(parser-error src (format nil "Expected a '~C' character." c)))
      ))
  (if skip-trailing-whitespace
      (skip-space src))
  c)

(defmethod must-read ((src parser-source) (s string)
		      &optional
		      (skip-leading-whitespace nil)
		      (skip-trailing-whitespace nil)
		      (case-sensitive t))
  "Read string S from SRC. Optionally skip leading and/or trailing whitespace.
If CASE-SENSITIVE, obey case."
  (if skip-leading-whitespace
      (skip-space src))
  (with-input-from-string (word s)
    (do ((c (read-char word nil nil) (read-char word nil nil)))
	((not c))
      (must-read src c nil nil case-sensitive)))
  (if skip-trailing-whitespace
      (skip-space src)))

(defmethod skip-empty-lines ((src parser-source))
  "Skip over empty lines. Stops at the first non-space character of the first
nonempty line."
  (suppressing-eof
   (skip-space src)
   (do ((c (peek-a-char src) (peek-a-char src)))
       ((not (char= c #\Newline)))
     (skip-to-next-line src)
     (skip-space src))))

(defparameter *digit-lookup-table*
  (let ((table
	 (make-array char-code-limit :element-type 'integer
		     :adjustable nil :fill-pointer nil)))
    (dolist (d '((#\0 . 0) (#\1 . 1) (#\2 . 2) (#\3 . 3) (#\4 . 4)
		 (#\5 . 5) (#\6 . 6) (#\7 . 7) (#\8 . 8) (#\9 . 9)))
      (setf (svref table (char-code (car d))) (cdr d)))
    table)
  "A simple-vector for looking up the numerical value of a digit-char.")

(defmethod parse-uint ((src parser-source)
		       &optional (no-number-errorp t) (default-value -1))
  "Read an unsigned integer from SRC.
 If NON-NUMBER-ERRORP is T, not finding a number is not an error and this function
returns DEFAULT-VALUE (which defaults to -1). The erroneous character is not consumed."
  (do ((num 0 (+ (* 10 num) (the (integer 0 9)
			      (svref *digit-lookup-table* (char-code d)))))
       (digit-count 0 (1+ digit-count))
       (d (read-a-char src) (read-a-char src)))
      ((not (digit-char-p d)) (if (= 0 digit-count)
				  (if no-number-errorp
				      (parser-error src "Missing unsigned integer.")
				      (progn (unread-a-char src d)
					     (the integer default-value)))
				  (progn (unread-a-char src d)
					 (the integer num))))
    (declare (type (integer 0) num digit-count))
    ))

(defmethod skip-to-next-line ((src parser-source))
  "Skip rest of this line, gobble up newline."
  (suppressing-eof
    (do ((c (read-a-char src) (read-a-char src)))
	((char= c #\Newline) t))
    (values)))

(defmethod parse-sign ((src parser-source) &optional (no-sign-errorp nil))
  "Parse a `+' or `-', skipping leading whitespace. `+' is returned as +1, `-' as -1.
If NO-SIGN-ERRORP is non-nil, not finding one is an error.
 If no sign is found and no error is signaled +1 is returned. "
  (skip-space src)
  (let ((s (read-a-char src)))
    (if (not (member s '( #\+ #\-)))
	(if no-sign-errorp
	    (parser-error src "Need sign `+' or `-' here.")
	  (progn
	    (unread-a-char src s)
	    +1))
      (if (char-equal s #\+) +1 -1))))
  
(defmethod parse-rational ((src parser-source) &optional (allow-emptyp t))
  "Parse a (possibly signed) rational number.
If ALLOW-EMPTYP is T (which is the default), an empty
term (meaning 1) is allowed."
  (let ((num 0)
	(deno 0)
	(sign 1))
    (declare (type integer num deno)
	     (type fixnum sign))
    (setq sign (parse-sign src nil))
    (setq num (progn (skip-space src)
		     (parse-uint src nil -1)))
    (if (= num -1)
	(if allow-emptyp
	    (setq num 1)
	    (parser-error src (format nil "Expected a rational number, looking at ~C." (peek-a-char src)))))
    (skip-space src)
    (let ((c (read-a-char src)))
      (if (not (char-equal c #\/))
	  ;; we assume that the number has no denominator
	  (progn
	    ;(format t "Defaulting in parse-rational, looking at `~C'." c)
	    (unread-a-char src c)
	    (setq deno 1))
	(progn
	  (skip-space src)
	  ;; we assume that <number> '/' is to be interpreted as <number> '/' 1
	  (setq deno (parse-uint src nil 1)))))
    ;; return rational
    (the rational (/ (* sign num) deno))))

(defmethod parse-N-rationals ((src parser-source) (N fixnum))
  "Parse N rationals in a single input line, and return them as
a (simple-vector rational N)."
  (let ((result 
	 (make-array N
		     :element-type 'rational
		     :initial-element 0
		     :adjustable NIL)))
    (loop for idx from 0 below N
       do (skip-space src)
       do (let ((val (parse-rational src NIL)))
	    (setf (svref result idx) val)))
    result))
    
