;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; package: atrop-utils -*-
;;;; ****************************************************************************
;;;;
;;;; Dispatcher for command line binaries
;;;;
;;;; (c) 2005 Utz-Uwe Haus, <atrop@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; ****************************************************************************

(eval-when (:compile-toplevel :load-toplevel :execute)
  (declaim (optimize (safety 3) (debug 3) (speed 1))))

(defpackage #:atrop-utils
  (:documentation "Utility routines ($Revision$, $Date$).")
  (:use #:COMMON-LISP)
  (:export #:with-open-file-or-stream #:iota
	   #:print-package-revisions
	   #:getenv
	   #:quit-with-status
	   #:array-index)
  )

(in-package #:atrop-utils)

(defun open-file-or-stream (dst &key (direction :io))
  "Open a file or a stream designated by DST.
Return a stream and a flag indicating whether the stream must be closed.
If DST is already a stream, return it.
If DST is T, return a stream corresponding to standard-input or output.
If DST is a pathname or a string, opens the file DST, and returns the stream.
Understands the :direction keyword :input, :output and :io, like (open) does.
Default is :io.
Existing files will be :superseded."
  (assert (member direction '(:input :output :io)))
  (cond
   ((or (pathnamep dst) (stringp dst))
    (values (open dst :direction direction :if-exists :supersede)
	    t))
   ((streamp dst)
    (values dst nil))
   ((eq T dst)
    (values (cond
	     ((eq :input direction)
	      *standard-input*)
	     ((eq :output direction)
	      *standard-output*)
	     ((eq :io direction)    ; perhaps *terminal-io*? --mkoeppe
	      (make-two-way-stream *standard-input*
				   *standard-output*)))
	    nil))
   (T
    (error "Need a pathname, a string, a stream, or the symbol T, got ~A"
	   dst))))

(defmacro with-open-file-or-stream ((dstdesig dst &key (direction :io))
			       &body body)
  "Execute BODY with DSTDESIG bound to a stream.
If DST is a stream, binds it to DSTDESIG.
If DST is T, binds *standard-output* to DSTDESIG.
If DST is a pathname or a string, opens the file DST, binds the resulting stream to DSTDESIG,
and closes the file after excuting body.
Understands the :direction keyword :input, :output and :io, like (open) does.
Default is :io.
Existing files will be :superseded.
"
  (let ((have-file (gensym "HAVE-FILE")))
    `(multiple-value-bind (,dstdesig ,have-file)
	 (open-file-or-stream ,dst :direction ,direction)
       (declare (type stream ,dstdesig))
       (unwind-protect
	   (progn
	     ,@body)
	 (if ,have-file (close ,dstdesig))))))

(defun iota (count &key (start 0) (increment 1))
  "The full-featured iota function."
  (declare (type (integer 0) count)
	   (type integer start increment))
  (loop for i of-type integer from start
     below (+ start (* count increment))
     by increment
     collect i))

(defun strip-char (s c)
  "Remove all occurences of C from string S."
  (declare (type string s))
  (remove-if #'(lambda (x) (char= c x)) s))

(defun print-package-revisions (&key (dst t))
  "Fetch documentation strings for all packages,
and print those that contain RCS/CVS keywords to DST."
  ;; FIXME: keywords are identified by looking for a substring between $ and $ only
  (let ((package-docs
	 (map 'list #'(lambda (p)
			(concatenate 'string
				     " " (package-name p)
				     ": " (documentation p t)))
	      (list-all-packages))))
    (format dst "~%atrop engine built with the following package revisions:~%")
    (dolist (s package-docs)
      (if (stringp s)
	  (let ((left-$ (position #\$ s))
		(right-$ (position #\$ s :from-end t)))
	    (if (and left-$ right-$ (> (- right-$ left-$) 0))
		(format dst " ~A~%" (strip-char s #\$))))))))

(deftype array-index ()
  "The allowable range of indices for accessing arrays."
  `(integer 0 #.ARRAY-DIMENSION-LIMIT))


;; stolen from CLOCC's sys.lisp:
(defun getenv (var)
  "Portability-wrapper for Unix getenv:
   Return the value of the environment variable."
  #+allegro (sys::getenv (string var))
  #+clisp (ext:getenv (string var))
  #+(or cmu scl)
  (cdr (assoc (string var) ext:*environment-list* :test #'equalp
              :key #'string))
  #+gcl (si:getenv (string var))
  #+lispworks (lw:environment-variable (string var))
  #+lucid (lcl:environment-variable (string var))
  #+mcl (ccl::getenv var)
  #+sbcl (sb-ext:posix-getenv var)
  #-(or allegro clisp cmu gcl lispworks lucid mcl sbcl scl)
  (error 'not-implemented :proc (list 'getenv var)))

(defun (setf getenv) (val var)
  "Set an environment variable."
  #+allegro (setf (sys::getenv (string var)) (string val))
  #+clisp (setf (ext:getenv (string var)) (string val))
  #+(or cmu scl)
  (let ((cell (assoc (string var) ext:*environment-list* :test #'equalp
                     :key #'string)))
    (if cell
        (setf (cdr cell) (string val))
        (push (cons (intern (string var) "KEYWORD") (string val))
              ext:*environment-list*)))
  #+gcl (si:setenv (string var) (string val))
  #+lispworks (setf (lw:environment-variable (string var)) (string val))
  #+lucid (setf (lcl:environment-variable (string var)) (string val))
  #-(or allegro clisp cmu gcl lispworks lucid scl)
  (error 'not-implemented :proc (list '(setf getenv) var val)))


(defun quit-with-status (n)
  "Quit program execution with system status N."
  #+cmu
  (unix:unix-exit n)
  #+sbcl
  (sb-ext:quit :unix-status n)
  #+allegro
  (excl::exit n :quiet T)
  #-(or cmu sbcl allegro)
  (format t "Would quit with status ~D~%" n)
  )

