;;; atrop.el --- IEQ/POI support

;; Copyright 2005 Utz-Uwe Haus

;; Author: Utz-Uwe Haus <atrop@uuhaus.de>
;; Keywords: data, help
;; RCS: $Id$

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; This file provides some emacs support for the ATROP package.
;; Modes for IEQ- and POI-files are defined. 

;;; Code:

(require 'generic)

(define-generic-mode 'ieq-generic-mode
  nil ;; no real comment characters
  ;; keywords:
  (list "DIM" "LOWER_BOUNDS" "UPPER_BOUNDS" "VALID" "COMMENT" "ELIMINATION_ORDER" "INEQUALITIES_SECTION" "END")
  ;; add'l font-lock defs
  (list
   '("\\([<>=]=\\)" 1 font-lock-function-name-face)
   '("\\([<>]\\)" 1 font-lock-function-name-face)
   )
  (list "\\.\\(ieq\\)\\'")
  (list (lambda ()))
  "Generic mode for IEQ files.")


;;; atrop.el ends here
