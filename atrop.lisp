;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; package: atrop -*-
;;;; **********************************************************************
;;;;
;;;; Dispatcher for command line binaries
;;;;
;;;; (c) 2004-2006 Utz-Uwe Haus <atrop@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; **********************************************************************

;; Shell scripts may call the dispatch-atrop function with the environment variable
;; ATROP_COMMAND_LINE set to a suitable string. Convention is:
;;   first word contains the name of the operation to be performed, one of the 
;;   words of *ATROP-CALLS*
;;   rest of string is interpreted as command line arguments as documented for
;;   the individual commands in +ATROP-CALLS+
;; This file is also what has to be loaded for dumping a custom core.
(eval-when (:compile-toplevel :load-toplevel :execute)
  (declaim (optimize (safety 1) (debug 1) (speed 3)))
  )

(defpackage #:atrop
  (:documentation "atrop dispatcher ($Revision$, $Date$).")
  (:use #:COMMON-LISP #:poly #:de.uuhaus.htable #:de.uuhaus.getopt-long #:atrop-utils #:vint #:traf)
  (:export #:dispatch-atrop)
  (:nicknames #:dispatch)
  )
(in-package #:atrop)

(defparameter *ATROP-VERSION* "0.5-prerelease")

(defparameter +ATROP-COMMAND-LINE-VAR+
  "ATROP_COMMAND_LINE"
  "Name of the environment variable used for passing command line.")

(defparameter +ATROP-CALLS+
  '(
    ("fmel"      fmel      ((("read-ine" :NONE)  "Read INE format")
			    (("read-ieq" :NONE)  "Read IEQ format (default)")
			    (("write-ine" :NONE) "Write output in INE format")
			    (("write-ieq" :NONE) "Write output in IEQ format (default)")
			    ) 0 2 "Perform Fourier-Motzkin elimination")
    ("atropsort" atropsort ((("read-ieq" :NONE)  "Read IEQ format")
			    (("read-poi" :NONE)  "Read POI format")
			    (("read-ine" :NONE)  "Read INE format")
			    (("read-ext" :NONE)  "Read EXT format")
			    (("write-ieq" :NONE) "Write output in IEQ format")
			    (("write-poi" :NONE) "Write output in POI format")
			    (("write-ine" :NONE) "Write output in INE format")
			    (("write-ext" :NONE) "Write output in EXT format")
			    ) 0 2 "Bring the problem into a standardized, sorted form.")
    ("dim"       dim       () 0 2 "Compute the dimension of the polyhedron")
    ("vint"      vint      ((("read-ine"   :NONE) "Read INE format")
			    (("read-ieq"   :NONE) "Read IEQ format (default)")
			    (("write-ext"  :NONE) "Write EXT format")
			    (("write-poi"  :NONE) "Write POI format (default)")
			    ) 0 2 "Enumerate all integral points within the polyhedron")
    ("traf"      traf      ((("split-equations" :NONE) "Split all equations")
			    (("read-ieq" :NONE)  "Read IEQ format")
			    (("read-poi" :NONE)  "Read POI format")
			    (("read-ine" :NONE)  "Read INE format")
			    (("read-ext" :NONE)  "Read EXT format")
			    (("write-ieq" :NONE) "Write output in IEQ format")
			    (("write-poi" :NONE) "Write output in POI format")
			    (("write-ine" :NONE) "Write output in INE format")
			    (("write-ext" :NONE) "Write output in EXT format")
			    ) 0 2 "Transform between H- and V-represenatation. Handles IEQ/POI and INE/EXT file formats.")
    ("ieq2ine"   ieq2ine   ((("split-equations" :NONE) "Split all equations")
			    (("ensure-integers" :NONE) "Make all entries integral")
			    ) 0 2 "Convert an IEQ file to INE format.")
    ("ine2ieq"   ine2ieq   ((("split-equations" :NONE) "Split all equations")
			    (("ensure-integers" :NONE) "Make all entries integral")
			    ) 0 2 "Convert an INE file to IEQ format.")
    ("ieq2lp"    ieq2lp    ((("rename-variables" :REQUIRED) "Rename variables using a renaming table")
			    ) 0 2 "Convert an IEQ file to LP format.")
     
    ;; etc
    )
  "The list of externally visible calling stubs. Each list entry is a list of 6 arguments:
The call name (a string), the lisp function to be called, the getopt-list for option processing for this function,
the minimum number of args, the maximum number of args, and a simple description of the function. ")

;; 

(define-condition usage-msg ()
  ((message :initarg :message
	    :reader usage-msg-message
	    :initform NIL))
  (:report (lambda (condition s)
	     (declare (type stream s))
	     (format s "~&~A~%" (usage-msg-message condition)))))
(define-condition usage-error (usage-msg)
  ((command :initarg :command
	    :reader usage-error-command))
  (:report (lambda (condition s)
	     (declare (type stream s))
	     (format s "~&Command ~A not understood.~%Possible command names are ~{~20T~A~%~}~%"
		     (usage-error-command condition)
		     (map 'list #'car +ATROP-CALLS+)))))
(define-condition usage-help (usage-msg)
  ((command :initarg :command
	    :reader usage-help-command))
  (:report (lambda (condition s)
	     (declare (type stream s))
	     ;; report specific error if present
	     (if (usage-msg-message condition)
		 (format s "~&Error: ~A~%" (usage-msg-message condition)))
	     ;; give help
	     (let ((callinfo (assoc (usage-help-command condition) +ATROP-CALLS+ :test #'string=)))
	       (format s "~%Usage: ~A [options] <arguments>~%" (first callinfo))
	       (format s "       ~A~%" (sixth callinfo))	       
	       (format s "~% valid options: ~{~17T~A~&~}~%"
		       (if (null (third callinfo))
			   "None"
			   (map 'list #'(lambda (getopt-def) 
					  (let ((o (caar getopt-def))
						(args? (ecase (cadar getopt-def)
							 (:NONE "")
							 (:OPTIONAL "[arg]")
							 (:REQUIRED " arg ")))
						(doc (cadr getopt-def)))
					    (format NIL "--~20A ~5A (~A)" o args? doc)))
				(third callinfo))))
	       (format s " as well as the universal options --help, --version and --show-lisp-stats~%")
	       (format s " This command accepts ~D to ~D files as arguments.~%" (fourth callinfo) (fifth callinfo))
	       (format s " Use a single `-' instead of a file name for standard input/output.~%")))))


(defun dispatch-atrop (&optional (cmdline nil))
  "External dispatch entry point. Evaluates environment variable ATROP_COMMAND_LINE and calls
appropriate function from *ATROP-CALLS*."
  #+allegro (setq EXCL:*global-gc-behavior* :auto) 
  (handler-case 
      (dispatch-atrop-helper cmdline)
    ;; handlers. Could be more specific
    (file-error (e)
      (progn (format *error-output* "Input/output error: ~A~%" e) 
	     (quit-with-status 1)))
    (ieq-parser-error (e)
      (progn (format *error-output* "~&Error while parsing input file: ~A~%" e)
	     (quit-with-status 1)))
    ;; generic stuff:
    (error (e)
      (progn (format *error-output* "An error occured: ~A~%" e) 
	     (quit-with-status 1)))
    (usage-error (e) (progn (format *error-output* "~A" e) (quit-with-status 1)))
    (usage-help (c)  (progn (format *error-output* "~A" c) (quit-with-status 0)))
    (simple-condition (c) (progn
			    ;; most probably SIGINT
			    (format *error-output* "~A" c)
			    (quit-with-status 1)))			   
    (condition (c)
      (progn (format *error-output* "Some silly condition (type ~A): ~A~%" (type-of c) c)
	     (quit-with-status 2)))
    (:no-error (&rest dummy) (declare (ignore dummy)) (quit-with-status 0))
    )
  )

(defun dispatch-atrop-helper (cmdline)
  "Internal dispatch routine. Does all the work of dispatch-atrop, except for error-handling,
so it is suitable for interactive use in debugging environments."
  (let ((command-line (or cmdline
			  (getenv +ATROP-COMMAND-LINE-VAR+))))
    (unless command-line (error "No command line specified!"))
    (multiple-value-bind (args common-options remaining-options)
	(getopt-long:getopt-long (tokenize-string command-line)
				 '(("show-lisp-stats" :none)
				   ("version" :none)
				   ("help" :none)))
      (declare (ignore remaining-options))
      (let* ((command-name (first args))
	     (command (assoc command-name +ATROP-CALLS+ :test #'string=) )
	     (give-stats-at-end (option-is-set "show-lisp-stats" common-options))
	     (show-version-only (option-is-set "version" common-options))
	     (help-only (option-is-set "help" common-options)))

	(format *error-output* "atrop engine, version ~A~41T(c) 2005/06 Utz-Uwe Haus <atrop@uuhaus.de>~%" *ATROP-VERSION*)
	(format *error-output* "This build uses ~A ~A~%"
		(lisp-implementation-type)
		(lisp-implementation-version))
	#+allegro
	(format *error-output* "Allegro Common Lisp is copyrighted 1985-2004 Franz Inc., Oakland, CA, USA.~%")
	    
	(cond 
	  (show-version-only
	   (print-package-revisions :dst *error-output*))
	  (help-only
	   (signal 'usage-help :command command-name))
	  ((null command)
	   (signal 'usage-error :command command-name))
	  (t
	   (format *error-output* "~%")
	   (let ((function-to-call (second command))
		 (function-args (cdr args))
		 (specific-getopt-options (map 'list #'car (third command))))
	     (multiple-value-bind (dummyargs specific-options unparsed-options)
		 (getopt-long:getopt-long (tokenize-string command-line)
					  specific-getopt-options)
	       (declare (ignore unparsed-options dummyargs))
	       ;(format *error-output* "Calling ~A with ~A and ~A~%" function-to-call function-args specific-options)
	       (funcall function-to-call function-args specific-options)))))
	(if give-stats-at-end
	    (format *error-output* "~%Run time ~,2Fs~A~%Memory stats:~%~A~%"
		    (/ (get-internal-run-time) internal-time-units-per-second)
		    #+cmu
		    (format nil ", ~,2F% GC time." (* 100 (/ (/ (get-internal-run-time) extensions:*gc-run-time*)
							     internal-time-units-per-second)))
		    #+sbcl
		    (format nil ", ~,2F% GC time." (* 100 (/ (/ (get-internal-run-time) SB-EXT::*gc-run-time*)
							     internal-time-units-per-second)))
		    #-(or cmu sbcl)
		    ""
		    (with-output-to-string (*standard-output*)
		      (cl::room))))))))


;;;;; API functions
;;; FMEL
(defun fmel-helper (in in-name out out-name
		    &key
		    (read-ieq T) (read-ine NIL)
		    (write-ieq T) (write-ine NIL) (no-output NIL))
  (declare (type string in-name out-name))
  (format *error-output* "Fourier-Motzkin elimination for ~A~%" in-name)
  ;; sanity checks
  (cond
    ((and read-ieq read-ine)
     (signal 'usage-help :message "Only one of IEQ or INE input options may be specified" :command "fmel"))
    ((not (or read-ieq read-ine))
     (signal 'usage-help :message "Either IEQ or INE input must be selected" :command "fmel"))
    ((and write-ieq write-ine)
     (signal 'usage-help :message "Only one of IEQ or INE output options may be specified" :command "fmel"))
    ((not (or write-ieq write-ine no-output))
     (signal 'usage-help :message  "Either IEQ or INE input must be selected if output is to be generated" :command "fmel")))
  ;;
  (let ((p (cond
	     (read-ieq (make-h-poly-from-ieqfile in))
	     (read-ine (make-h-poly-from-inefile in)))))
    (handler-case 
	(fm-elim p :split-equations? NIL)
      (inconsistent-system (c) 
	(format *error-output* "~%System found to be inconsistent: ~A.~%Stopped." c)))
    (unless no-output
      (cond 
	;; FIXME: write info about the elimination variables in fname-info
	(write-ieq (write-poly-ieq-format p :dst out 
					  :fname-info (concatenate 'string out-name " generated from " in-name)))
	(write-ine (write-poly-ine-format p :dst out
					  :fname-info (concatenate 'string out-name " generated from " in-name)))
	;; lattE?
	)
      (format *error-output* "~&Projected system written to ~A~%" out-name)
      )))

(defun fmel (args options)
  (declare (type list args options))
  (let* ((reading-ine (option-is-set "read-ine" options))
	 (writing-ine (option-is-set "write-ine" options))
	 ;; implictly we prefer IEQ/IEQ:
	 (reading-ieq (or (option-is-set "read-ieq" options)
			  (not reading-ine)))
	 (writing-ieq (or (option-is-set "write-ieq" options)
			  (not writing-ine))))
    (multiple-value-bind (in in-name out out-name)
	(case (length args)
	  (0 ;; read stdin, write stdout
	   (values *standard-input* "standard input"
		   *standard-output* "standard output"))
	  (1 ;; assume it is the input file
	   (if (string= "-" (first args))
	       ;; stdin, and hence also implicityly output to stdout
	       (values *standard-input* "standard input"
		       *standard-output* "standard output")
	       ;; construct default output 
	       (let ((outpath (make-pathname :defaults (merge-pathnames (first args))
					     :type (if writing-ieq 
						       (if reading-ieq "ieq.ieq" "ine.ieq")
						       (if reading-ieq "ieq.ine" "ine.ine")))))
		 (values (merge-pathnames (first args)) (namestring (merge-pathnames (first args)))
			 outpath (namestring outpath)))))
	  (2 ;; 2 file arguments: in and out
	   (multiple-value-bind (in in-name)
	       (if (string= "-" (first args))
		   (values *standard-input* "standard input")
		   (values (merge-pathnames (first args)) (namestring (merge-pathnames (first args)))))
	     (multiple-value-bind (out out-name)
		 (if (string= "-" (second args))
		     (values *standard-output* "standard output")
		     (values (merge-pathnames (second args)) (namestring (merge-pathnames (second args)))))
	       (values in in-name out out-name))))
	  (otherwise (error "Need 0, 1 or 2 filename arguments")))
      (fmel-helper in in-name out out-name
		   :read-ieq reading-ieq :read-ine reading-ine
		   :write-ieq writing-ieq :write-ine writing-ine))))


(defun traf (args options)
  "Transform inner to outer description, or vice-versa. 0, 1 or 2 files as arguments,
later will have options to choose between FM or DD algorithm."
  (declare (type list args options))
  (let* ((reading-inner (or (option-is-set "read-poi" options)
			    (option-is-set "read-ext" options)
			    (option-is-set "write-ieq" options)
			    (option-is-set "write-ine" options)))
	 (reading-outer (or (option-is-set "read-ieq" options)
			    (option-is-set "read-ine" options)
			    (option-is-set "write-poi" options)
			    (option-is-set "write-ext" options)))
	 (know-what-to-read (or reading-inner
				reading-outer
				;; last resort: try to recognize file format by a parsing attempt
				(error "Automatic file type recognition needs to be implemented")))
	 (reading-ine (option-is-set "read-ine" options))
	 (writing-ine (option-is-set "write-ine" options))
	 (reading-ext (option-is-set "read-ext" options))
	 (writing-ext (option-is-set "write-ext" options))
	 ;; implictly we prefer IEQ/POI and POI/IEQ
	 (reading-ieq (or (option-is-set "read-ieq" options)
			  (and (not reading-ine)
			       (not reading-inner))))
	 (reading-poi (or (option-is-set "read-poi" options)
			  (and (not reading-ext)
			       reading-inner)))
	 (writing-ieq (or (option-is-set "write-ieq" options)
			  (and reading-inner
			       (not writing-ine))))
	 (writing-poi (or (option-is-set "write-poi" options)
			  (and (not reading-inner)
			       (not writing-ext))))
	 (splitting-equations (option-is-set "split-equation" options)))
    (multiple-value-bind (in in-name out out-name)
	(case (length args)
	  (0 ;; read stdin, write stdout
	   (values *standard-input* "standard input"
		   *standard-output* "standard output"))
	  (1 ;; assume it is the input file
	   (if (string= "-" (first args))
	       ;; stdin, and hence also implicityly output to stdout
	       (values *standard-input* "standard input"
		       *standard-output* "standard output")
	       ;; construct default output 
	       (let ((outpath (make-pathname :defaults (merge-pathnames (first args))
					     :type (if reading-inner
						       (if writing-ieq
							   (if reading-poi "poi.ieq" "ext.ieq")
							   (if reading-poi "poi.ine" "ext.ine"))
						       (if writing-poi
							   (if reading-ieq "ieq.poi" "ine.poi")
							   (if reading-ieq "ieq.ext" "ine.ext"))))))
		 (values (merge-pathnames (first args)) (namestring (merge-pathnames (first args)))
			 outpath (namestring outpath)))))
	  (2 ;; 2 file arguments: in and out
	   (multiple-value-bind (in in-name)
	       (if (string= "-" (first args))
		   (values *standard-input* "standard input")
		   (values (merge-pathnames (first args)) (namestring (merge-pathnames (first args)))))
	     (multiple-value-bind (out out-name)
		 (if (string= "-" (second args))
		     (values *standard-output* "standard output")
		     (values (merge-pathnames (second args)) (namestring (merge-pathnames (second args)))))
	       (values in in-name out out-name))))
	  (otherwise (error "Need 0, 1 or 2 filename arguments")))
      (if reading-inner
	  (traf-helper-inner in in-name out out-name
			     :read-poi reading-poi :read-ext reading-ext
			     :write-ieq writing-ieq :write-ine writing-ine)
	  (traf-helper-outer in in-name out out-name
			     :read-ieq reading-ieq :read-ine reading-ine
			     :write-poi writing-poi :write-ext writing-ext
			     :split-equations? splitting-equations))
      )))

(defun traf-helper-inner (in in-namestring out out-namestring 
			  &key (read-poi T) (read-ext NIL) (write-ieq T) (write-ine NIL))
  "Traf for inner-to-outer representation"
  (format *error-output* "Transforming inner to outer representation for ~A~%" in-namestring)
  ;; sanity check
  (cond
    ((and read-poi read-ext)
     (signal 'usage-help :message "Only one of POI or EXT input options may be specified" :command "traf"))
    ((not (or read-poi read-ext))
     (signal 'usage-help :message "Either POI or EXT input must be selected" :command "traf"))
    ((and write-ieq write-ine)
     (signal 'usage-help :message "Only one of IEQ or INE output options may be specified" :command "traf"))
    ((not (or write-ieq write-ine))
     (signal 'usage-help :message "Either IEQ or INE output must be selected" :command "traf")))
  
  (let* ((p (cond 
	      (read-poi
	       (make-v-poly-from-poifile in))
	      (read-ext
	       (make-v-poly-from-extfile in))))
	 (q (i2o p)))
    (format *error-output* "~&Outer description given by ~D equations, ~D inequalities~%"
	    (poly::numeqs q) (poly::numieqs q))
    (cond
      (write-ieq
       (write-poly-ieq-format q :dst out))
      (write-ine
       (write-poly-ine-format q :dst out)))
    (format *error-output* "~&Inequality system written to file ~A~%" out-namestring))
  )
(defun traf-helper-outer (in in-namestring out out-namestring 
			  &key (read-ieq T) (read-ine NIL) (write-poi T) (write-ext NIL) (split-equations? NIL))
  "Traf for outer-to-inner representation"
  (format *error-output* "Transforming outer to inner representation for ~A~%" in-namestring)
  ;; sanity check
  (cond
    ((and read-ieq read-ine)
     (signal 'usage-help :message "Only one of IEQ or INE input options may be specified" :command "traf"))
    ((not (or read-ieq read-ine))
     (signal 'usage-help :message "Either IEQ or INE input must be selected" :command "traf"))
    ((and write-poi write-ext)
     (signal 'usage-help :message "Only one of POI or EXT output options may be specified" :command "traf"))
    ((not (or write-poi write-ext))
     (signal 'usage-help :message "Either POI or EXT output must be selected" :command "traf")))
  
  (let* ((p (cond 
	      (read-ieq
	       (make-h-poly-from-ieqfile in))
	      (read-ine
	       (make-h-poly-from-inefile in))))
	 (dummy (write-poly-ine-format p :fname-info "recently read ine"))
	 (dummy2 (write-poly-ine-format (split-equations p) :fname-info "after-splitting eqs"))
	 (q (o2i (if split-equations?
		     (split-equations p)
		     p))))
    (format *error-output* "~&Inner description given by ~D points, ~D rays~%"
	    (poly-points q) (poly-rays q))
    (cond
      (write-poi
       (write-poly-poi-format q :dst out))
      (write-ext
       (write-poly-ext-format q :dst out)))
    (format *error-output* "~&Generators written to file ~A~%" out-namestring))
  )


(defun vint-helper (in in-namestring out out-namestring
		    &key
		    (read-ieq T) (read-ine NIL)
		    (write-poi T) (write-ext NIL))
  "Enumerate all points within a polyhedron."
  (format *error-output* "Enumeration of all points for ~A~%" in-namestring)
  ;; sanity check
  (cond
    ((and read-ieq read-ine)
     (signal 'usage-help :message "Only one of IEQ or INE input options may be specified" :command "vint"))
    ((not (or read-ieq read-ine))
     (signal 'usage-help :message "Either IEQ or INE input must be selected" :command "vint"))
    ((and write-poi write-ext)
     (signal 'usage-help :message "Only one of POI or EXT output options may be specified" :command "vint"))
    ((not (or write-poi write-ext))
     (signal 'usage-help :message "Either POI or EXT input must be selected" :command "vint")))
  
  (let* ((p (cond 
	      (read-ieq
	       (make-h-poly-from-ieqfile in))
	      (read-ine
	       (make-h-poly-from-inefile in))))
	 (v (enumerate-points p)))
    (format *error-output* "~&Number of valid integral points: ~D~%"
	    (poly::numpoints v))
    (cond
      (write-poi
       (write-poly-poi-format v :dst out))
      (write-ext
       (write-poly-ext-format v :dst out)))
    (format *error-output* "~&Integral points written to file ~A~%" out-namestring)))
		    
(defun vint (args options)
  "Enumerate all points within a polyhedron."
  (declare (type list args options))
  (let* ((reading-ine (option-is-set "read-ine" options))
	 (writing-ext (option-is-set "write-ext" options))
	 ;; implictly we prefer IEQ/POI:
	 (reading-ieq (or (option-is-set "read-ieq" options)
			  (not reading-ine)))
	 (writing-poi (or (option-is-set "write-poi" options)
			  (not writing-ext))))
    (multiple-value-bind (in in-name out out-name)
	(case (length args)
	  (0 ;; read stdin, write stdout
	   (values *standard-input* "standard input"
		   *standard-output* "standard output"))
	  (1 ;; assume it is the input file
	   (if (string= "-" (first args))
	       ;; stdin, and hence also implicityly output to stdout
	       (values *standard-input* "standard input"
		       *standard-output* "standard output")
	       ;; construct default output 
	       (let ((outpath (make-pathname :defaults (merge-pathnames (first args))
					     :type (if writing-poi "poi" "ext"))))
		 (values (merge-pathnames (first args)) (namestring (merge-pathnames (first args)))
			 outpath (namestring outpath)))))
	  (2 ;; 2 file arguments: in and out
	   (multiple-value-bind (in in-name)
	       (if (string= "-" (first args))
		   (values *standard-input* "standard input")
		   (values (merge-pathnames (first args)) (namestring (merge-pathnames (first args)))))
	     (multiple-value-bind (out out-name)
		 (if (string= "-" (second args))
		     (values *standard-output* "standard output")
		     (values (merge-pathnames (second args)) (namestring (merge-pathnames (second args)))))
	       (values in in-name out out-name))))
	  (otherwise (error "Need 0, 1 or 2 filename arguments")))
      (vint-helper in in-name out out-name
		   :read-ieq reading-ieq :read-ine reading-ine
		   :write-poi writing-poi :write-ext writing-ext))))
    

(defun ieq2ine-helper (in in-name out out-name
		       &key
		       (split-equations NIL) (ensure-integers NIL))
  "Read a porta-style .IEQ file and write it in .INE format."
  (declare (type string in-name out-name))
  (format *error-output* "Transforming IEQ to INE (from ~A to ~A)~%" in-name out-name)
  (when split-equations
    (format *error-output* "~&Warning: split-equations option not yet implemented~%"))
  (when ensure-integers
    (format *error-output* "~&Warning: ensure-integers option not yet implemented~%"))

  (let ((p (make-h-poly-from-ieqfile in)))
    (if p
	(write-poly-ine-format p
			       :dst out
			       :fname-info (concatenate 'string out-name " generated from " in-name)))))

(defun ieq2ine (args options)
  "Read a porta-style .ieq file and write it in .ine format."
  (declare (type list args options))
  (multiple-value-bind (in in-name out out-name)
      (case (length args)
	(0 ;; read stdin, write stdout
	 (values *standard-input* "standard input"
		 *standard-output* "standard output"))
	(1 ;; assume it is the input file
	 (if (string= "-" (first args))
	     ;; stdin, and hence also implicityly output to stdout
	     (values *standard-input* "standard input"
		     *standard-output* "standard output")
	     ;; construct default output 
	     (let ((outpath (make-pathname :defaults (merge-pathnames (first args))
					   :type "ine")))
	       (values (merge-pathnames (first args)) (namestring (merge-pathnames (first args)))
		       outpath (namestring outpath)))))
	(2 ;; 2 file arguments: in and out
	 (multiple-value-bind (in in-name)
	     (if (string= "-" (first args))
		 (values *standard-input* "standard input")
		 (values (merge-pathnames (first args)) (namestring (merge-pathnames (first args)))))
	   (multiple-value-bind (out out-name)
	       (if (string= "-" (second args))
		   (values *standard-output* "standard output")
		   (values (merge-pathnames (second args)) (namestring (merge-pathnames (second args)))))
	     (values in in-name out out-name))))
	(otherwise (error "Need 0, 1 or 2 filename arguments")))
    (ieq2ine-helper in in-name out out-name
		    :split-equations (option-is-set "split-equations" options)
		    :ensure-integers (option-is-set "ensure-integers" options))))

(defun atropsort (args options)
  (declare (ignore args options))
  (error "atropsort unimplemented, sorry")
  )
(defun atropsort-helper (in in-name out out-name
			 &key (in-filetype NIL) (out-filetype NIL))
  "Read IN, sort to standard form, write OUT."
  (declare (ignore out-name))
  (let ((p (ecase in-filetype
	     (('ieq) (make-h-poly-from-ieqfile in))
	     (('ine) (make-h-poly-from-inefile in))
	     (('poi) (make-v-poly-from-poifile in))
	     (('ext) (make-v-poly-from-extfile in))
	     ((nil) (format *error-output*
			    "Automatic file type recognition not implemented~%")
	      (return-from atropsort-helper)))))
    (declare (type poly p))
    ;; reading was done above, beautification ist part of writing
    ;; dafault to same format
    (if (not out-filetype)
	(setq out-filetype in-filetype))
    (let ((info (format nil "This is the result of atropsort on ~A" in-name)))
      (ecase out-filetype
	(('ieq) (write-poly-ieq-format p :dst out :fname-info info))
	(('ine) (write-poly-ine-format p :dst out :fname-info info))
	(('poi) (write-poly-poi-format p :dst out :fname-info info))
	(('ext) (write-poly-ext-format p :dst out :fname-info info))
	((nil) (format *error-output*
		       "Automatic file type recognition not implemented~%")
	 (return-from atropsort-helper))))))
     


(defun ine2ieq-helper (in in-name out out-name
		       &key
		       (split-equations NIL) (ensure-integers NIL))
  "Read a cdd/lrs-style .ine file and write it in .ieq format."
  (declare (type string in-name out-name))
  (format *error-output* "Transforming INE to IEQ (from ~A to ~A)~%" in-name out-name)
  (when split-equations
    (format *error-output* "~&Warning: split-equations option not yet implemented~%"))
  (when ensure-integers
    (format *error-output* "~&Warning: ensure-integers option not yet implemented~%"))

  (let ((p (make-h-poly-from-inefile in)))
    (if p
	(write-poly-ieq-format p
			       :dst out
			       :fname-info (concatenate 'string out-name " generated from " in-name)))))

(defun ine2ieq (args options)
  "Read a cdd/lrs-style .ine file and write it in .ieq format."
  (declare (type list args options))
  (multiple-value-bind (in in-name out out-name)
      (case (length args)
	(0 ;; read stdin, write stdout
	 (values *standard-input* "standard input"
		 *standard-output* "standard output"))
	(1 ;; assume it is the input file
	 (if (string= "-" (first args))
	     ;; stdin, and hence also implicityly output to stdout
	     (values *standard-input* "standard input"
		     *standard-output* "standard output")
	     ;; construct default output 
	     (let ((outpath (make-pathname :defaults (merge-pathnames (first args))
					   :type "ieq")))
	       (values (merge-pathnames (first args)) (namestring (merge-pathnames (first args)))
		       outpath (namestring outpath)))))
	(2 ;; 2 file arguments: in and out
	 (multiple-value-bind (in in-name)
	     (if (string= "-" (first args))
		 (values *standard-input* "standard input")
		 (values (merge-pathnames (first args)) (namestring (merge-pathnames (first args)))))
	   (multiple-value-bind (out out-name)
	       (if (string= "-" (second args))
		   (values *standard-output* "standard output")
		   (values (merge-pathnames (second args)) (namestring (merge-pathnames (second args)))))
	     (values in in-name out out-name))))
	(otherwise (error "Need 0, 1 or 2 filename arguments")))
    (ine2ieq-helper in in-name out out-name
		    :split-equations (option-is-set "split-equations" options)
		    :ensure-integers (option-is-set "ensure-integers" options))))



(defun read-variable-renaming-table (stream)
  "Read a variable renaming table from STREAM.
Return it as an association list of strings."
  (flet ((read-name ()
	   (let ((name (make-array 0 :element-type 'base-char :adjustable t :fill-pointer t)))
	     (loop for char = (read-char stream nil nil)
		while char
		until (member char '(#\Space #\Tab #\Newline))
		do (vector-push-extend char name))
	     name)))
    (loop while (peek-char t stream nil nil)
       collect (cons (read-name)
		     (progn (peek-char t stream nil nil)
			    (read-name))))))

(defun ieq2lp-helper (in in-name out out-name
		      &key (rename-stream nil))
  "Read a PORTA-style .IEQ file and write it in .LP format."
  (format *error-output* "Transforming IEQ to LP (from ~A to ~A)~%" in-name out-name)
  (let* ((p (make-h-poly-from-ieqfile in))
	 (rename-table (and rename-stream (read-variable-renaming-table rename-stream)))
	 (variable-names
	  (and rename-table
	       (loop
		  with names = (make-array (the array-index (adim p)))
		  for index of-type array-index from 1 upto (adim p)
		  for porta-name = (format nil "x~D" index)
		  for real-name = (cdr (assoc porta-name rename-table :test #'string=))
		  do (setf (aref names (1- index))
			   (or real-name porta-name))
		  finally (return names)))))
    (if p
	(write-poly-lp-format p :dst out
			      :variable-names variable-names))))

(defun ieq2lp (args options)
  "Read a porta-style .ieq file and write it in .lp format."
  (declare (type list args options))
  (multiple-value-bind (in in-name out out-name)
      (case (length args)
	(0 ;; read stdin, write stdout
	 (values *standard-input* "standard input"
		 *standard-output* "standard output"))
	(1 ;; assume it is the input file
	 (if (string= "-" (the string (first args)))
	     ;; stdin, and hence also implicityly output to stdout
	     (values *standard-input* "standard input"
		     *standard-output* "standard output")
	     ;; construct default output 
	     (let ((outpath (make-pathname :defaults (merge-pathnames (first args))
					   :type "lp")))
	       (values (merge-pathnames (first args)) (namestring (merge-pathnames (first args)))
		       outpath (namestring outpath)))))
	(2 ;; 2 file arguments: in and out
	 (multiple-value-bind (in in-name)
	     (if (string= "-" (the string (first args)))
		 (values *standard-input* "standard input")
		 (values (merge-pathnames (first args)) (namestring (merge-pathnames (first args)))))
	   (multiple-value-bind (out out-name)
	       (if (string= "-" (the string (second args)))
		   (values *standard-output* "standard output")
		   (values (merge-pathnames (second args)) (namestring (merge-pathnames (second args)))))
	     (values in in-name out out-name))))
	(otherwise (error "Need 0, 1 or 2 filename arguments")))
    (let ((rename-variables (cdr (assoc "rename-variables" options :test #'string=))))
      (if rename-variables
	  (with-open-file (rename-stream rename-variables :direction :input)
	    (ieq2lp-helper in in-name out out-name
			   :rename-stream rename-stream))
	  (ieq2lp-helper in in-name out out-name)))))
