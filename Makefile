# $Id$
#
# This makefile is tested only with GNU make. It may very well not work
# with other flavors.
#
## choose one of the following lisp styles and the associated binary: 
# CMU:
#LISPSTYLE=cmu
#LISP=lisp
#
# Franz's Allegro CL:
#LISPSTYLE=allegro
#LISP=alisp
#
# SBCL:
LISP=/usr/bin/sbcl
LISPSTYLE=sbcl

########################################################################
# No user-serviceable parts below

# FIXME: separate install of shell scripts from core file
INSTALLPATH=$(CURDIR)
#INSTALLPATH=/usr/lib/atrop

###########
WRAPPER_SCRIPTS=\
	fmel\
	traf\
	vint\
	atropsort\
	dim\
	ieq2ine\
	ine2ieq\
	ieq2lp\


LISP_SOURCES=\
	atrop.asd\
	atrop.lisp\
	gauss.lisp\
	parser.lisp\
	ieq-parser.lisp\
	ine-ext-parser.lisp\
	lrestr.lisp\
	poly.lisp\
	htable.lisp\
	traf.lisp\
	vint.lisp\
	allegro-optimize.lisp\
	utils.lisp\




RELEASE_NAME=atrop-$(shell date +%Y-%m-%d)
RELEASE_FILES=README\
	      AUTHORS\
	      COPYING\
	      $(LISP_SOURCES)\
	      Makefile\
	      atrop-wrapper.in\
	      atrop.el\
	      getopt-long\
	      prepare-core.lisp\



###########
all: atrop.core $(WRAPPER_SCRIPTS)

release: $(RELEASE_FILES)

clean:
	$(MAKE) -k -C$(CURDIR)/getopt-long clean 
	rm -f *.fasl *.x86f *.sparcf $(WRAPPER_SCRIPTS) atrop-wrapper atrop.core

install: all
	if test ! -d $(INSTALLPATH) ; then \
	  mkdir $(INSTALLPATH); \
	fi
	cp atrop.core atrop-wrapper $(INSTALLPATH)/
	cp $(WRAPPER_SCRIPTS) $(INSTALLPATH)/

uninstall:
	rm -f $(INSTALLPATH)/atrop.core 
	rm -f $(INSTALLPATH)/atrop-wrapper
	for i in $(WRAPPER_SCRIPTS); do rm -f $(INSTALLPATH)/$$i; done

## CMU specialization:
ifeq (${LISPSTYLE},cmu)
LISPCALL=$(LISP) -core $(INSTALLPATH)/atrop.core
atrop.core: prepare-core.lisp $(LISP_SOURCES)
	$(LISP) -load prepare-core.lisp
else
## Allegro CL specialization:
ifeq (${LISPSTYLE},allegro)
LISPCALL=$(LISP) -I $(INSTALLPATH)/atrop.core
atrop.core: prepare-core.lisp $(LISP_SOURCES) allegro-load.lisp allegro-optimize.lisp Makefile
	$(LISP) -batch < prepare-core.lisp
else
## SBCL specialization:
ifeq (${LISPSTYLE}, sbcl)
LISPCALL=$(LISP) --noinform --core $(INSTALLPATH)/atrop.core
atrop.core: prepare-core.lisp $(LISP_SOURCES)
	$(LISP) --load prepare-core.lisp
else
## roll your own...
$(error $(LISPSTYLE) must be set to a supported dialect)
endif
endif
endif

atrop-wrapper: atrop-wrapper.in Makefile
	sed -e "s!\@\@LISPCALL\@\@!$(LISPCALL)!" < $<  > $@
$(WRAPPER_SCRIPTS): atrop-wrapper
	cp atrop-wrapper $@
	chmod +x $@
