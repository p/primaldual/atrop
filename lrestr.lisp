;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; package: lrestr -*-
;;;; ***********************************************************************
;;;;
;;;; LRESTR class for linear restrictions, with derived
;;;; classes INEQUALITY and EQUATION.
;;;;
;;;; (c) 2004,2005 Utz-Uwe Haus, <atrop@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; ***********************************************************************

(eval-when
    (:compile-toplevel :load-toplevel)
  (declaim (optimize (safety 1) (debug 1) (speed 3))
	   (inline linform rhs sense nzcnt)))


(defpackage #:LRESTR
  (:documentation "Linear Restrictions package ($Revision$, $Date$).")
  (:use #:COMMON-LISP)
  (:export  ;; structures:
	    #:lrestr #:inequality #:equation
	    #:lrestr-p #:inequality-p #:equation-p
	    ;; accessors:
	    #:linform #:nzcnt #:rhs #:sense
	    ;; ctors
	    #:make-inequality #:make-equation
	    ;; methods:
	    #:normalize-sense 
	    #:split #:beautify #:integralize #:normalize 
	    #:write-ieq-format #:write-lp-format #:compare-<
	    #:scale
	    #:check-nzcnt
	    ;; types
	    #:col-or-row-index #:sense-symbol
	    ;; converter
	    #:translate-sense
	    ;; predicates
	    #:equality-sensep #:nonequality-sensep #:strict-sensep
	    #:inequality-clearly-infeasible
	    ;; others
	    #:compare-fun-for-lrestr
	    )
  ;;  (:shadow #:assert)
  )


(in-package #:LRESTR)
;;(defmacro assert (&rest x))

;;;; columns and rows are stored as vectors, currently, so indices are fixnums of limited size:
(deftype col-or-row-index ()
  `(integer 0 #.ARRAY-DIMENSION-LIMIT))

;;;; A class representing a single linear restriction
(defstruct (lrestr 
		   ;(:predicate lrestr-p)
		   ;(:copier shallow-copy-lrestr) ;; dup-lrestr defined later does a deep copy
		   ;(:constructor make-lrestr)
		   )
  "A structure storing linear restrictions."
  ;; The linear form on the lhs
  (linform (make-array 1 :element-type 'rational :adjustable nil) ;; dummy but...
	   :type (simple-array rational (*)))
  ;; The number of nonzeros in LINFORM
  (nzcnt   0   :type col-or-row-index)
  ;; RHS term
  (rhs     0   :type rational)
  )

(defstruct (equation (:include lrestr))
  "A structure representing a linear equation."
  )

;; The SENSE symbols we use will internally always be LRESTR::<= etc, so the following function should be used to
;; translate from arbitrary symbols to ours
(deftype sense-symbol ()
  `(member <= >= < > ==))

(defstruct (inequality (:include lrestr)
		       (:constructor make-ieq))
  "A structure representing a linear inequality."
  (sense '<=  :type sense-symbol))



(defmacro translate-sense (sense)
  (let ((tmp (gensym "translsense")))
    `(let ((,tmp ,sense))
      (declare (type (or base-char base-string symbol) ,tmp))
      (the sense-symbol
	(cond
	  ((string= ,tmp '<=)    '<=)
	  ((string= ,tmp '>=)    '>=)
	  ((string= ,tmp '==)    '==)
	  ((string= ,tmp '<)     '<)
	  ((string= ,tmp '>)     '>)
	  ((string= ,tmp '=)     '==)
	  ((string= ,tmp '=>)    '>=)
	  ((string= ,tmp '=<)  '<=)
	  (t (error (format nil "Symbolic sense ~A is not allowed" ,tmp))))))))

;; specialized predicates
(defun equality-sensep (sense)
  (assert (typep sense 'sense-symbol))
  (eq sense '==))
(defun nonequality-sensep (sense)
  (assert (typep sense 'sense-symbol))
  (not (equality-sensep sense)))
(defun strict-sensep (sense)
  (assert (typep sense 'sense-symbol))
  (member sense '(< >) :test #'eq))

(defun inequality-clearly-infeasible (l)
  "Check whether l has no entries on lhs and relation/rhs value which are obviously infeasible"
  (declare (type inequality l))
  (cond
    ((eq (sense l) '<=)
     (< (rhs l) 0))
    ((eq (sense l) '<)
     (<= (rhs l) 0))
    ((eq (sense l) '>=)
     (> (rhs l) 0))
    ((eq (sense l) '>)
     (>= (rhs l) 0))
    (t (error (format nil "Symbolic sense ~A not allowed in inequality" (the symbol (sense l)))))))

	   
;; class-style accessors
(declaim (ftype (function (lrestr) (simple-array rational)) linform))
(defun linform (l)
  (lrestr-linform l))

(declaim (ftype (function (lrestr) col-or-row-index) nzcnt))
(defun nzcnt (l)
  (lrestr-nzcnt l))

(declaim (ftype (function (lrestr) rational) rhs))
(defun rhs (l)
  (lrestr-rhs l))

(defun (setf rhs) (value place)
  (setf (lrestr-rhs place) value))

(declaim (ftype (function (lrestr) symbol) sense))
(defun sense (restr)
  "Return the sense of RESTR; works for inequalities and equations."
  (etypecase restr
    (equation
     '==)
    (inequality
     (inequality-sense restr))))

(defun (setf sense) (value place)
  (etypecase place
    (equation
     (if (equal value '==)
	 '==
	 (error "Attempt to change sense of an equation")))
    (inequality
     (setf (inequality-sense place) value))))

;;
;; (define-setf-expander sense (x &environment env)
;;   "Change ieq-sense of x if it is an inequality, and check that sense remains == for an equation."
;;   (multiple-value-bind (dummies vals newval setter getter)
;;       (get-setf-expansion x env)
;;     (declare (ignorable newval setter))
;;     (let ((store (gensym)))
;;       (values dummies
;; 	      vals
;; 	      `(,store)
;; 	      `(progn
;; 		(etypecase ,getter
;; 		  (equation
;; 		   (if (equal ,store '==)
;; 		       '==
;; 		       (error "Attempt to change sense of an equation")))
;; 		  (inequality
;; 		   (setf (ieq-sense ,getter) ,store)
;; 		   ,store)))
;; 	      `(sense ,getter)))))


(defun dup-lrestr (l)
  "Do a deep copy of L."
  (declare (ignore l))
  (error "Implement this if necessary.")
  )
(defun dup-equation (e)
  "Do a deep copy of E."
  (declare (ignore e))
  (error "Implement this if necessary.")
  )
(defun dup-inequality (i)
  (declare (ignore i))
  "Do a deep copy of I."
  (error "Implement this if necessary.")
  )
(defun make-inequality (&key (sense '<=) (linform NIL) rhs (lhs-list NIL) (nzcnt 0 nzcnt-specified-p))
  (unless (or linform lhs-list)
    (error ":linform or :lhs-list as well as :rhs must be specified"))
  (let ((lf (or linform
 		(make-array (length (the list lhs-list))
			    :element-type 'rational
			    :initial-contents lhs-list
			    :adjustable nil))))
    (if nzcnt-specified-p
	(assert (= (count-if-not #'zerop lf) nzcnt)))
    (make-ieq :linform lf
	      :nzcnt (if nzcnt-specified-p
			 nzcnt
			 (count-if-not #'zerop lf))
	      :rhs rhs
	      :sense (translate-sense sense))))

(defun normalize-sense (i)
  "Destructively convert I to be oriented as <= or <.
 Returns (possibly modified) I."
  (declare (type inequality i))
  (if (member (inequality-sense i) '(< <=) :test #'eq)
      i
      (progn
 	(if (eq (inequality-sense i) '>)
 	    (setf (inequality-sense i) '<)
 	    (setf (inequality-sense i) '<=))
 	(setf (inequality-rhs i) (- (inequality-rhs i)))
 	(loop for idx from 0 below (length (inequality-linform i))
 	      do (setf (svref (inequality-linform i) idx)
 		       (- (the rational (svref (inequality-linform i) idx)))))
 	i)))
	   
(defun split (e)
  "Return two new inequalities that together represent equation E."
  (let ((e1 (make-inequality :sense '<= :rhs (equation-rhs e) 
			     :linform (copy-seq (equation-linform e))))
	(e2 (make-inequality :sense '>= :rhs (equation-rhs e) 
			     :linform (copy-seq (equation-linform e)))))
    (values e1 e2)))

(defun beautify (l)
  (integralize l))

(defun integralize (l &key (include-rhs T))
  "Simplify coefficients in L to be all-integer, recomputing the nzcnt.
If include-rhs is non-nil include the rhs into the gcd computation."
  (let ((denominators (cons (if include-rhs
				(denominator (lrestr-rhs l))
				1)
 			    (map 'list #'denominator (lrestr-linform l))))
 	(numerators (delete-if (lambda (x) (= (the integer x) 0))
 			       (cons (if include-rhs
					 (numerator (lrestr-rhs l))
					 0)
 				     (map 'list #'numerator (lrestr-linform l))))))
    (let ((deno (apply #'lcm denominators))
 	  (nume (apply #'gcd (or numerators '(1)))))
      (declare (type integer deno nume))
      (setf (lrestr-nzcnt l)
	    (loop for idx of-type col-or-row-index
	       from 0 below (length (lrestr-linform l))
	       initially (setf (lrestr-rhs l) (* (/ deno nume) (lrestr-rhs l)))
	       do (setf (svref (lrestr-linform l) idx)
			(* (/ deno nume)
			   (the rational (svref (lrestr-linform l) idx))))
	       counting (not (zerop (the rational
				      (svref (lrestr-linform l) idx))))))
      ))
  ;;(format t " yields ") (write-ieq-format l) 
  )

(defun normalize (l)
  "Simplify coefficients in L so that the leftmost entry equals +1 or -1,
 recomputing the nzcnt."
  (let ((leftmost-nonzero (position-if-not #'zerop (lrestr-linform l))))
    (if (not leftmost-nonzero)
	(setf (lrestr-rhs l) (/ (lrestr-rhs l) 
				(if (zerop (lrestr-rhs l))
				    1
				    (abs (lrestr-rhs l))))
	      (lrestr-nzcnt l) 0) ;; this is the nzcnt
	(let ((mult (/ 1 (the (rational 0)
			   (abs (the rational 
				  (svref (lrestr-linform l) leftmost-nonzero)))))))
	  (setf (lrestr-nzcnt l)
		(loop for idx of-type col-or-row-index
		   from leftmost-nonzero below (length (lrestr-linform l))
		   initially (setf (lrestr-rhs l) (* (lrestr-rhs l) mult))
		   do (setf (svref (lrestr-linform l) idx)
			    (* (the rational
				 (svref (lrestr-linform l) idx)) mult))
		   counting (not (zerop (the rational
					  (svref (lrestr-linform l) idx))))))))))

(defun write-ieq-format (l &key (rownum NIL) (dst *standard-output*))
  (declare (type stream dst)
	   (type (or integer (member NIL)) rownum))
  (if rownum
      (format dst "~&(~5D) " rownum)
      (format dst "~&        "))
  (loop for number of-type rational across  (lrestr-linform l)
     for idx of-type col-or-row-index from 1
     do (if (= 0 number)
 	    (format dst "            ")
 	    (progn
 	      (format-rational number :force-sign t :dst dst)
 	      (format dst " x~D " idx))))
  (ctypecase l
    (equation
     (format dst "== "))
    (inequality
     (format dst "~2S " (inequality-sense l))))
  (format-rational (lrestr-rhs l) :dst dst)
  (format dst "~%"))

(defun write-lp-format (l &key (row-name nil) (dst *standard-output*)
			(variable-names nil))
  (declare (type stream dst))
  ;; This is not the most beautiful LP format; we could take a better
  ;; version from CPLEX-glue. --mkoeppe
  (if row-name
      (format dst "~&~A:~11T" row-name)
      (format dst "~&~11T"))
  ;; We should also deal with rationals as we do in GYWOPT. --mkoeppe
  (loop for number of-type rational across  (lrestr-linform l)
     for idx of-type col-or-row-index from 1
     for name = (if variable-names
		    (aref variable-names (1- idx))
		    (format nil "x~D" idx))
     do (if (= 0 number)
 	    (format dst "    ~V@T " (length name))
 	    (progn
 	      (format-rational number :force-sign t :dst dst)
 	      (format dst " ~A " name))))
  (ctypecase l
    (equation
     (format dst "=  "))
    (inequality
     (format dst "~2S " (inequality-sense l))))
  (format-rational (lrestr-rhs l) :dst dst)
  (format dst "~%"))

(defun compare-senses-lt (s1 s2)
  "Returns T or NIL"
  (ecase s1
    (<
     (if (member s2 '(<= > >=) :test #'eq)
	 T NIL))
    (>
     (if (member s2 '(<= >=) :test #'eq)
	 T NIL))
    (<=)
     (eq s2 '>=)))

(defun compare-< (i1 i2)
  "Compare I1 and I2. 
   I1 < I2 if
 * senses are (= vs. <) or (< vs <=) or (= vs >) or (> vs >=) or (< vs >) or (<= vs >=); if equal
 * rhs value smaller; if equal
 * lexicographically smaller."
  (cond
    ;; equations before ieqs
    ((and (equation-p i1)
 	  (inequality-p i2))
     T)
    ((and (inequality-p i1)
	  (equation-p i2))
     NIL)		 
    ;; sort ieqs by their senses
    ((and (inequality-p i1) (inequality-p i2))
     (cond 
       ((compare-senses-lt (inequality-sense i1) (inequality-sense i2))
	T)
       ((compare-senses-lt (inequality-sense i2) (inequality-sense i1))
	NIL)
       ;; rhs?
       ((< (lrestr-rhs i1) (lrestr-rhs i2))
	T)
       ((> (lrestr-rhs i1) (lrestr-rhs i2))
	NIL)
       (t
	;; compare vectors lexicographically
	(cond
	  ((svcompare-< (linform i1) (linform i2))
	   T)
	  (t
	   NIL)))))
    (t
     (error "incomplete case distinction"))))

(defun svcompare-< (v1 v2)
  "Compare simple-vectors of same length V1 and V2 lexicographically by <"
  (declare (type (simple-array rational) v1 v2))
  (assert (= (length v1) (length v2)))
  (do ((i 0 (1+ i)))
      ((= i (length v1)) T)
    (cond
      ((< (svref v1 i) (svref v2 i))
       (return-from svcompare-< T))
      ((> (svref v1 i) (svref v2 i))
       (return-from svcompare-< NIL)))))

(defun scale (l factor)
  "Multiply L by FACTOR."
  (declare (type rational factor))
  (setf (lrestr-rhs l) (* factor (lrestr-rhs l)))
  (loop for c of-type col-or-row-index 
     from 0 below (length (lrestr-linform l))
     do (setf (svref (lrestr-linform l) c)
	      (* factor (the rational (svref (lrestr-linform l) c)))))
  (if (inequality-p l)
      (let ((s (inequality-sense l)))
	(setf (inequality-sense l)
	      (ecase s
		(< '>)
		(<= '>=)
		(> '<)
		(>= '<=))))))

(declaim (ftype (function (lrestr) (function (number number) boolean)) compare-fun-for-lrestr))
(defun compare-fun-for-lrestr (l)
  "Return a comparison function for the row L"
  (if (equation-p l)
      #'=
      (ecase (inequality-sense l)
	(<= #'<=)
	(<  #'<)
	(>= #'>=)
	(>  #'>))))

 
;;;; Utility functions:
(defun format-rational (r &key (dst nil) (force-sign NIL))
  (declare (type rational r)
 	   (type boolean force-sign)
 	   (type (or NULL stream) dst))
  (if force-sign
      (if (= 1 (denominator r))
 	  (format dst "~@D" (numerator r))
 	  (format dst "~@D/~D" (numerator r) (denominator r)))
      (if (= 1 (denominator r))
 	  (format dst "~D" (numerator r))
 	  (format dst "~D/~D" (numerator r) (denominator r)))))

(defun stringify-rational (r &key  (force-sign NIL))
  (format-rational  r :dst nil :force-sign force-sign))
