;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10                  -*-
;;;; ***********************************************************************
;;;;
;;;; Setup for dumping optimized core with Allegro CL.
;;;;
;;;; (c) 2004 Utz-Uwe Haus, haus@uuhaus.de
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; ***********************************************************************
(eval-when
    (:compile-toplevel :load-toplevel :execute)
  (declaim (optimize (safety 1) (debug 0) (speed 3)))
  )


;; prime the clos compiler
(require :yacc)
(in-package :excl) 
(preload-forms) 
(excl::preload-constructors) 
(excl::precache-generic-functions)
(excl::preload-constructors)
