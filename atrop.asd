;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; **********************************************************
;;;;
;;;; Fourier-Motzkin projection and transformation of 
;;;; Polyhedra, including IEQ/INE/LATTE file format converters.
;;;;
;;;; (c) 2005 Utz-Uwe Haus, <atrop@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; **********************************************************

(asdf:defsystem atrop
  :description "A Common-Lisp PORTA-replacement"
  :version "0.3"
  :author "Utz-Uwe Haus <atrop@uuhaus.de>"
  :licence "GPL"
  :depends-on ("de.uuhaus")
  :components ((:file "atrop" :depends-on ("poly"  "utils" "vint" "traf"))
  	       (:file "utils")
	       (:file "parser")
  	       (:file "ieq-parser" :depends-on ("utils" "parser"))
	       (:file "ine-ext-parser" :depends-on ("utils" "parser"))
	       (:file "lrestr")
	       (:file "poly" :depends-on ("ieq-parser" "ine-ext-parser"
					  "lrestr" "gauss" "utils"))
	       (:file "gauss" :depends-on ("lrestr" "utils"))
	       (:file "traf" :depends-on ("poly" "lrestr" "utils"))
	       (:file "vint" :depends-on ("poly" "traf" "utils"))
	       ))
