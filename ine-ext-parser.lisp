;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; package: ine-ext-parser -*-
;;;; *******************************************************************************
;;;;
;;;; INE and EXT format parser.
;;;;
;;;; (c) 2005 Utz-Uwe Haus, <atrop@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; ******************************************************************************

(eval-when
    (:compile-toplevel :load-toplevel)
  (declaim (optimize (safety 1) (debug 3) (speed 1))
	   ))

(defpackage #:INE-EXT-PARSER
  (:documentation "Parser for INE and EXT format ($Revision$, $Date$).")
  (:use #:COMMON-LISP #:ATROP-UTILS #:PARSER)
  (:export 
   ;; general parser function
   #:parse-ine-ext-format
   ;; specialized parser function
   #:parse-ine-format #:parse-ext-format
   ;; conditions
   #:ine-ext-parser-error
   #:ine-ext-parser-warning
   )
  )
;;;; TODO: parsing of VALID, LOWER_BOUNDS, UPPER_BOUNDS sections, and rows without line number

(in-package #:INE-EXT-PARSER)

;;;;; The ine format is badly documented, 
;;;;; the docs in the cdd distribution are not really up-to-date

(defclass ine-ext-parser-source (parser-source)
  ((hdim :initform 0 :type (integer 0) :accessor hdim)
   (ieqlist :initform NIL
	    :type list
	    :accessor ieqlist)
   ))

(define-condition ine-ext-parser-error (parser-error)
  ()
  (:report (lambda (condition s)
	     (declare (type stream s))
	     (format s "INE/EXT-Parser error at (~D,~D): ~A~%"
		     (parser::line (parser::parser-error-parser-obj condition))
		     (parser::col (parser::parser-error-parser-obj condition))
		     (parser::parser-error-message condition)))))

(define-condition ine-ext-parser-warning (parser-warning)
  ()
  (:report (lambda (condition s)
	     (declare (type stream s))
	     (format s "INE/EXT-Parser warning at (~D,~D): ~A~%"
		     (parser::line (parser::parser-warning-parser-obj condition))
		     (parser::col (parser::parser-warning-parser-obj condition))
		     (parser::parser-warning-message condition)))))


(defmethod ine-ext-parser-error ((src ine-ext-parser-source) (message string))
  (signal 'ine-ext-parser-error :message message :parser-obj src))
(defmethod ine-ext-parser-warning ((src ine-ext-parser-source) (message string))
  (warn 'ine-ext-parser-warning :message message :parser-obj src))

(defparameter *section-markers*
  (list
   '("H-representation"  . h-representation)
   '("V-representation"  . v-representation)
   '("begin" . begin)
   '("end" . end)
   '("linearity" . linearity)
   '("equality" . equality) ;; used interchangeably with `linearity'
   '("minimize" . minimize)
   '("maximize" . maximize)
   '("strict_inequality" . strict-ieq)
   ;; empty section is used to signal EOF
   '("" . EOF)
   ))

;;; externally visible function:
(defun parse-ine-ext-format (&optional (file t))
  "Parse INE or EXT format from FILE,
 which may be a pathname or a stream, otherwise use stdin by default.
Returns two values: either of the symbols 'INE or 'EXT and a list of data representing the
file contents. 
For INE: adim ieqlist lb ub eorder valid-point obj
For EXT: adim points rays"
  (restart-case
   (handler-bind ((ine-ext-parser-error
		   #'(lambda (condition)
		       (format t "Parsing ~A failed.~%~A~%" file condition)
		       (invoke-restart 'give-up NIL)))
		  (ine-ext-parser-warning
		   #'(lambda (condition)
		       (format t "~A" condition)
		       (invoke-restart 'muffle-warning))))
     (with-open-file-or-stream (istream file :direction :input)
       (let ((*break-on-signals* nil
					;t
	       ))
	 (let ((i (make-instance 'ine-ext-parser-source :stream istream)))
	   (parse-ine-ext i)))))
    (give-up (&optional v)  (declare (ignore v))
	     (error "Parsing INE file failed."))))

(defun parse-ine-format (&optional (file t))
    "Parse INE format from FILE,
which may be a pathname or a stream, otherwise use stdin by default.
Returns a list of values: adim ieqlist lb ub eorder valid-point obj
"
    (multiple-value-bind (type data)
	(parse-ine-ext-format file)
      (when (not (eq type 'INE))
	(error "File not recognized as INE format."))
      data))

(defun parse-ext-format (&optional (file t))
    "Parse EXT format from FILE,
which may be a pathname or a stream, otherwise use stdin by default.
Returns a list of values: adim points rays
"
    (multiple-value-bind (type data)
	(parse-ine-ext-format file)
      (when (not (eq type 'EXT))
	(error "File not recognized as EXT format."))
      data))



;; internally parsing is handled by this method:
(defmethod parse-ine-ext ((src ine-ext-parser-source))
  "Parse INE or EXT file from SRC. Returns ..."
  (let ((ftype NIL)
	(linearity-data '())
	(matrix-data NIL)
	(matrix-numrows -1)
	(matrix-numcols -1)
	(obj-data NIL)
	(strictness-data NIL))
    (declare (type (or (integer -1 -1) array-index)
		   matrix-numcols matrix-numrows))
    ;;
    (let ((first-section (detect-next-section src)))
      (case first-section
	(H-representation
	 (setq ftype 'INE))
	(V-representation
	 (setq ftype 'EXT))
	(begin
	 (ine-ext-parser-warning src "~&old-style INE file: missing `H-representation' keyword~%")
	 (setq ftype 'INE))
	(t (ine-ext-parser-error src (format nil "~&Illegal file format (missing all section headers), read `~A'" 
					     first-section)))))
    (do ((section (detect-next-section src) (detect-next-section src)))
	((eq section 'EOF))
      (case section
	((linearity equality)
	 (unless (>= matrix-numcols 0)
	   (ine-ext-parser-warning src "Linearity section before matrix block -- assuming 4ti2 style .ine file~%"))
	 (setq linearity-data (parse-linearity-data src)))
	(begin
	 (multiple-value-setq 
	     (matrix-numrows matrix-numcols matrix-data)
	   (parse-matrix-data src)))
	(maximize
	 (unless (>= matrix-numcols 0)
	   (ine-ext-parser-error src "Objective section encountered before matrix block~%"))
	 (setq obj-data (parse-obj-data src matrix-numcols 'MAX)))
	(minimize
	 (unless (>= matrix-numcols 0)
	   (ine-ext-parser-error src "Objective section encountered before matrix block~%"))
	 (setq obj-data (parse-obj-data src matrix-numcols 'MIN)))
	(strict-ieq
	 ;; FIXME: which format??
	 (setq strictness-data (parse-strictness-data src)))
	(t (ine-ext-parser-error src (format nil "Illegal or unexpected section keyword: ~A" section)))))
    ;(break "M ~A, N ~A, Matrix ~A, linearity ~A" matrix-numrows matrix-numcols matrix-data linearity-data)
    (ecase ftype
      (INE
       (values 'INE
	       ;; adim ieqlist lb ub eorder valid-point obj
	       (list
		(- matrix-numcols 1)
		(create-ieq-list matrix-numcols matrix-data strictness-data linearity-data)
		nil nil nil nil 
		(if obj-data
		    (cons (car obj-data) 
			  ;; strip rhs component from obj
			  (make-array (- matrix-numcols 1) :element-type 'rational
				      :adjustable NIL :fill-pointer NIL
				      :initial-contents (make-array (- matrix-numcols 1) :element-type 'rational
								    :displaced-to (cdr obj-data)
								    :displaced-index-offset 1)))
		    nil))))
      (EXT
       (values 'EXT
	       ;; adim points rays
	       (cons
		(- matrix-numcols 1)
		(multiple-value-list (create-points-and-rays matrix-numcols matrix-data linearity-data)))))
       )))



;; helper functions

(defmethod detect-next-section ((src ine-ext-parser-source))
  "Skip over lines until we see a new section keyword."
  (skip-empty-lines src)
  (skip-space src)
  (do ((next-word (read-word src) (read-word src)))
      (nil)
    '(format t "Got `~A'~%" next-word)
    (let ((next-section (assoc next-word *section-markers*
			       :test #'string-equal)))
      '(format t "next word: `~A', next section `~A'~%" next-word next-section)
      (when next-section
	(return-from detect-next-section (cdr next-section)))
      (skip-to-next-line src)
      (skip-space src))))

(defmethod parse-linearity-data ((src ine-ext-parser-source))
  "Parse an unsigned integer K and then K unsigned integers.
 Returns the latter as a list."
  (let ((result
	 (let ((K (progn (skip-space src) (parse-uint src))))
	   (loop for i of-type array-index from 0 below K
	      collecting (progn (skip-space src)
				(parse-uint src))))))
    ;(format t "linearity: ~A~%" result)
    result))

(defmethod parse-matrix-data ((src ine-ext-parser-source))
  "Parse 2 unsigned integers M and N and either the word `rational' or `integer'
and then M * N numbers. 
For IEQ files N consecutive numbers form a row \transp{a} x <= b
where the first number is b and the next N-1 numbers are -a_i.
For EXT files N consecutive numbers are the N coordinates of a point.

Ignores line breaks, as CDD sometimes spreads a row over more than one file line
Expects an END keyword after the numbers.
Returns 3 values: M, N and a list of M lists of N numbers."
  (skip-empty-lines src)
  (let* ((M (progn (skip-space src) (parse-uint src)))
	 (N (progn (skip-space src) (parse-uint src)))
	 (numbertype (progn (skip-space src) (read-word src))))
    ;(format t "Found M=~D, N=~D type ~A~%" M N numbertype)
    (when (string-equal numbertype "real")
      (ine-ext-parser-error src "Number type `real' unsupported by atrop"))
    (values M
	    N 
	    (loop for row of-type array-index from 0 below M
	       collecting (loop for col of-type array-index from 0 below N
			     collecting (progn
					  (skip-chars-in-list src (list #\Newline #\Space #\Tab))
					  (parse-rational src NIL)))
	       finally (progn (skip-empty-lines src)
			      (skip-space src)
			      (unless (string-equal "END" (read-word src))
				(ine-ext-parser-error src "Garbage withing matrix section")))))))

(defmethod parse-obj-data ((src ine-ext-parser-source) (N integer) (objsense symbol))
  "Parse N rationals and return them as objective coefficients"
  (skip-empty-lines src)
  (cons objsense (parse-N-rationals src N)))

(defmethod parse-strictness-data ((src ine-ext-parser-source))
  ""
  )

(defun create-ieq-list (matrix-numcols matrix-data strictness-data linearity-data)
  "Create ieqs from MATRIX-DATA consulting STRICTNESS-DATA and LINEARITY-DATA"
  (declare (type array-index matrix-numcols))
  (let ((adim (- matrix-numcols 1))
	(strict-rows
	 (sort (remove-duplicates strictness-data) #'<))
	(eq-rows
	 (sort (remove-duplicates linearity-data) #'<)))	
    (loop for row in matrix-data
       for idx of-type array-index from 1
       collecting
       ;; b -A representing A <= b becomes -A >= -b
	 (let ((rhs (- (the rational (first row))))
	       (l (make-array adim :element-type 'rational :adjustable NIL :fill-pointer NIL 
			      :initial-contents (cdr row)))
	       (sense (cond
			((and (consp strict-rows)
			      (= idx (the array-index (first strict-rows))))
			 (pop strict-rows)
			 (assert (or (null eq-rows)
				     (not (= idx (the array-index (first eq-rows))))))
			 '>)
			((and (consp eq-rows)
			      (= idx (the array-index (first eq-rows))))
			 (pop eq-rows)
			 (assert (or (null strict-rows)
				     (not (= idx (the array-index (first strict-rows))))))
			 '==)
			(t '>=))))
	   (list l sense rhs)))))

(defun create-points-and-rays (matrix-numcols matrix-data linearity-data)
  "Read rows from MATRIX-DATA as points if their first component is 1, and as rays if 0.
For those rows that appear in LINEARITY-DATA both the point/ray and its negative are 
returned. FIXME: This is not really compatible to CDD.
Returns two lists: POINTS and RAYS"
  (declare (type array-index matrix-numcols))
  (let ((points '())
	(rays '())
	(free-generators (sort (remove-duplicates linearity-data) #'<))
	(adim (- matrix-numcols 1)))
    (loop for row in matrix-data
       for idx of-type array-index from 1
       do (let* ((l (make-array adim :element-type 'rational :adjustable NIL :fill-pointer NIL
				:initial-contents (cdr row)))
		 (neg-l (if (and free-generators
				 (= idx (the array-index (first free-generators))))
			    (progn 
			      (pop free-generators)
			      (let ((neg-l (make-array adim :element-type 'rational :adjustable NIL :fill-pointer NIL)))
				(loop for i of-type array-index from 0 below adim
				   do (setf (svref neg-l i) (- (the rational (svref l i)))))
				neg-l))
			    NIL)))
	    (ecase (the rational (first row))
	      (0
	       (push l rays)
	       (when neg-l (push neg-l rays)))
	      (1
	       (push l points)
	       (when neg-l (push neg-l points))))))
    (values points rays)))