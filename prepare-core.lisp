;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10               -*-
;;;; ********************************************************************
;;;;
;;;; script for dumping custom core
;;;;
;;;; (c) 2005 Utz-Uwe Haus, <atrop@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; ********************************************************************

;; compile and load our dispatch script
(asdf:operate 'asdf:load-op '#:atrop)

;; dump custom core
#+cmu
(progn
  (setq *gc-verbose* NIL
	*compile-verbose* NIL
	*load-verbose* NIL)
  (setq *bytes-consed-between-gcs* (* 48 1024 1024)) ;; tune if you like
  (extensions:save-lisp "atrop.core"
		      :purify T
		      :init-function 'atrop:dispatch-atrop
		      :print-herald NIL))
#+allegro
(progn 
  ;;(use-package '#:atrop)
  (setq *restart-app-function* #'atrop::dispatch-atrop
	*global-gc-behavior* :auto
	;;*compile-verbose* NIL
	;;*load-verbose* NIL
	)
  ;;(excl:dumplisp :name "atrop.core" 
					;:suppress-allegro-cl-banner T)
  ;; run a sample of every critical code path
  ;; (assumes that all packages have already been loaded into :cl-user)
  (atrop::fmel '("instances/ieq/A-Kap3.ieq" "/dev/null") '())
  (atrop::fmel '("instances/ieq/groetschel7.ieq" "/dev/null") '())
  (atrop::fmel '("instances/ieq/mit41-16.ieq" "/dev/null") '())
  (atrop::vint '("instances/ieq/aggregate4.ieq") '())
  ;; save collected info:
  (compile-file "allegro-optimize" :load-after-compile t)
  (build-lisp-image "atrop.core"
		    :lisp-files '("~/.clinit.cl"
				  "allegro-load.lisp"
				  :constructor "allegro-optimize.fasla16")
		    :preserve-documentation-strings t
		    :print-startup-message nil
		    :restart-app-function 'atrop:dispatch-atrop
		    :init-file-names '()
		    :close-oldspace t
		    :discard-compiler nil
		    :allow-existing-directory t
		    :debug t :verbose t
		    :build-input "input.txt" :build-output "output.txt"
		    :suppress-allegro-cl-banner t
		    :image-only t)
  (exit))
#+sbcl
(progn ;; fixup from sbcl-devel for array typing problems in 0.9.9
  (in-package :sb-kernel)
  (defun sb-kernel::vector-t-p (x) 
    (or (simple-vector-p x) 
	(and (sb-kernel::complex-vector-p x) 
	     (do ((data (sb-kernel::%array-data-vector x) (sb-kernel::%array-data-vector data))) 
	       ((not (sb-kernel::array-header-p data)) (simple-vector-p data))))))
  (save-lisp-and-die "atrop.core" :purify T :toplevel #'atrop:dispatch-atrop))
#-(or cmu allegro sbcl)
(error "Dumping a core for your lisp dialect is not yet supported, please contribute code!")
