;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; package: poly -*-
;;;; *********************************************************************
;;;;
;;;; POLY class, with derived classes H-POLY and V-POLY
;;;; and assorted tools to do Fourier-Motzkin projection.
;;;;
;;;; (c) 2004-2006 Utz-Uwe Haus, <atrop@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; *********************************************************************

(eval-when
    (:compile-toplevel :load-toplevel)
  (declaim 
   (optimize (safety 1) (debug 2) (speed 3)
   ;;(optimize (safety 3) (debug 2) (speed 0)
	      
	     )
   (inline #:numieqs #:numeqs #:ieq-rows #:eq-rows
	   #:combine-ieqs
	   #:delete-ieq-rows
	   #:sense #:linform)))


(defpackage #:POLY
  (:documentation "Polyhedra and projection package ($Revision$, $Date$).")
  (:use #:COMMON-LISP
	#:ieq-parser #:ine-ext-parser
	#:lrestr #:gauss #:de.uuhaus.htable #:de.uuhaus.getopt-long #:atrop-utils)
  (:export 
   ;; Types
   #:poly #:h-poly #:v-poly #:point 
   ;; Ctors
   #:make-h-poly #:make-v-poly
   #:make-h-poly-from-ieqfile
   #:make-h-poly-from-inefile
   #:make-v-poly-from-extfile
   #:make-v-poly-from-poifile   
   ;; type-predicates
   #:poly-p #:h-poly-p #:v-poly-p
   ;; Accessors
   #:elimination-order #:adim #:ieq-rows #:eq-rows #:numeqs #:numieqs #:valid-point
   #:numpoints #:numrays #:poly-points #:poly-rays
   #:lower-bounds #:upper-bounds
   ;; functions
   #:fm-elim 
   #:write-poly-ieq-format #:write-poly-ine-format #:write-poly-latte-format
   #:write-poly-poi-format #:write-poly-ext-format #:write-poly-lp-format
   #:point-validp
   #:delete-empty-equations
   #:split-equations
   ;; Conditions
   #:inconsistent-system
   ;; Re-export from lrestr
   #:linform #:rhs #:sense
   ;; Re-export from ieq-parser
   #:ieq-parser-error
   #:ieq-parser-warning
   )
;  (:shadow #:assert)
  )

(in-package #:POLY)
;(defmacro assert (&rest x) (declare (ignore x)) )

(defparameter *row-vector-extension-increment* 1000)

(defstruct (fmieq (:include inequality))
  "Inequality structure with extra information used during FM-elimination"
  (creation-round 0 :type array-index)
  (cinfo nil :type constituents))
	   


(defstruct (constituents)
  "Data to reconstruct the construction history of an inequality"
  (count 1 :type array-index)
  (poly-reference nil :type (or null h-poly)))

(defstruct (constituent-list (:include constituents))
  "Constituent data represented with a list or original row indices."
  (data '() :type list))
	   
(defstruct (constituent-bits (:include constituents))
  "Constituent data represented by bit-vectors, for large cases."
  (data nil :type simple-bit-vector))

(defmacro convert-to-constituent-bits (list-cinfo)
  `(make-constituent-bits 
    :count (constituents-count ,list-cinfo)
    :poly-reference (constituents-poly-reference ,list-cinfo)
    :data (constituent-list->bitfield (constituent-list-data ,list-cinfo)
				      (num-orig-rows (constituents-poly-reference ,list-cinfo)))))
;(declaim (inline ensure-constituent-bits))
(defun ensure-constituent-bits (rowarray idx)
  (declare (type (vector (or fmieq NULL)) rowarray))
  ;(cerror "continue anyway" "tried to convert row ~D's cdata to constituent-bits" idx)
  (let ((cinfo (fmieq-cinfo (aref rowarray idx))))
    (etypecase cinfo
      (constituent-bits
       cinfo)
      (constituent-list
       (setf (fmieq-cinfo (aref rowarray idx))
	     (convert-to-constituent-bits cinfo))))))

;(declaim (inline subsetp-sorted))
;; (defun subsetp-sorted (list1 list2)
;;   "Check whether l1 is a subset of l2. Both must be sorted by #'<."
;;   (declare (list list1 list2))
;; ;;   (assert (equal (sort list1 #'<) list1))
;; ;;   (assert (equal (sort list2 #'<) list2))
;;   (do ((l1 list1)
;;        (l2 list2))
;;       (nil)
;;     (if (null l1) 
;; 	(return-from subsetp-sorted T))
;;     (let ((x1 (the array-index (car l1))))
;;       (tagbody
;;        l2-chopping
;; 	 (if (null l2)
;; 	     (return-from subsetp-sorted NIL))
;; 	 ;; both nonempty
;; 	 (let ((x2 (the array-index (car l2))))
;; 	   (cond
;; 	     ((> x2 x1)
;; 	      (return-from subsetp-sorted NIL))
;; 	     ((< x2 x1)
;; 	      (setq l2 (cdr l2))
;; 	      (go l2-chopping))
;; 	     ((= x2 x1)
;; 	      (setq l1 (cdr l1)
;; 		    l2 (cdr l2)))
;; 	     ))))))
(defun subsetp-sorted (list1 list2)
  "Check whether l1 is a subset of l2."
  (declare (list list1 list2))
;;   (assert (equal (sort list1 #'<) list1))
;;   (assert (equal (sort list2 #'<) list2))
  (do ((l1 list1)
       (l2 list2))
      (nil)
    (if (null l1) 
	(return-from subsetp-sorted T)
	(if (null l2)
	    (return-from subsetp-sorted NIL)))
    ;; both nonempty
    (let ((x1 (the array-index (car l1)))
	  (x2 (the array-index (car l2))))
      (cond
	((> x2 x1)
	 (return-from subsetp-sorted NIL))
	((= x2 x1)
	 (setq l1 (cdr l1)
	       l2 (cdr l2)))
	((< x2 x1)
	 (setq l2 (cdr l2)))
	))))
;; untested but maybe faster variant:
;; (defun subsetp-sorted (list1 list2)
;;   "Check whether l1 is a subset of l2"
;;   (declare (list list1 list2))
;; ;;   (assert (equal (sort list1 #'<) list1))
;; ;;   (assert (equal (sort list2 #'<) list2))
;;   (do ((l1 list1)
;;        (l2 list2))
;;       (nil)
;;     (if (null l1) 
;; 	(return-from subsetp-sorted T)
;; 	(if (null l2)
;; 	    (return-from subsetp-sorted NIL)))
;;     ;; both nonempty
;;     (let ((x1 (the array-index (car l1))))
;;       (do* ((l l2 (cdr l)))
;; 	   (nil)
;; 	(if (null l)
;; 	    (return-from subsetp-sorted NIL)) ;; l2 empty but we where still looking for X1
;; 	(let ((x2 (the array-index (car l))))
;; 	  (cond
;; 	    ((= x2 x1) ;; have a match, go on
;; 	     (setq l1 (cdr l1)
;; 		   l2 (cdr l))
;; 	     (return-from nil))
;; 	    ((> x2 x1)
;; 	     (return-from subsetp-sorted NIL))))))))

;; (defun  subsetp-sorted (list1 list2)
;;   "Check whether l1 is a subset of l2"
;;    (declare (list list1 list2))
;;   (assert (equal (sort list1 #'<) list1))
;;   (assert (equal (sort list2 #'<) list2))
;;   (if (null list1) 
;;       (return-from subsetp-sorted T))

;;   (do* ((l1 list1 (cdr l1))
;; 	(x1 (car l1) (car l1))
;; 	(l2 list2)) ;; updated in the body
;;        (nil)
;;     (if (null l2)
;; 	;; l2 became and we are still looking for an x1, so
;; 	(return-from subsetp-sorted NIL))
;;     ;; find x1 in l2
;;     (do* ((l l2 (cdr l2))
;; 	  (x2 (car l) (car l)))
;; 	 ((null l) (return-from subsetp-sorted NIL)) ;; because we are still looking for x1
;;       (cond 
;; 	((= x1 x2)
;; 	 ;; found x1 in l, so update l2 and go on to next x1
;; 	 (setq l2 (cdr l))
;; 	 (return-from nil))
;; 	((< x1 x2)
;; 	 ;; possible position of x1 in l passed, so no match
;; 	 (return-from subsetp-sorted NIL))
;; 	 ))
;;     (if (null (cdr l1))
;;       (return-from subsetp-sorted T))))

;(declaim (inline check-bit-domination))
(defun check-bit-domination (d1 d2)
  (let ((scratch-bf (the simple-bit-vector 
		      (cinfo-scratch-bf (fm-info (constituents-poly-reference d1)))))
	(zero-bitfield (the simple-bit-vector
			 (cinfo-zero-bf (fm-info (constituents-poly-reference d1))))))
    (bit-andc2 (constituent-bits-data d1) (constituent-bits-data d2) scratch-bf)
    (equal scratch-bf zero-bitfield)))

;(declaim (inline check-list-domination))
(defun check-list-domination (d1 d2)
  (declare (inline subsetp-sorted))
  (subsetp-sorted (constituent-list-data d1)
		  (constituent-list-data d2)))

;(declaim (inline check-constituent-domination))
(defun check-constituent-domination (p idx1 other-constituent)
  "Check whether row at IDX1 in P dominates OTHER-CONSTITUENT.
This means that the constituents of OTHER-CONSTITUENTS are a superset of the constituents for row IDX1."
  (declare (type (or array-index constituents) idx1)
	   (type constituents other-constituent)
	   (type h-poly p))
  (let ((d1 (if (constituents-p idx1)
		idx1
		(fmieq-cinfo (aref (ieq-rows p) idx1)))))
    (if (< (constituents-count other-constituent) (constituents-count d1))
	NIL
	(cond
	  ((and (constituent-list-p d1) (constituent-list-p other-constituent))
	   (check-list-domination d1 other-constituent))
	  ((and (constituent-bits-p d1) (constituent-bits-p other-constituent))
	   (check-bit-domination d1 other-constituent))
	  ((and (constituent-list-p d1) (constituent-bits-p other-constituent))
	   ;; maybe not a reason to convert d1 since other-constituent is not permanent yet ?
	   (let ((d1-bf (convert-to-constituent-bits d1)))
	     (format t "Inefficiency 1? (const)~%")
	     (check-bit-domination d1-bf other-constituent)))
	  ((and (constituent-bits-p d1) (constituent-list-p other-constituent))
	   (let ((d2-bf (convert-to-constituent-bits other-constituent)))
	     (format t "Inefficiency 2? (const)~%")
	     (check-bit-domination d1 d2-bf)))))))
	 

  

(defun constituent-list->bitfield (clist bitfield-length)
  "Convert a list like '(0 42 4711) into a bitfield of length BITFIELD-LENGTH
 where bits 0, 42, and 4711 are set."
  (declare (type list clist)
	   (type (integer 1 *) bitfield-length))
  (do ((result (make-array bitfield-length
			   :element-type 'bit
			   :initial-element 0))
       (tail clist (cdr tail)))
      ((null tail) result)
    (setf (aref result (the array-index (car tail))) 1)))
  

;;;; Exceptions
(define-condition inconsistent-system (warning)
  ((inconsistent-row :initarg :inconsistent-row
		     :reader inconsistent-row))
  (:report (lambda (condition s)
	     (declare (type stream s))
	     (format s "inconsistent: ~D" 
		     (inconsistent-row condition)))))
(define-condition inconsistent-equation (inconsistent-system)
  ()
  (:report (lambda (condition s)
	     (declare (type stream s))
	     (format s "inconsistent equation: ~D" 
		     (inconsistent-row condition)))))
(define-condition inconsistent-ieq (inconsistent-system)
  ()
  (:report (lambda (condition s)
	     (declare (type stream s))
	     (format s "inconsistent inequality: ~D" 
		     (inconsistent-row condition)))))


;; FM-stuff
(defstruct (fm-information (:conc-name nil))
  "A structure storing information during FM elimination."
  ;; The number of trivial eliminations done
  (num-zero-elims 0 :type array-index)
  ;; The number of eliminations done using equations
  (num-eq-elims 0 :type array-index)
  ;; The number of 'real' eliminations done
  (num-real-elims 0 :type array-index)
  ;; The number of rows saved by applying Chernikov's rule
  (num-saved-by-chernikovrule 0 :type integer)
  ;; The number of rows saved by applying Duffin's rule
  (num-saved-by-duffinrule 0 :type integer)
  ;; The number of deletions due to single-row domination
  (num-single-dom-del 0 :type integer)
  ;; The number of rows generated in total
  (num-ieqs-generated 0 :type integer)
  (zero-idx-vec  
   (make-array 100 :element-type 'array-index :fill-pointer 0 :adjustable t)
   :type (vector array-index *))
  ;;; The following are used only if constituent data is kept in bitfields 
  ;;; instead of lists
  ;; The length of all constituent-info bitfields that we use 
  (cinfo-bf-len 0 :type array-index)
  ;; A bitfield of the above size that is all-zero
  (cinfo-zero-bf ;:type (simple-array bit (*))
   )
  ;; A bitfield of the above size that serves as scratch space
  (cinfo-scratch-bf ; type (simple-array bit (*)
   )
  )

(defun make-fm-info (bf-len)
  (make-fm-information :cinfo-bf-len bf-len
		       :cinfo-zero-bf (make-array bf-len
						  :element-type 'bit
						  :initial-element 0)
		       :cinfo-scratch-bf (make-array bf-len
						  :element-type 'bit)
		       ))
(deftype point ()
  "The type describing a point in space"
  `(and simple-vector
	(vector rational)))

;;;; Polyhedra
(defstruct (poly (:conc-name nil))
  "A Polyhedron"
  (adim nil  :type (or array-index NULL))
  )

(defstruct (v-poly (:include poly)
		   (:conc-name nil)
		   (:constructor make-v-poly-ctor))
  (numpoints     -1 :type array-index)
  (numrays       -1 :type array-index)
  (points        NIL :type (vector (or point NULL)))
  (rays          NIL :type (vector (or point NULL)))		 
  )

;; externally renamed accessors
(declaim (ftype (function (v-poly) (vector (or point NULL))) poly-points))
(defun poly-points (p)
  (points p))
(declaim (ftype (function (v-poly) (vector (or point NULL))) poly-rays))
(defun poly-rays (p)
  (rays p))

(defstruct (h-poly (:include poly)
		   (:conc-name nil)
		   (:constructor make-h-poly-ctor))
  ;; Number of inequalities in system
  (numieqs        -1 :type array-index)
  ;; Number of ieqs+ 2*eqs in the original system
  (num-orig-rows  -1 :type array-index)
  ;; Numer of equations in system
  (numeqs         -1 :type array-index)
  ;; The inequalities of the system
  (ieq-rows       -1 :type (vector (or fmieq NULL)))
  ;; The equations of the system
  (eq-rows        -1 :type (vector equation))
  ;; Elimination order to be used
  (e-order        NIL :type (or NULL (simple-array array-index)))
  ;; A valid point for the system
  (valid-point    NIL :type (or NULL (simple-array rational)))
  ;; Lower and upper bounds
  (lower-bounds   NIL :type (or NULL (simple-array rational)))
  (upper-bounds   NIL :type (or NULL (simple-array rational)))
  ;; Bookkeeping information for FM algorithm
  (fm-info        NIL :type fm-information)
  )

;; FIXME: specialized handling of lb/ub someday
(defun make-h-poly (coldim rowlist &key (e-order nil) (valid-point nil) (lower-bounds nil) (upper-bounds nil))
  "Return a fresh H-POLY containing all the rows in ROWLIST. Rows are 
triples like ( ((idx . value) ...) SENSE rhsval). 1-based indexing!"
  (declare (type array-index coldim)
	   (type list rowlist)
	   (type (or NULL (simple-array array-index)) e-order)
	   (type (or NULL (simple-array rational)) valid-point))
  (multiple-value-bind (eqs ieqs)
      (ieqlist->fm-ieq-arrays rowlist coldim)
    (if (not (or (null e-order)
		 (= coldim (length e-order))))
	(error "e-order is junk"))
    (if (not (or (null valid-point)
		 (= coldim (length valid-point))))
	(error "valid-point is junk"))
    ;; FIXME: more checks    
    (let ((numieqs (length ieqs))
	  (numeqs (length eqs)))
      (let ((instance
	     (make-h-poly-ctor :adim coldim
			       :numieqs numieqs
			       :numeqs numeqs
			       :num-orig-rows (+ numieqs (* 2 numeqs))
			       :ieq-rows ieqs :eq-rows eqs
			       :e-order e-order
			       :valid-point valid-point
			       :lower-bounds lower-bounds
			       :upper-bounds upper-bounds
			       :fm-info (make-fm-info (+ numieqs (* 2 numeqs))))))
	;; normalize rows to <= and add cinfo 
	(loop for i across (ieq-rows instance)
	   do (progn
		(normalize-sense i)
		(assert (member (sense i) '(<= <) :test #'string=)))
	   do (setf (constituents-poly-reference (fmieq-cinfo i)) instance))
	instance
	))))

(defmethod print-object ((x h-poly) stream)
  "Print method for H-POLY structures."
  (print-unreadable-object (x stream :type T :identity T)
    (format stream "adim ~D, ~D ieqs, ~D eqs, ~A bounds, ~A valid point"
	    (adim x) (numieqs x) (numeqs x)
	    (if (and (lower-bounds x) (upper-bounds x))
		"has" "no")
	    (if (valid-point x)		
		"has" "no")
	    )))

(defmethod print-object ((x v-poly) stream)
  "Print method for V-POLY structures."
  (print-unreadable-object (x stream :type T :identity T)
    (format stream "adim ~D, ~D points, ~D rays"
	    (adim x) (numpoints x) (numrays x))))


(defun make-h-poly-from-ieqfile (ieq-file)
  (declare (type (or stream string pathname) ieq-file))
  (destructuring-bind (dim ieqs lb ub eorder valid obj) 
      (parse-ieq-format ieq-file)
    (declare (ignore obj))
    ;; FIXME: special handling of LB/UB in make-h-poly someday?
    (let ((all-ieqs ieqs))
      (if lb
	  (loop for i from 1 upto dim
	     do (push (list (list (cons i 1)) '>= (svref lb (- i 1))) all-ieqs)))
      (if ub
	  (loop for i from 1 upto dim
	     do (push (list (list (cons i 1)) '<= (svref ub (- i 1))) all-ieqs)))
      (make-h-poly dim all-ieqs :e-order eorder :valid-point valid
		   :lower-bounds lb :upper-bounds ub))))

(defun make-h-poly-from-inefile (ine-file)
  (declare (type (or stream string pathname) ine-file))
  (destructuring-bind (dim ieqs lb ub eorder valid obj) 
      (parse-ine-format ine-file)
    (declare (ignore obj))
    ;; FIXME: special handling of LB/UB in make-h-poly someday?
    (let ((all-ieqs ieqs))
      (if lb
	  (loop for i from 1 upto dim
	     do (push (list (list (cons i 1)) '>= (svref lb (- i 1))) all-ieqs)))
      (if ub
	  (loop for i from 1 upto dim
	     do (push (list (list (cons i 1)) '<= (svref ub (- i 1))) all-ieqs)))
      (make-h-poly dim all-ieqs :e-order eorder :valid-point valid
		   :lower-bounds lb :upper-bounds ub))))


(defun make-v-poly (adim pointlist raylist)
  (let ((numpoints (length pointlist))
	(numrays (length raylist)))
    (let ((pointvec (make-array numpoints :element-type 'point :adjustable T :fill-pointer T
				:initial-contents pointlist))
	  (rayvec   (make-array numrays :element-type 'point :adjustable T :fill-pointer T
				:initial-contents raylist)))
      (make-v-poly-ctor :adim adim
			:numpoints (length pointlist)
			:numrays (length raylist)
			:points pointvec
			:rays rayvec))))

(defun make-v-poly-from-extfile (ext-file)
  "Read an EXT format file and return a v-poly"
  (apply #'make-v-poly (parse-ext-format ext-file)))

(defun make-v-poly-from-poifile (poi-file)
  (throw 'unimplemented NIL)
  )


(deftype stream-or-path ()
  "The type describing the sunion of STREAM and PATHNAME"
  `(or (satisfies pathnamep)
       (satisfies streamp)))

(defmethod fill-slots-from-ieqfile ((instance h-poly) ieq-file)
  (declare (type (or stream pathname) ieq-file))
  (destructuring-bind (dim ieqs lb ub eorder valid obj) 
      (parse-ieq-format ieq-file)
    (declare (ignore obj))
    (declare (type array-index dim)
	     (type (or null simple-array) lb ub eorder valid obj)
	     (type list ieqs))
    (setf (adim instance) dim)
    (let ((all-ieqs ieqs))
      ;; NOTE: indices will be adjusted later by -1, so we have to give them 1-based here like
      ;; we would in an IEQ file
      (if lb
	  (loop for i from 1 upto dim
		do (push (list (list (cons i 1)) '>= (aref lb (- i 1))) all-ieqs)))
      (if ub
	  (loop for i from 1 upto dim
		do (push (list (list (cons i 1)) '<= (aref ub (- i 1))) all-ieqs)))
      (multiple-value-bind (eqs ieqs)
	  (ieqlist->fm-ieq-arrays all-ieqs dim)
	;;(format t "Found ~A and ~A~%" eqs ieqs)
	(setf (eq-rows instance) eqs
	      (ieq-rows instance) ieqs
	      (numieqs instance) (fill-pointer (ieq-rows instance))
	      (numeqs instance) (fill-pointer (eq-rows instance))
	      (num-orig-rows instance) (+ (numieqs instance) (* 2 (numeqs instance)))
	      (fm-info instance) (make-fm-info (num-orig-rows instance))
	      (e-order instance) eorder
	      (valid-point instance) valid))
      ;; normalize all rows to <= and =
      ;; and add references to INSTANCE to all ieqs
      (loop for i across (ieq-rows instance)
	 do (progn
	      (normalize i)
	      (assert (member (sense i) '(<= <) :test #'string=)))
	 do (setf (constituents-poly-reference (fmieq-cinfo i)) instance))
      )))



(defun ensure-ieqrowspace (p num)
  (declare (type h-poly p)
	   (type integer num))
  (if (> num ARRAY-DIMENSION-LIMIT)
      (error "Cannot handle more than ~A rows, write conservative row pointer allocation code~%" num))
  (adjust-array (ieq-rows p) num :element-type '(or fmieq symbol) :initial-element NIL))

(defun store-ieq (p dstidx i)
  "Store I in (aref (ieq-rows P) DSTIDX). 
If DSTIDX is not below fill-pointer of (ieq-rows P), it must be equal to it.
Dynamically extends P by *row-vector-extension-increment* if fill-pointer is at end of allocated space.
Returns I."
  (declare (type h-poly p)
	   (type (integer 0 #.ARRAY-DIMENSION-LIMIT) dstidx)
	   (type fmieq i))
  (assert (<= dstidx (fill-pointer (ieq-rows p))))
  (prog1
      (let ((fp (fill-pointer (ieq-rows p))))
	(cond
	  ((< dstidx fp)
	   (setf (aref (ieq-rows p) dstidx) i))
	  ((= dstidx fp)
	   (vector-push-extend i (ieq-rows p) *row-vector-extension-increment*)
	   (incf (numieqs p))
	   i)
	  (t (error "ieq-row indexing messed up."))))
    ;(format t "~&stored ieq in ~D, numieqs ~D, fp ~D~%" dstidx (numieqs p) (fill-pointer (ieq-rows p)))
    ))

(defun sorted-p (l pred)
  "Check whether the list L is sorted according to PRED, which must be a function."
  (declare (type function pred)
	   (type list l))
  (if (or (null l)
	  (null (cdr l)))
      T
      (and (funcall pred (car l) (cadr l))
	   (sorted-p (cdr l) pred))))

(defun delete-ieqrows (p rowlist)
  "Delete the ieq-rows in ROWLIST from P. ROWLIST must be ordered by #'>."
  (declare (type h-poly p)
	   (type list rowlist))
  (assert (sorted-p rowlist #'> ))
  (assert (= (numieqs p) (fill-pointer (ieq-rows p))))
  (let ((fill-from-idx (- (numieqs p) 1)))
    (declare (type fixnum fill-from-idx)) ;; can become -1 if we end up with an empty system
    (let ((ieq-vec (ieq-rows p)))
      (declare (type (vector (or fmieq null)) ieq-vec))
      (dolist (delidx rowlist)
	(declare (type array-index delidx))
	;;(format t "deleting row ~A~%" delidx)
	(cond
	  ((< delidx fill-from-idx)
	   ;; deletion much in front of us, fill from end
	   (progn
	     (setf (aref ieq-vec delidx)
		   (aref ieq-vec fill-from-idx))
	     (setf (aref ieq-vec fill-from-idx) NIL)
	     (decf fill-from-idx)))
	  ((= delidx fill-from-idx)
	   ;; deletion at end
	   (setf (aref ieq-vec fill-from-idx) NIL)
	   (decf fill-from-idx))
	  (t
	   (error "delidx and fill-from-idx crossed"))))
      (assert (= (- (numieqs p) 1 (length rowlist))
		 fill-from-idx))
      (setf (numieqs p) (1+ fill-from-idx))
      (setf (fill-pointer ieq-vec) (numieqs p))
      )))


;;; Beautification and output
(defun beautify-poly (p)
  "Guarantee integral data and sort rows."
  (if (h-poly-p p)
      (progn
	(delete-empty-ieqs p)
	(integralize-rows p)
	(sort-rows p))))



(defun write-poly-ieq-format (p &key (dst *standard-output*) (fname-info ""))
  "Write out P in ieq format.
Keyword argument DST specifies output stream (default: standard-output) or a pathname or string."
  (declare (type h-poly p))
  (beautify-poly p)
  (with-open-file-or-stream (dst-stream dst :direction :output)
    (format dst-stream "~&DIM = ~D~%~%" (adim p))
    (format dst-stream "~&COMMENT~% Automatically generated by atrop~% ~A~%~%" fname-info)
    ;;   (if (lb p)
    ;;       (loop for i across (lb p)
    ;; 	    initially (format dst-stream "~&LOWER_BOUNDS~%")
    ;; 	    do (format dst-stream "~@A" i)
    ;; 	    finally (format dst-stream "~%~%")))
    ;;   (if (ub p)
    ;;       (loop for i across (ub p)
    ;; 	    initially (format dst-stream "~&UPPER_BOUNDS~%")
    ;; 	    do (format dst-stream "~@A" i)
    ;; 	    finally (format dst-stream "~%~%")))
    (if (valid-point p)
	(loop for i across (the simple-vector (valid-point p))
	   initially (format dst-stream "~&VALID~%")
	   do (format dst-stream " ~D" i)
	   finally (format dst-stream "~%~%")))
    (if (e-order p)
	(loop for i across (the (simple-array array-index) (e-order p))
	   initially (format dst-stream "~&ELIMINATION_ORDER~%")
	   do (format dst-stream " ~D" i)
	   finally (format dst-stream "~%~%")))
    (format dst-stream "~&INEQUALITIES_SECTION~%")
    (if (eq-rows p)
	(loop for equation across (the vector (eq-rows p))
	   for idx of-type array-index from 1
	   do (lrestr:write-ieq-format equation :rownum idx :dst dst-stream)))
    (if (ieq-rows p)
	(loop for inequality across (the vector (ieq-rows p))
	   for idx of-type array-index from (1+ (the array-index (numeqs p)))
	   do (lrestr:write-ieq-format inequality :rownum idx :dst dst-stream)))
    (format dst-stream "~&END~%"))
  )


(defun write-poly-ine-format (p &key (dst *standard-output*) (fname-info ""))
  "Write out P in ine format.
 Keyword argument DST specifies output stream (default: standard-output).
 Keyword artgument FNAME-INFO specifies the string to be put into the
 `filename: '-comment at the top."
  (declare (type h-poly p))
  (beautify-poly p)
  (with-open-file-or-stream (dst-stream dst :direction :output)
    (format dst-stream "~&* filename: ~A~%" fname-info)
    (format dst-stream "* Automatically generated by atrop~%")
    (format dst-stream "H-representation~%")
    (format dst-stream "begin~%")
    (format dst-stream "~4D ~4D integer~%"
	    (+ (numeqs p) (numieqs p))
	    (1+ (adim p)))
    (if (eq-rows p)
	(loop for e across (eq-rows p)
	   do (loop for r of-type rational across (linform e)
		 initially (format dst-stream "~& ~6@A" (rhs e))
		 do (format dst-stream " ~6@A" (- r)))))
    (if (> (numieqs p) 0)
	(loop for e across (ieq-rows p)
	   do (let ((s (if (string= (sense e) '<=) 1 -1)))
		(loop for r of-type rational across (the simple-array (linform e))
		   initially (format dst-stream "~& ~6@A" (* s (the rational (rhs e))))
		   do (format dst-stream " ~6@A" (* s -1 r))))))
    (format dst-stream "~&end~%")
    (if (> (numeqs p) 0)
	(progn
	  (format dst-stream "~&linearity~%")
	  (loop for i from 1 upto (numeqs p)
	     initially (format dst-stream "~5D" (numeqs p))
	     do (format dst-stream " ~4D" i)
	     finally (format dst-stream "~%"))))
    (let ((the-strict-rows (loop for e across (ieq-rows p)
			      for rownum from (1+ (numeqs p))
			      when (strict-sensep (sense e))
			      collect rownum)))
      (unless (null the-strict-rows)
	(format dst-stream "~&strict_inequality~% ~{ ~4D~}~%" the-strict-rows)))))

(defmethod write-poly-latte-format ((p h-poly) &key (dst *standard-output*))
  "Write out P in LattE format.
 Optional argument DST specifies output stream (default: standard-output)."
  (declare (type h-poly p))
  (beautify-poly p)
  (with-open-file-or-stream (dst-stream dst :direction :output)
    (format dst-stream "~4D ~4D~%"
	    (+ (* 2 (numeqs p)) (numieqs p))
	    (1+ (adim p)))
    (if (eq-rows p)
	(loop for e across (eq-rows p)
	   do (integralize e)
	   do (loop for r of-type rational across (linform e)
		 initially (format dst-stream "~& ~6@A" (rhs e))
		 do (format dst-stream " ~6@A" (- r)))))
    (if (> (numieqs p) 0)
	(loop for e across (ieq-rows p)
	   do (integralize e)
	   do (let ((s (if (string= (sense e) '<=) 1 -1)))
		(loop for r of-type rational across (the simple-array (linform e))
		   initially (format dst-stream "~& ~6@A" (* s (the rational (rhs e))))
		   do (format dst-stream " ~6@A" (* s -1 r))))))
    (if (> (numeqs p) 0)
	(progn
	  (format dst-stream "~&linearity~%")
	  (loop for i from 1 upto (numeqs p)
	     initially (format dst-stream "~5D" (numeqs p))
	     do (format dst-stream " ~4D" i)
	     finally (format dst-stream "~%"))))
    (let ((the-strict-rows (loop for e across (ieq-rows p)
			      for rownum from (1+ (numeqs p))
			      when (strict-sensep (sense e))
			      collect rownum)))
      (unless (null the-strict-rows)
	(format dst-stream "~&strict_inequality~% ~{ ~4D~}~%" the-strict-rows)))
    ))

(defun write-poly-lp-format (p &key (dst *standard-output*)
			     (variable-names nil))
  "Write out the h-poly P in CPLEX LP file format.
When VARIABLES-NAMES is non-nil, it must be a vector of variable names
to use instead of the default names x1, x2, ..."
  (declare (type h-poly p))
  (flet ((variable-name (index)
	   (if variable-names
	       (aref variable-names (1- index))
	       (format nil "x~D" index))))
    (with-open-file-or-stream (dst-stream dst :direction :output)
      (when (< (adim p) 1)
	(error "Cannot write a polyhedron in 0-space in LP format."))
      (format dst-stream "~&MINIMIZE~% 0 ~A~%SUBJECT TO~%"
	      (variable-name 1))
      (when (eq-rows p)
	(loop for equation across (the vector (eq-rows p))
	   for idx of-type array-index from 1
	   do (lrestr:write-lp-format equation
				      :row-name (format nil "c~D" idx)
				      :variable-names variable-names
				      :dst dst-stream)))
      (when (ieq-rows p)
	(loop for inequality across (the vector (ieq-rows p))
	   for idx of-type array-index from (1+ (the array-index (numeqs p)))
	   do (lrestr:write-lp-format inequality
				      :row-name (format nil "c~D" idx)
				      :variable-names variable-names
				      :dst dst-stream)))
      ;; The bounds are part of the IEQ-ROWS, so don't repeat them.
      ;; Just declare variables that could take negative variables as free,
      ;; to obtain the simplest possible form.
      (loop
	 initially (format dst-stream "~&BOUNDS~%")
	 for index from 1 upto (adim p)
	 unless (and (upper-bounds p) (not (minusp (svref (upper-bounds p) (1- index)))))
	 do (format dst-stream "~& ~A free~%" (variable-name index)))
      (format dst-stream "~&END~%"))))

(defun write-poly-poi-format (p &key (dst *standard-output*) (fname-info ""))
  "Write out the v-poly P in poi format.
Keyword argument DST specifies output stream (default: standard-output) or a pathname or string."
  (declare (type v-poly p))
  (with-open-file-or-stream (dst-stream dst :direction :output)
    (format dst-stream "~&DIM = ~D~%~%" (adim p))
    (format dst-stream "~&COMMENT~% Automatically generated by atrop~% ~A~%~%" fname-info)
    (flet ((print-generators (section-name generators)
	     (when (and generators 
			(not (zerop (length generators))))
	       (format dst-stream "~&~A~%" section-name)
	       (loop for point across generators
		  for index from 1
		  do (progn 
		       (format dst-stream "~&(~3D) " index)
		       (loop for x across point
			  do (format dst-stream "~D " x))
		       (format dst-stream "~%")))
	       (format dst-stream "~%"))))
      (print-generators "CONV_SECTION" (points p))
      (print-generators "CONE_SECTION" (rays p)))
    (format dst-stream "~&END~%")))

(defun write-poly-ext-format (p &key (dst *standard-output*) (fname-info ""))
  "Write out the v-poly P in EXT format.
Keyword argument DST specifies output stream (default: standard-output) or a pathname or string."
  (declare (type v-poly p))
  (with-open-file-or-stream (dst-stream dst :direction :output)
    (error "Unimplemented")))


;;;; Elimination procedures
(defun fm-elim (p &key (elim-order '()) (elim-vars '()) (split-equations? NIL) (expect-no-duplicates NIL))
  "Run Fourier-Motzkin elimination on P.
Either ELIM-VARS or ELIM-ORDER may be specified, giving the variable indices (0-based counting)
that are to be eliminated.
ELIM-VARS just lists them, ELIM-ORDER specifies them in the order that must be used.
Note: If equations are present in P they are used first to eliminate variables, disobeying ELIM-ORDER.
Afterwards the remaining variables in ELIM-ORDER are eliminated in original sequence.
If SPLIT-EQUATIONS? is non-NIL, equations will be split into two inequalities 
instead of being used for elimination."
  (when (and elim-order elim-vars)
    (error "Only one of ELIM-ORDER and ELIM-VARS may be specified"))
  
  (if split-equations?
      (split-equations p))
  
  (when (null elim-order)
    (setq elim-order (or (elimination-order p)
			 (select-elim-order p (or elim-vars)))))
  
  (unless split-equations?
      ;;; Use equalities to our advantage and see what remains to be done
      ;;; * Gaussify the equations to row-echelon form, following the indices of elim-order
      ;;;   - equations with zero in elim-order columns won't hurt
      ;;;   - equations with nonzero in elim-order columns avoid FM steps: use them for elimination
      (progn
	(setq elim-order
	      (eliminate-using-equations p elim-order))
	;(format t "Remaining indices: (~{ ~D~}) ~%" elim-order)
	))

  ;;(write-poly-ieq-format p :fname-info "after-equations")
  
  ;; remove some redundancy from the ieqs given
  (remove-duplicate-ieqs p)
  (let ((num-before-dd 0))
    (if elim-vars
	;; use best-next-elimination strategy
	(loop for i from 0 below (length elim-order)
	      do (multiple-value-bind (next-var remaining-vars)
		     (find-min-and-rest
		      #'(lambda (x y) 
			  (< (cdr x) (cdr y)))
		      (map 'list
			   #'(lambda (varnum)
			       (cons varnum 
				     (count-expected-fm-combinations p varnum)))
			   elim-order))
		   (setq next-var (car next-var))
		   (setq elim-order (map 'list #'car remaining-vars))
		   ;(format *error-output* "Picked ~A~%" next-var)
		   (fm-elim-var p next-var)
		   (setq num-before-dd (numieqs p))
		   (unless expect-no-duplicates
		     (remove-duplicate-ieqs p)
		     (format *error-output* "~&Removed ~D duplicate~P.~%"
			     (- (- (numieqs p) num-before-dd))
			     (- (- (numieqs p) num-before-dd))))))
	;; use prescribed order
	(dolist (var elim-order)
	  (fm-elim-var p var)
	  (setq num-before-dd (numieqs p))
	  (unless expect-no-duplicates
	    (remove-duplicate-ieqs p)
	    (format *error-output* "~&Removed ~D duplicate~P.~%"
		    (- (- (numieqs p) num-before-dd))
		    (- (- (numieqs p) num-before-dd)))))
	)
    )
  )
  

(defun fm-elim-var (p var)
  "Eliminate variable VAR in P by Fourier-Motzkin. Destructively works on P, returns P."
  (declare (type h-poly p)
	   (type array-index var))
  ;;(format t "~D ieqs in poly~%" (numieqs p))
  ;;(when (= var 6) (break "check elimination of var 6"))

  ;;(write-poly-ieq-format p :fname-info (format nil "before elimination of ~D" var))
  (multiple-value-bind (num-zeros zeros num-positives positives num-negatives negatives)
      (identify-zeros/pos/neg p var)
    (declare (type list positives negatives)
	     (type array-index num-zeros num-positives num-negatives))
    ;;(break "zeros/pos/neg:" (list zeros positives negatives))
    ;; sort by decreasing/increasing nzcnt (this seems to be a good choice by testing on instances we have)
    (setq positives
	  (sort positives #'(lambda (idx1 idx2) (> (nzcnt (aref (ieq-rows p) idx1))
						   (nzcnt (aref (ieq-rows p) idx2)))))
	  negatives
	  (sort negatives #'(lambda (idx1 idx2) (< (nzcnt (aref (ieq-rows p) idx1))
						   (nzcnt (aref (ieq-rows p) idx2))))))
    ;; sort by cinfo constituent count: low numbers first, so the first rows generated have a high chance of
    ;; dominating others
    (setq positives
	  (sort positives #'(lambda (idx1 idx2) (< (constituents-count (fmieq-cinfo (aref (ieq-rows p) idx1)))
						   (constituents-count (fmieq-cinfo (aref (ieq-rows p) idx2))))))
	  negatives
	  (sort negatives #'(lambda (idx1 idx2) (< (constituents-count (fmieq-cinfo (aref (ieq-rows p) idx1)))
						   (constituents-count (fmieq-cinfo (aref (ieq-rows p) idx2)))))))
    (format t "~&round ~3D(~3D), expecting ~6D" 
	    (+ (num-real-elims (fm-info p))
	       (if (= (* num-positives num-negatives) 0)
		   0 1))
	    (+ (num-real-elims (fm-info p))
	       (num-eq-elims (fm-info p))
	       (num-zero-elims (fm-info p))
	       1)
	    (* num-positives num-negatives))
    (force-output t)
    (cond
      ((= 0 num-positives)
       (delete-ieqrows p  (sort negatives #'>))
       (incf (num-zero-elims (fm-info p))))
      ((= 0 num-negatives)
       (delete-ieqrows p  (sort positives #'>))
       (incf (num-zero-elims (fm-info p))))
      ;; FIXME: count eq-elims somewhere
      (t
       ;; Really do elimination.
       (incf (num-real-elims (fm-info p)))
       (adjust-array (zero-idx-vec (fm-info p)) num-zeros
		     :element-type 'array-index :fill-pointer num-zeros :initial-contents zeros)
       ;; We consider pairs (POSROWIDX . NEGROWIDX) to generate DSTROW.
       ;; An index DSTIDX into (ieq-rows p) is kept to record the place where DSTROW should
       ;; be stored.
       ;; We consider one POSROWIDX with all NEGROWIDXs, if NUM-POSITIVES > NUM-NEGATIVES or
       ;; vice-versa.
       ;; Basically, DSTIDX points to places following the old rows in (ieq-rows p) and grows.
       ;; At the end we  kill positives and negatives and compactify
       (dolist (posrowidx positives)
	 (dolist (negrowidx negatives)
	   (let ((dest-slot (fill-pointer (ieq-rows p))))
	     ;;(format t "prow: ") (lrestr:write-ieq-format (aref (ieq-rows p) posrowidx) :rownum posrowidx :dst *standard-output*)
	     ;;(format t "nrow: ") (lrestr:write-ieq-format (aref (ieq-rows p) negrowidx) :rownum negrowidx :dst *standard-output*)
	     ;;(declare (type array-index dstidx posrowidx negrowidx dstrowidx))
	     ;;(format t "posrow ~D, negrow ~D, dest-slot ~D" posrowidx negrowidx dest-slot)
	     ;;(force-output t)(force-output *standard-output*)
	     (if (combine-ieqs p posrowidx negrowidx dest-slot var)
		 ;; true -> was stored
		 (progn
		   ;;(format t "-> stored, numieqs ~D, fp ~D!~%" (numieqs p) (fill-pointer (ieq-rows p)))
		   ;;(force-output t)(force-output *standard-output*)
		   (vector-push-extend dest-slot (zero-idx-vec (fm-info p)) 
				       *row-vector-extension-increment*))
		 ;; false -> not stored
		 (progn
		   ;;(format t "-> NOT stored! , numieqs ~D, fp ~D!~%" (numieqs p) (fill-pointer (ieq-rows p)))
		   ;;(force-output t)(force-output *standard-output*)
		   )
		 
		 ))
	   )
	 ;; this posrowidx is expended, kill it
	 (setf (aref (ieq-rows p) posrowidx) NIL))
       ;; kill positive and negative rows
       ;;(format t "~&now deleting ~A and ~A~%" positives negatives)
       (delete-ieqrows p (sort (union positives negatives) #'>))
       ;; zero-idx-list is now worthless
       (setf (fill-pointer (zero-idx-vec (fm-info p))) 0)
       
       (assert (= (numieqs p) (fill-pointer (ieq-rows p))))
       ;; update statistics
       (setf (num-ieqs-generated (fm-info p))
	     (+ (num-ieqs-generated (fm-info p))
		(- (numieqs p) num-zeros)))
       ;; try strong duffin test:
       (let ((dominated-rows (check-duffin-domination-for-all-rows p)))
	 ;(format t "~&Complete Duffin check finds dominated rows ~A~A~%" 
		 ;dominated-rows (if dominated-rows ", deleting" ""))
	 (delete-ieqrows p dominated-rows))
       )))

  (let ((info (fm-info p)))
    (format t ", generated ~5D, saved ~6D +~5D, deleted ~5D, size ~5D~%"
	    (num-ieqs-generated info)
	    (num-saved-by-chernikovrule info)
	    (num-saved-by-duffinrule info)
	    (num-single-dom-del info)
	    (numieqs p)))	  
  ;;(write-poly-ieq-format p :fname-info (format nil "after elimination of ~D" var))
  )

(defun identify-zeros/pos/neg (p var)
  "Identify positives/negatives/zeros and how many of each we have in column VAR.
Returns 6 values: num-zeros zeros num-positives positives num-negatives negatives.
All lists are ordered wrt. to #'>."
  (declare (type array-index var)
	   (type h-poly p))
  (do ((num-zeros 0)
       (num-positives 0)
       (num-negatives 0)
       (zeros '())
       (positives '())
       (negatives '())
       (idx 0 (1+ idx)))
      ((= idx (numieqs p)) (values num-zeros zeros num-positives positives num-negatives negatives))
    (declare (type array-index num-zeros num-positives num-negatives idx))
    (let ((val (svref (linform (aref (ieq-rows p) idx)) var)))
      (declare (type rational val))
      (assert (not (eq '= (sense (aref (ieq-rows p) idx))))) ;; not handled here
      (cond
	((< val 0) (progn (incf num-negatives)
			  (push idx negatives)))
	((> val 0) (progn (incf num-positives)
			  (push idx positives)))
	((= val 0) (progn (incf num-zeros)
			  (push idx zeros)))))))

(defun identify-useful-eqs (p eorder)
  "Split the indices of the equalities in P according to usefulness
for elimination with EORDER. Returns two lists: USELESS-IDX-LIST of
equation-indices that are useless and USEFUL-IDX-LIST/ELIM-INDICES, a
list of pairs containing an equation index and the column index from
EORDER that can be eliminated with this equation. The ELIM-INDICES are
guaranteed to be in the same order as they occur in EORDER.

This function performs gauss elimination and deletes empty equations
on the equality rows of P destructively."
  ;; Convert the equations to row-echelon form using Gauss elimination first.
  (gauss-eliminate (eq-rows p) :colsequence eorder)
  ;; Clean up. This also checks for infeasibility of the equation-system and signals appropriately.
  (delete-empty-equations p)
  
  ;; Gauss generated a row-echelon form, but we have to read it through the
  ;; eorder list, it's not left-to-right as one might expect
  (do ((elim-vars eorder (cdr elim-vars))
       (eq-indices (iota (numeqs p)))
       (useful-idx/elim-idx-list '())
       )
      ((or (null eq-indices) ;; no more equations
	   (null elim-vars)) ;; more equation rank than elimination 
       (values eq-indices (reverse useful-idx/elim-idx-list)))
    (let ((e (aref (eq-rows p) (car eq-indices))))
      (when (not (zerop (svref (linform e) (car elim-vars))))
	;; this row is useful
	(push (cons (car eq-indices)
		    (car elim-vars))
	      useful-idx/elim-idx-list)
	;; this equation is used-up
	(pop eq-indices)))))

(defun eliminate-using-equations (p elim-order)
  "Perform as much elimination according to ELIM-ORDER using equations present in P.
Returns the shortened ELIM-ORDER that lists the remaining work."
  (multiple-value-bind (untouched-eqs useful-eqs/elim-indices)
      (identify-useful-eqs p elim-order)
    ;;(break "After Gauss")
    ;;(write-poly-ieq-format p :fname-info "after gaussification")
    ;; Now eliminate with equations in useful-eqs/elim-indices
    (when (and (not (null useful-eqs/elim-indices))
	       (< 0 (length (ieq-rows p)))) ;; we may be facing an empty ieq set
      (loop for eqidx/elimidx in useful-eqs/elim-indices
	 initially (format t "Using equation~P for elimination of: " (length useful-eqs/elim-indices))
	 do (progn
	      (format t "x~D " (cdr eqidx/elimidx))
	      (eliminate-col-using-eq p (cdr eqidx/elimidx) (car eqidx/elimidx)))
	 finally (format t "~%")))
    
    ;; keep only untouched equations: the others are used up
    (loop for eqrow in untouched-eqs
       for dstidx from 0
       do (setf (aref (eq-rows p) dstidx) (aref (eq-rows p) eqrow))) ;; relies on ordering of untouched-eqs
    (setf (numeqs p) (length untouched-eqs)
	  (fill-pointer (eq-rows p)) (numeqs p))

    ;; clean up elim-order by removing the indices already handled
    (setq elim-order
	  (delete-if (lambda (x)
		       (member x useful-eqs/elim-indices :key #'cdr))
		     elim-order))
;;     (do ((remaining-elim-indices '())
;; 	 (elim-indices elim-order (cdr elim-indices))
;; 	 (u-e/e-i useful-eqs/elim-indices (cdr u-e/e-i)))
;; 	((or (null elim-indices)
;; 	     (null u-e/e-i)) (progn
;; 			       ;(break)
;; 			       (setq elim-order (append 
;; 						 (reverse remaining-elim-indices)
;; 						 elim-indices))))
;;       (let ((i (car elim-indices)))
;; 	(if (not (= i (cdar u-e/e-i)))
;; 	    ;; keep this idx
;; 	    (setq remaining-elim-indices (cons i remaining-elim-indices))))
;;       )
    elim-order
    ))

(defun eliminate-col-using-eq (p colidx eqidx)
  "Eliminate column COLIDX in P using equation at EQIDX.
Assumes that the equation already has a '1' in column COLIDX (as guaranteed by gauss)."
  (let ((e (aref (eq-rows p) eqidx)))
    (assert (= 1 (svref (linform e) colidx)))
    ;(break "Before elimination in column ~D~%" colidx)
    (loop for i of-type fmieq across (ieq-rows p)
       do (let ((pivelt (svref (linform i) colidx)))
	    (declare (type rational pivelt))
	    (unless (zerop pivelt)
	      (loop for col from 0 below (adim p)
		 initially (setf (rhs i) (- (rhs i) (* pivelt (rhs e))))
		 do (setf (svref (linform i) col)
			  (- (svref (linform i) col)
			     (* pivelt (svref (linform e) col)))))
	      (beautify i)
	      ;(break "Break after eliminating in row ~A~%" i)
	      ))
       finally
	 (progn
	     (assert (zerop (svref (linform i) colidx)))
	     ;; count successful elimination
	     (incf (num-eq-elims (fm-info p)))	     
	 ))))

(defun combine-senses (s1 s2)
  ;(declare (type (member '< '<= '> '>=) s1 s2))
  (assert (and (member s1 '(LRESTR::< LRESTR::<=) :test #'eq)
	       (member s2 '(LRESTR::< LRESTR::<=) :test #'eq)))
  (the sense-symbol
    (if (or (strict-sensep s1) (strict-sensep s2))
	'LRESTR::<
	'LRESTR::<=)))

(defun combine-ieqs (p pos-idx neg-idx dst-idx elim-col)
  "Combine the inequalities indexed by pos-idx and neg-idx in IEQ-ARRAY to zero column ELIM-COL, and store the result
in the row indexed by DST.
Returns NIL if the new ieq is redundant and was not stored in DST, T otherwise.
FM-ROUND counts the number of FM-eliminations already performed on IEQ-ARRAY, starting at 0."
  (declare (type integer pos-idx neg-idx dst-idx elim-col)
	   (type h-poly p))
  (assert (and (member (sense (aref (ieq-rows p) pos-idx)) '(<= <) :test #'string=)
	       (member (sense (aref (ieq-rows p) neg-idx)) '(<= <) :test #'string=))) ;; otherwise we have to normalize
  (assert (> (nzcnt (aref (ieq-rows p) pos-idx)) 0))
  (assert (> (nzcnt (aref (ieq-rows p) neg-idx)) 0))
  (if (check-chernikov-domination p pos-idx neg-idx)
      (progn
	(incf (num-saved-by-chernikovrule (fm-info p)))
	(return-from combine-ieqs NIL)))

  (let ((constituent-info (check-duffin-domination p  pos-idx  neg-idx elim-col)))
    (if (not constituent-info)
	(progn
	  (incf (num-saved-by-duffinrule (fm-info p)))
	  (return-from combine-ieqs NIL)))
    ;; perform elimination
    (let* ((prow (aref (ieq-rows p) pos-idx))
	   (nrow (aref (ieq-rows p) neg-idx))
	   (d  (if (and (< dst-idx (fill-pointer (ieq-rows p)))
			(fmieq-p (aref (ieq-rows p) dst-idx)))
		   (aref (ieq-rows p) dst-idx)
		   ;; create new ieq on demand
		   (store-ieq p dst-idx
			      (make-fmieq :linform (make-array (adim p)
							       :element-type 'rational
							       :initial-element 0
							       :adjustable NIL)
					  :sense 'LRESTR::<=
					  :rhs -4711
					  :nzcnt 0
					  :cinfo constituent-info))))
	   )
      (declare (type inequality prow nrow))
      (assert (and (member  (sense prow) '(<= <) :test #'string=)
		   (member (sense nrow) '(<= <) :test #'string=)
		   d))
      (setf (fmieq-cinfo d) constituent-info) ;; necessary if we re-used the slot
      (setf (sense d) (combine-senses (sense prow) (sense nrow)))
      (setf (fmieq-creation-round d) (num-real-elims (fm-info p)))
      (let ((p (svref (linform prow) elim-col))
	    (n (svref (linform nrow) elim-col)))
	(declare (type rational p n))
	;(format t "p=~D, n=~D~%" p n)
	(assert (and (< n 0) (> p 0)))

	(loop for i from 0 below (length (the (simple-array rational (*)) (linform prow)))
	   initially
	     (setf (fmieq-rhs d) (- (* p (fmieq-rhs nrow))
				    (* n (fmieq-rhs prow))))
	   do
	     (setf (svref (fmieq-linform d) i)
		   (- (* p (svref (fmieq-linform nrow) i))
		      (* n (svref (fmieq-linform prow) i))))))
      (beautify d)
      (assert (= (svref (linform d) elim-col) 0))
      (assert (= (nzcnt d) (loop for i from 0 below (length (linform d)) count (not (zerop (svref (linform d) i))))))
      (if (= 0 (nzcnt d))
	  (if (or (and (string= (sense d) '<) (<= (rhs d) 0))
		  (and (string= (sense d) '<=) (< (rhs d) 0)))
	      (progn
		;; let user know we found inconsistency ...
		(signal 'inconsistent-ieq :inconsistent-row dst-idx)
		;; ... but keep it in the system
		(return-from combine-ieqs T))
	      ;; else: empty ieq constructed, that is redundant
	      (progn
		;; we have to correct numieqs and fp here, because the above
		;; code assumes that we usually only allocate the slot when
		;; we know the ieq will survive.
		;(format t "empty ieq constructed~%")
		(unless (< dst-idx (fill-pointer (ieq-rows p)))
		  (format t "reducing numieqs, fill-pointer~%")
		  (decf (numieqs p))
		  (decf (fill-pointer (ieq-rows p))))
		(return-from combine-ieqs NIL)))
	  )))
  (return-from combine-ieqs T))

;(declaim (inline compute-new-cdata))
(defun compute-new-cdata (p idx1 idx2)
  "Compute a new constituent-data object from rows IDX1 and IDX2."
  (let ((c1 (fmieq-cinfo (aref (ieq-rows p) idx1)))
	(c2 (fmieq-cinfo (aref (ieq-rows p) idx2))))
    (if (and (constituent-list-p c1) (constituent-list-p c2))
	(let ((data (sort
		     (copy-list
		      (union (constituent-list-data c1)
			     (constituent-list-data c2)
			     :test #'=))
		     #'<)))
	  (make-constituent-list
	   :poly-reference (constituents-poly-reference c1)
	   :data data 
	   :count (length data)))
	(progn
	  (if (constituent-list-p c1)
	      (setf c1 (ensure-constituent-bits (ieq-rows p) idx1))
	      (setf c2 (ensure-constituent-bits (ieq-rows p) idx2)))
	  (let ((data (bit-ior (constituent-bits-data c1) (constituent-bits-data c2))))
	    (make-constituent-bits 
	     :count (count-1-bits data)
	     :data data
	     :poly-reference (constituents-poly-reference c1)))))))

(defun check-duffin-domination-for-all-rows (p)
  "Check every row using Duffin's criterion against all other rows. Deletes dominated ones."
  (declare (type h-poly p))
  (let ((dominated-rows '())
	(cinfos (make-array (numieqs p) :element-type 'constituents :adjustable NIL)))
    (sort-rows-by-constituentcount>= p)
    ;; extract cinfo to avoid repeated fetches
    (loop for r of-type array-index from 0 below (numieqs p)
	  do (setf (aref cinfos r) (fmieq-cinfo (aref (ieq-rows p) r))))
    ;; compare each row by rows following it; only those have fewer nonzeros and can dominate
    (loop for this-row of-type array-index from 0 below (numieqs p)
	  do (let ((this (svref cinfos this-row)))
	       (if (loop for other-row of-type array-index from (1+ this-row) below (numieqs p)
			 thereis (when (check-constituent-domination p (svref cinfos other-row) this) T))
		   (push this-row dominated-rows))))
    dominated-rows))
			   
  
(defun check-duffin-domination (p idx1 idx2 elim-var)
  "Duffin rule: Dominated inequalities can be distinguished by
  looking at their original constituents.
Check Duffin's criterion for a row generated from idx1 and idx2.
Return the domination data for the generated row, or NIL if the new row would be dominated."
  (declare (type poly p)
	   (type array-index idx1 idx2 elim-var))
  (let ((new (compute-new-cdata p idx1 idx2)))
    ;(return-from check-duffin-domination new) ;; debug: disable check
    (loop for row-idx  across (zero-idx-vec (fm-info p))
       do (if (check-constituent-domination p row-idx new)	;; new one dominated?
	      (return-from check-duffin-domination NIL)))
    ;; no dominator found, so this one survives:
    (return-from check-duffin-domination new)))

(defun count-unique-entries (l1 l2)
  "Count unique entries in union of L1 and L2 which must be sorted."
  (do ((unique-row-count 0 (1+ unique-row-count)))
      ((or (null l1) (null l2))
       (+ unique-row-count (the array-index (+ (length l1) (length l2)))))
    (declare (type array-index unique-row-count))
    (let ((n1 (car l1))
	  (n2 (car l2)))
      (declare (type array-index n1 n2))
      (cond 
	((< n1 n2)
	 (setf l1 (cdr l1)))
	((> n1 n2)
	 (setf l2 (cdr l2)))
	(t 
	 (setf l1 (cdr l1)
	       l2 (cdr l2)))))))

(defun check-chernikov-domination (p idx1 idx2)
  "Chernikov rule: An inequality is redundant if in the k-th
 elimination step (only counting those steps where at least 1 new
 row was created) and inequality is derived from k+2 or more
 original inequalities.
Return T if combining ieqs at IDX1 and IDX2 will result in a dominated ieq,
NIL if not"
  (declare (type h-poly p)
	   (type array-index idx1 idx2))
  ;(return-from check-chernikov-domination NIL) ;; debug: disable any check
  ;; Chernikov rule: An inequality is redundant if in the k-th
  ;; elimination step (only counting those steps where at least 1 new
  ;; row was created) and inequality is derived from k+2 or more
  ;; original inequalities
  ;;(format t "CCD: ~D, ~D~%" idx1 idx2)
  (let ((cinfo1 (fmieq-cinfo (aref (ieq-rows p) idx1)))
	(cinfo2 (fmieq-cinfo (aref (ieq-rows p) idx2))))
    ;; switch to bit-representation if lists get long
    ;; FIXME: needs tuning wrt. bitvector-length
    ;; and we should also decide whether to keep the bit-representation permanently then, 
    ;; conversion is expensive!
    (when (or (>= (constituents-count cinfo1) 150)
	      (>= (constituents-count cinfo2) 150))
      (format t "Switching to bit-representation for rows ~D and ~D.~%" idx1 idx2)
      (setq cinfo1 (ensure-constituent-bits (ieq-rows p) idx1)
	    cinfo2 (ensure-constituent-bits (ieq-rows p) idx2))
      (assert (constituent-bits-p (fmieq-cinfo (aref (ieq-rows p) idx1))))
      (assert (constituent-bits-p (fmieq-cinfo (aref (ieq-rows p) idx2)))))
    ;; perform domination test
    (cond
      ((and (constituent-list-p cinfo1) (constituent-list-p cinfo2))
       ;; handle two lists
       (if (>= (the array-index
		 (count-unique-entries (constituent-list-data cinfo1)
				       (constituent-list-data cinfo2)))
	       (+ 2 (num-real-elims (fm-info p))))
	   T NIL))
      ((and (constituent-bits-p cinfo1) (constituent-bits-p cinfo2))
       (let ((common-bits (bit-ior (constituent-bits-data cinfo1)
				   (constituent-bits-data cinfo2)
				   (the simple-bit-vector (cinfo-scratch-bf (fm-info p))))))
	 (if (>= (the array-index
		   (count-1-bits common-bits))
		 (the fixnum
		   (+ 2 (num-real-elims (fm-info p)))))
	     T NIL)))
      (t ;; happens sometimes even for low COUNTS if the cinfo was upgraded earlier -- convert back?
       (let ((c1 (ensure-constituent-bits (ieq-rows p) idx1))
	     (c2 (ensure-constituent-bits (ieq-rows p) idx2)))
	 (assert (constituent-bits-p (fmieq-cinfo (aref (ieq-rows p) idx1))))
	 (assert (constituent-bits-p (fmieq-cinfo (aref (ieq-rows p) idx2))))
	 (if (>= (the array-index
		   (count-1-bits (bit-ior (constituent-bits-data c1)
						 (constituent-bits-data c2)
						 (the simple-bit-vector
						   (cinfo-scratch-bf (fm-info p))))) )
		 (+ 2 (num-real-elims (fm-info p))))
	     T NIL)))
	 )))

;; Utility functions
(defun var/val-pairs->array (pairlist dimension)
  "Create an array of rationals of size DIMENSION with entries specified by PAIRLIST.
For each pair (idx . val) in PAIRLIST, sets array[idx]=val, all others are 0.
The pairs can be in any order wrt. idx."
  (declare (type integer dimension))
  (let ((result
	 (do ((row-array (make-array dimension
				     :element-type 'rational
				     :adjustable NIL :initial-element 0))
	      (rt pairlist (cdr rt)))
	     ((null rt) row-array)
    (destructuring-bind (varidx . value)
	(car rt)
      (declare (type (integer 0) varidx)
	       (type rational value))
      (setf (aref row-array (- varidx 1)) value)))))
    ;;(format t "~%var/val-pairs->array ~A -> ~A" pairlist result)
    result))


(defun ieqlist->fm-ieq-arrays (l nvars)
  "Takes a list of inequalities as returned by the IEQ parser, or a list of INEQUALITY or EQUATION objects,
or a list of triples LINFORM, SENSE, RHS
and generates two arrays: fm-ieqs and equations.
 NVARS is the number of columns of the matrix.
Returns values EQUATION-ARRAY INEQUALITY-ARRAY."
  (declare (type (integer 0) nvars)
	   (type list l))
  (let ((ieq-count (length l)))
    (do ((ieqs (make-array ieq-count :element-type 'fmieq :adjustable t :fill-pointer 0))
	 (eqs  (make-array ieq-count :element-type 'equation :adjustable t :fill-pointer 0))
	 (rt l (cdr rt)))
	((null rt) (values eqs ieqs))
      (if (lrestr-p (car rt))
	  (if (equation-p (car rt))
	      (vector-push (car rt) eqs)
	      (vector-push 
	       (make-fmieq :linform (linform (car rt))
			   :nzcnt (nzcnt (car rt))
			   :rhs (rhs (car rt))
			   :sense (translate-sense (sense (car rt)))
			   :creation-round 0
			   :cinfo (make-constituent-list
				   :poly-reference nil
				   :count 1
				   :data (list (fill-pointer ieqs))))
	       ieqs))
	  (destructuring-bind (pairs-or-linform sense rhs)
	      (car rt)
	    (let ((sane-sense (translate-sense sense))
		  (linform-sv (if (simple-vector-p pairs-or-linform)
				  pairs-or-linform
				  (var/val-pairs->array pairs-or-linform nvars))))
	      (cond
		((equality-sensep sane-sense)
		 (vector-push (make-equation
			       :linform linform-sv
			       :nzcnt (count-if-not #'zerop linform-sv)
			       :rhs rhs)
			      eqs))
		((nonequality-sensep sane-sense)	       
		 (vector-push (make-fmieq
			       :linform linform-sv
			       :nzcnt (count-if-not #'zerop linform-sv)
			       :rhs rhs
			       :sense sane-sense 
			       :creation-round 0
			       :cinfo (make-constituent-list
				       :poly-reference nil
				       :count 1
				       :data (list (fill-pointer ieqs))))
			      ieqs)))))))))
  
(defun count-1-bits (v)
  "Count the number of 1-bits in the bit-vector V."
  ;; NOTE: This could be improved by 32-bit or 64-bit-chunk comparisons with a prepared
  ;; counting table, if we assume that bit-and + table lookup are faster than 32 or 64 bit-index-comparisons in
  ;; the loop.
  (declare (type simple-bit-vector v))
  (loop for idx of-type array-index from 0 below (length v)
	when (= (aref v idx) 1)
	count 1))


(defun elimination-order (p)
  "Return a list of the sequence of eliminations requested for P."
  (declare (type h-poly p))
  (if (not (e-order p))
      '()
      (let* ((idxlist (iota (adim p)))
	     (sorted (sort idxlist 
			   (lambda (idx1 idx2)
			     "Compare using  elim-order[idx1] and [idx2]"
			     (> (aref (e-order p) idx1)
				(aref (e-order p) idx2))))))
	(do ((tail sorted (cdr tail))
	     (result '()))
	    ((null tail) result)
	  (if (= 0 (aref (e-order p) (car tail)))
	      (return-from elimination-order result)
	      (push (car tail) result))))))

(defun count-expected-fm-combinations (p col)
    "Compute the number of expected fm-combinations for COL"
  (let ((positives 0)
	(negatives 0))
    (loop for i from 0 below (numieqs p)
	  do (let ((val (svref (the simple-vector (linform (aref (ieq-rows p) i))) col)))
	       (cond
		 ((< val 0) (incf negatives))
		 ((> val 0) (incf positives)))))
    (* positives negatives)))

(defun compare-vars-by-expected-fm-combinations (p i j)
    "Compare variables i and j by the number of expected FM-combinations"
  (< (count-expected-fm-combinations p i)
     (count-expected-fm-combinations p j)))

(defun find-min-and-rest (pred l)
  "Find a minimal element in L using PRED and return a pair of it and the (possibly re-ordered) list L.
Returns two values: a minimal element and the remaining list.
If L was empty returns and '()"
  (if (null l)
      (values NIL '())
      (let ((min-elt (first l))
	    (rest '())
	    (tail (cdr l)))
	(loop for x in tail
	      if (funcall pred x min-elt)
	      do (progn
		   (push min-elt rest)
		   (setq min-elt x))
	      else do (push x rest))
	(values min-elt rest))))

  
(defun select-elim-order (p elim-vars)
  "Return an elimination order that is suitable for eliminating the variables listed in ELIM-VARS from P."
  (declare (type h-poly p)
	   (type list elim-vars))
  (sort elim-vars #'(lambda (i j) (compare-vars-by-expected-fm-combinations p i j))))

(defun delete-empty-equations (p)
  "Check equations of P for empty rows from highest equation-number down. Remove empty rows, if rhs is also 0.
Signal infeasibility if rhs of such a row is nonzero."
  (declare (type h-poly p))
  (unless (= (numeqs p) 0)
    (loop for i of-type array-index from (- (numeqs p) 1) downto 0
       always (= 0 (nzcnt (aref (eq-rows p) i)))
       do (if (not (zerop (rhs (aref (eq-rows p) i))))
	      (signal 'inconsistent-equation :inconsistent-row i)
	      (setf (fill-pointer (eq-rows p)) (- (fill-pointer (eq-rows p)) 1)
		    (numeqs p) (- (numeqs p) 1))))))

(defun delete-empty-ieqs (p)
  "Check ieqs of P for empty inequalities. Remove empty rows."
  (declare (type h-poly p))
  ;(format t "~% ~A ieqs~%" (numieqs p))
  (let ((empty-rows 
	 (loop for i of-type array-index from 0 below (numieqs p)
	    when (or (null (aref (ieq-rows p) i))
		     (and (= 0 (nzcnt (aref (ieq-rows p) i)))
			  (not (inequality-clearly-infeasible
				(aref (ieq-rows p) i)))))
	    collect i)))
    ;(format t "~% About to delete rows ~A because LHS is empty~%" empty-rows)
    (delete-ieqrows p (nreverse empty-rows))
    ))
  
(defun remove-duplicate-ieqs (p &key (check-pairs? nil))
  "Check ieqs of P for single-row domination and remove the weaker ones.
 Relies on the ieqs being only '< and '<= - oriented.
Integralizes all rows.
 If check-pairs is T, also check pairwise domination."
  (block removedups
    (unless (= (numieqs p) 0)
      (let ((row-hashes (compute-row-hashes p)))
	;;(format t "HT: ~A~%" row-hashes)
 	(multiple-value-bind (single-dom-count single-dominations)
 	    (identify-single-dominations p row-hashes)
 	  (declare (type array-index single-dom-count))
 	  (delete-ieqrows p (sort single-dominations #'>))
	  (incf (num-single-dom-del (fm-info p)) single-dom-count)))
       (when check-pairs?
 	(remove-pair-dominations p)))))
  
;; (defun remove-duplicate-ieqs (p &key (check-pairs? nil))
;;   "Check ieqs of P for single-row domination and remove the weaker ones. Relies on the ieqs being only
;; '< and '<= - oriented.
;; If check-pairs is T, also check pairwise domination."
;;   (block removedups
;;     (unless (= (numieqs p) 0)
;;       (let ((row-hashes (compute-row-hashes p)))
;; 	;(format t "HT: ~A~%" row-hashes)
;; 	(multiple-value-bind (single-dom-count single-dominations)
;; 	    (identify-single-dominations p row-hashes)
;; 	  (declare (type array-index single-dom-count))
;; 	  (delete-ieqrows p (sort single-dominations #'>))
;; 	  (incf (num-single-dom-del (fm-info p)) single-dom-count)))
;;       (when check-pairs?
;; 	(remove-pair-dominations p)))))

(defun row-hash-function (vec)
  "Compute a linear hash. Returns an INTEGER."
  (declare (type (simple-array rational *) vec))
  (the integer 
    (let ((result 0))
      (declare (type rational result))
      (loop for x of-type rational across vec
	 do (setq result (+ (* 31 result) x)))
      (+ (* 31 (numerator result)) (denominator result)))))
  
(defun compute-row-hashes (p)
  "Compute a hashtable for the ieqs of P.
Assumes that all ieqs are normalized, so that rows that were multiples of one another can only differ in their rhs. Hence, their hashes will be the same for our hash function."
  (declare (type h-poly p))
  (let ((size (numieqs p)))
    (declare (type (integer 1 #.ARRAY-DIMENSION-LIMIT) size))
    (let* ((ht (make-htable size :hash #'row-hash-function :equalp #'equal)))
      (loop for idx from 0 below (numieqs p)
	 do (let ((i (aref (ieq-rows p) idx)))
	      (normalize i)
	      (assert (member (sense i) '(< <=) :test #'string=))
	      (htable-set ht (linform (aref (ieq-rows p) idx)) idx)))
      ht)))

(defun compute-linear-row-hashes (p)
  "Compute a hashtable for the ieqs of P.
Integralizes the ieqs so that a linear hash can be computed, modifying the ieq-rows of P."
  (declare (type h-poly p))
  (let ((size (numieqs p)))
    (declare (type (integer 1 #.ARRAY-DIMENSION-LIMIT) size))
    (let* ((ht (make-htable size :hash #'(lambda (size) (x31-hash size))
			    :equalp #'equal)))
      (loop for idx from 0 below (numieqs p)
	 do (let ((i (aref (ieq-rows p) idx)))
	      (integralize i :include-rhs NIL)
	      (assert (member (sense i) '(< <=) :test #'string=))
	      (htable-set ht (linform (aref (ieq-rows p) idx)) idx)))
      ht)))

    
(defun identify-single-dominations (p row-hashes)
  "For each row in P identify weaker rows.
Row-hashes must be up to date!
Returns the number of deletable rows and the list of ieq-indices that should be deleted."
  (declare (type h-poly p)
	   (type htable row-hashes))
  ;(format t "identify-single-dominations: called with table ~A~%" row-hashes)
  ;; the ROW-HASHES are a linear hash table computed from the lhs of each ieq-row,
  ;; so we know that duplicates (with maybe differing rhs) occur only if the hash
  ;; value is the same.
  ;; Since the ieq-rows are integralized *EXCLUDING THE RHS*FOR THE RHS* there will be no 
  ;; multiples of one row occurring, but only duplicates
  ;; Since the hash is linear we can identify potential 2-row-domination by checking for ...
  ;(loop 
  (let ((dominations
	 (htable-fold-each-bucket 
	  (lambda (bucket result)
	    (let ((l '()))
	      (dolist (linform/index bucket)
		(let ((obj (assoc (car linform/index) l :test  #'equalp)))
		  (if obj
		      (rplacd obj (cons (cdr linform/index) (cdr obj)))
		      (setq l (acons (car linform/index) 
				     (list (cdr linform/index))
				     l)))))
	      ;;(format t "processed ~A, found ~A~%" bucket l)
	      (append l result)))
	  '()
	  row-hashes)))
    ;;(format t "Computed dominations ~A~%" dominations)
    ;; do something useful
    (let ((rows-to-be-deleted '())
	  (row-del-count 0))
      (declare (type array-index row-del-count))
      (dolist (lf/idx-list dominations)
	(when (> (length (cdr lf/idx-list)) 1)
	  ;(format t "Have potential domination among rows ~{ ~D~}~%" 
		;  (cdr lf/idx-list))
	  (let* ((minidx (first (cdr lf/idx-list)))
		 (minrhs (rhs (aref (ieq-rows p) minidx)))
		 (minsense (sense (aref (ieq-rows p) minidx))))
	    (loop for rowidx in (cddr lf/idx-list)
	       do (let ((r (rhs (aref (ieq-rows p) rowidx))))
		    (if (or (< r minrhs)
			    (and (string= minsense '<=)
				 (string= (sense (aref (ieq-rows p) rowidx)) '<)))
			(progn
			  (push minidx rows-to-be-deleted)
			  (setq minidx rowidx
				minrhs r
				minsense (sense (aref (ieq-rows p) rowidx))))
			(push rowidx rows-to-be-deleted))
		    (incf row-del-count)
		    ))
;; 	    (format t "Minimal rhs is ~A in row ~D with sense ~A~%" 
;; 		    minrhs minidx minsense)
	  )
	))
      (assert (= row-del-count (length rows-to-be-deleted)))
      (values row-del-count rows-to-be-deleted)
    )))

;; (defun identify-single-dominations (p row-hashes)
;;   "For each row in P identify weaker rows.
;; Row-hashes must be up to date!
;; Returns the number of deletable rows and the list of ieq-indices that should be deleted."
;;   (declare (type h-poly p)
;; 	   (type htable row-hashes))
;;   ;(format t "identify-single-dominations: called with table ~A~%" row-hashes)
;;   (let ((dominations
;; 	 (htable-fold-each-bucket 
;; 	  (lambda (bucket result)
;; 	    (let ((l '()))
;; 	      (dolist (linform/index bucket)
;; 		(let ((obj (assoc (car linform/index) l :test  #'equalp)))
;; 		  (if obj
;; 		      (rplacd obj (cons (cdr linform/index) (cdr obj)))
;; 		      (setq l (acons (car linform/index) 
;; 				     (list (cdr linform/index))
;; 				     l)))))
;; 	      '(format t "processed ~A, found ~A~%" bucket l)
;; 	      (append l result)))
;; 	  '()
;; 	  row-hashes)))
;;     '(format t "Computed dominations ~A~%" dominations)
;;     ;; do something useful
;;     (let ((rows-to-be-deleted '())
;; 	  (row-del-count 0))
;;       (declare (type array-index row-del-count))
;;       (dolist (lf/idx-list dominations)
;; 	(when (> (length (cdr lf/idx-list)) 1)
;; 	  ;(format t "Have potential domination among rows ~{ ~D~}~%" 
;; 		;  (cdr lf/idx-list))
;; 	  (let* ((minidx (first (cdr lf/idx-list)))
;; 		 (minrhs (rhs (aref (ieq-rows p) minidx)))
;; 		 (minsense (sense (aref (ieq-rows p) minidx))))
;; 	    (loop for rowidx in (cddr lf/idx-list)
;; 	       do (let ((r (rhs (aref (ieq-rows p) rowidx))))
;; 		    (if (or (< r minrhs)
;; 			    (and (string= minsense '<=)
;; 				 (string= (sense (aref (ieq-rows p) rowidx)) '<)))
;; 			(progn
;; 			  (push minidx rows-to-be-deleted)
;; 			  (setq minidx rowidx
;; 				minrhs r
;; 				minsense (sense (aref (ieq-rows p) rowidx))))
;; 			(push rowidx rows-to-be-deleted))
;; 		    (incf row-del-count)
;; 		    ))
;; ;; 	    (format t "Minimal rhs is ~A in row ~D with sense ~A~%" 
;; ;; 		    minrhs minidx minsense)
;; 	  )
;; 	))
;;       (assert (= row-del-count (length rows-to-be-deleted)))
;;       (values row-del-count rows-to-be-deleted)
;;     )))

(defun remove-pair-dominations (p)
  "For each pair of rows in P identify rows that are in the convex span of them and delete them.
Row-hashes must be up to date!"
  (declare (type h-poly p))
  (format t "remove-pair-dominations unimplemented~%")
  )

(defun integralize-rows (p)
  "Transform to integral rows"
  (format *error-output* "~&Transforming to integers~%")
  (loop for i from 0 below (numeqs p)
     do (integralize (aref (eq-rows p) i)))
  (loop for i from 0 below (numieqs p)
     do (integralize (aref (ieq-rows p) i))))

(defun sort-rows-by-constituentcount>= (p)
  "Sort the ieq-rows of P, by order of decreasing constituent count."
  (declare (type h-poly p))
  (setf (ieq-rows p)
	(sort (ieq-rows p) #'(lambda (r1 r2)
			       (>= (constituents-count (fmieq-cinfo r1))
				   (constituents-count (fmieq-cinfo r2)))))))
				  
(defun sort-rows (p)
  "Sort the eq-rows and the ieq-rows of P, increasingly according to the following criteria:
 * rhs value
 * lexicocgraphically on the linpart."
  (declare (type h-poly p))
  (setf (ieq-rows p)
	(sort (ieq-rows p) #'compare-<)))


(defun split-equations (p)
  "Split all (eq-rows p) into inequalities and add them to (ieq-rows p).
New ieqs are assigned a creation-round of 0. Afterwards remove them from eq-rows."
  (loop for e across (eq-rows p)
     do ;; split equation
       (multiple-value-bind (e1 e2)
	   (split e)
	 (normalize-sense e1)
	 (normalize-sense e2)
	 (format t "Debug: numieqs ~D, num-orig-rows ~D~%" (numieqs p) (num-orig-rows p))
	 (vector-push-extend 
	  (make-fmieq 
	   :linform (linform e1) :nzcnt (nzcnt e1) :rhs (rhs e1) :sense (sense e1)
	   :creation-round 0
	   :cinfo (make-constituent-list :poly-reference p
					 :count 1
					 :data (list (fill-pointer (ieq-rows p)))))
	  (ieq-rows p) *row-vector-extension-increment*)
	 (vector-push-extend 
	  (make-fmieq
	   :linform (linform e2) :nzcnt (nzcnt e2) :rhs (rhs e2) :sense (sense e2)
	   :creation-round 0
	   :cinfo (make-constituent-list :poly-reference p
					 :count 1
					 :data (list (fill-pointer (ieq-rows p)))))
	  (ieq-rows p) *row-vector-extension-increment*)
	 (incf (numieqs p) 2)
	 (format t "Debug: numieqs ~D, num-orig-rows ~D~%" (numieqs p) (num-orig-rows p)))
       finally (setf (fill-pointer (eq-rows p)) 0
		     (numeqs p) 0)
       )
  p)

(defun scalar-product (x y)
  "Compute the scalar product for two simple-arrays of rationals."
  (declare (type (simple-array rational) x y))
  (assert (= (length x) (length y)))
  (the rational
    (loop  for i of-type array-index from 0 below (length x)
       summing (the rational
		 (* (the rational (svref x i))
		    (the rational (svref y i)))))))
  
(defun point-validp (p pt)
  "Predicate to test whether PT is in P."
  (declare (type h-poly p)
	   (type (simple-array rational) pt))
  (and (= (adim p) (length pt))
       (or (not (upper-bounds p))
	   (every #'>= (upper-bounds p) pt))
       (or (not (lower-bounds p))
	   (every #'<= (lower-bounds p) pt))
       (loop for i of-type array-index
	  from 0 below (numeqs p)
	  always (let* ((row (aref (eq-rows p) i))
			(lhs (scalar-product (linform row) pt)))
		   (= lhs (rhs row))))
       (loop for i of-type array-index
	  from 0 below (numieqs p)
	  always (let* ((row (aref (ieq-rows p) i))
			(lhs (scalar-product (linform row) pt)))
		   (funcall (compare-fun-for-lrestr row) lhs (rhs row))))))
