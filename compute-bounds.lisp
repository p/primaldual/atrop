;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; package: compute-bounds -*-
;;;; *********************************************************************
;;;;
;;;; code to compute bounds for a H-poly
;;;; (c) 2006 Utz-Uwe Haus, <atrop@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; *********************************************************************

(eval-when
    (:compile-toplevel :load-toplevel)
  (declaim 
   (optimize (safety 3) (debug 2) (speed 1)
   ;;(optimize (safety 3) (debug 2) (speed 0)
	      
	     )
   ))


(defpackage #:compute-bounds
  (:documentation "Bound computation for H-polyhedra ($Revision$, $Date$).")
  (:use #:COMMON-LISP
	#:POLY
	#:cplex-glue #:cpxapi
	#:lrestr  #:atrop-utils)
  (:export 
   #:compute-bounds
  ))

(in-package #:COMPUTE-BOUNDS)

(defun compute-bounds (p
		       &key (destructive-p NIL) (integralize-epsilon 0 NIL))
  "Compute lower and upper bounds for P.
 If :DESTRUCTIVE-P is non-nil, update the lower/upper bounds in P.
 If :INTEGRALIZE-EPSILON is non-0 round to next integer after widening bounds by this value up and down. "
  (declare (type h-poly p))
  (multiple-value-bind (lower upper)
      (compute-lower/upper p)
    (if (> integralize-epsilon 0)
	(loop for i from 0 below (length lower)
	   do (setf (svref lower i) (ceiling (- (svref lower i) integralize-epsilon))
		    (svref upper i) (floor (+ (svref  upper i) integralize-epsilon)))))
    (if destructive-p
	(setf (poly:lower-bounds p) lower
	      (poly:upper-bounds p) upper))
    (values lower upper)))

(defun compute-lower/upper (p)
  (let ((dim (poly:ADIM p)))
    (multiple-value-bind (env lp)
	(construct-env/lp p dim)
      (unwind-protect
       (loop for i from 0 below dim
	  collecting (fix-and-minimize env lp i)
	  into lower
	  collecting (fix-and-maximize env lp i)
	  into upper
	  finally (return (values (convert-to-simple-array dim lower)
				  (convert-to-simple-array dim upper))))
       (progn
	 (cpxapi:CPXFREEPROB env lp)
	 (cpxapi:CPXCLOSECPLEX env)
	 )
       ))))

(defun convert-to-simple-array (dim l)
  (make-array dim :element-type 'rational :adjustable NIL
	      :initial-contents l))

(defun fix-and-minimize (env lp idx)
  (fix-and-min/max env lp idx (cpxapi:CPX-MIN)))
(defun fix-and-maximize (env lp idx)
  (fix-and-min/max env lp idx (cpxapi:CPX-MAX)))

(defun fix-and-min/max (env lp idx objsen)
  (let* ((dim (CPXgetnumcols env lp))
	 (obj (let ((l (make-list dim :initial-element 0)))
		(setf (elt l idx) 1)
		l))
	 (indices (iota dim)))
    
    (let ((old-sense (cpxapi:CPXGETOBJSEN env lp))
	  (old-obj (cpxapi:CPXGETOBJ env lp)))
      (unwind-protect
	   (progn
	     (cpxapi:CPXCHGOBJSEN env lp objsen)
	     (cpxapi:CPXCHGOBJ env lp indices obj)
	     (cpxapi:CPXPRIMOPT env lp)
	     (let ((status (CPXgetstat env lp)))
	       (unless (member status (list (cpxapi:CPX-STAT-OPTIMAL)))
		 (error "Polyhedron unbounded in direction of coordinate ~D" idx))
	       (aref (cpxapi:CPXGETX env lp idx idx) 0)))
	(progn
	  (cpxapi:CPXCHGOBJSEN env lp old-sense)
	  (cpxapi:CPXCHGOBJ env lp indices old-obj))))
    ))

(defun construct-env/lp (p dim)
  (let* ((env (make-cplex-env :output-to-screen NIL))
	 (lp (cpxapi:CPXCREATEPROB env "minmax"))
	 (indices (iota dim))
	 (colnames (loop for i from 1 upto dim
			collecting (format NIL "x~D" i)
			)))
    ;; add equations
    (loop for e across (poly:eq-rows p)
       do (cpxapi:CPXADDROW env lp (rhs e) #\E
			    indices (map 'list #'identity (linform e))
			    colnames NIL))
    ;; add ieqs
    (loop for l across (poly:ieq-rows p)
       do (cpxapi:CPXADDROW env lp (rhs l)
			    (case (sense l)
			      ('lrestr::<= #\L)
			      ('lrestr::>= #\G)
			      (otherwise (error "Bound computation cannot handle strict ieqs")))
			    indices (map 'list #'identity (linform l))
			    colnames NIL))
    ;; free vars
    (loop for i from 0 below dim
       do (cpxapi:CPXCHGBDS env lp (list i) (list #\L) (list (- (cpxapi:CPX-INFBOUND))))
       do (cpxapi:CPXCHGBDS env lp (list i) (list #\U) (list (cpxapi:CPX-INFBOUND))))
    ;; add bounds
    (if (poly:lower-bounds p)
	(loop for i from 0 below dim
	   do (cpxapi:CPXCHGBDS env lp (list i) (list #\L) (list (elt (poly:lower-bounds p) i)))))
    (if (poly:upper-bounds p)
	(loop for i from 0 below dim
	   do (cpxapi:CPXCHGBDS env lp (list i) (list #\U) (list (elt (poly:upper-bounds p) i)))))
    (values env lp)))



